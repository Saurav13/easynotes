@extends('frontend.main')
@section('body')


<div class="container">
<style type="text/css">

}

button, .btn {
    /* box-shadow: 0px 0px 3px 0px #3b3b3b, inset 0px 0px 0px 0px #3b3b3b; */
}
</style>
    
    <br>
   
    <div class="row forum">
        <div class="col-md-3 containershadow">
        <br>
            <form method="POST" action="{{route('search.threads')}}" accept-charset="UTF-8">
                <div class="form-group">
                    {{csrf_field()}}
                    <input class="form-control" placeholder="Search for threads..." name="search" type="text">
                    
                </div>
            </form>
            
            <a class="btn btn-theme btn-block" href="{{route('thread.create')}}">Create Thread</a>
            

            <h3>Threads Categories</h3>
                <a href="{{route('thread.index')}}" class="list-group-item active" style="background-color: #356fa1"/>All
                </a>

                            <div>
                                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                    @foreach($categories as $category)
                                      <div>
                                      <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="headingThree">
                                          <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" cid={{$category->category_id}} id="dright{{$category->category_id}}" data-parent="#accordion" href="#collapseThree{{$category->category_id}}" aria-expanded="false" aria-controls="collapseThree" style="text-decoration: none">
                                                <i style="color: #356fa1;" id="drightt{{$category->category_id}}" class="fa fa-angle-double-right" aria-hidden="true"></i>

                                              </i>  {{$category->category_name}}
                                            </a>
                                            
                                          </h4>
                                        </div>
                                        <div id="collapseThree{{$category->category_id}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                         @foreach($faculties->where('category_id', $category->category_id) as $f)
                                          <div class="panel-body">
                                            <a href="{{URL::to('thread/sortbyfaculty'.'/'.$f->faculty_id)}}" style="text-decoration: none;"><i style="color: #356fa1;" class="fa fa-angle-right" aria-hidden="true"></i>    {{$f->faculty_name}}</a>
                                          </div>
                                        @endforeach
                                        </div>
                                      </div>
                                    </div>
                                      @endforeach
                                    </div>
                                    </div>
                                        

                                 

        </div>


        <div class="col-md-9 ">
            
            <div class="bsa-cpc"></div>
        @foreach($threads as $thread)
            <div class="panel panel-default containershadow">
                <div class="panel-heading thread-info">
                    <div class="thread-info-avatar">
                        <img class="img-circle" src="{{asset('profile-images'.'/'.$thread->picture)}}" alt="{{asset('profile-images'.'/'.$thread->picture)}}">

                        <a href="forumdetail.html" class="thread-info-link">{{$thread->name}}</a>  Posted at: <i class="fa fa-clock-o" aria-hidden="true"></i> {{$thread->created_at->diffForHumans()}}

                  

                      
                        @if($thread->solution != null)
                                 <span class="badge pull-right" style=" margin: 5px; background:#5c8cb5 ">
                        <div class="comments">
                            <i class="fa fa-check" aria-hidden="true" style="color: ; "></i> Solved         
                            <div class="mark"></div>
                        </div>
                      </span>
                        @else
                                 <span class="badge pull-right" style=" margin: 5px; background:red; ">
                        <div class="comments">
                           <i class="fa fa-question" aria-hidden="true" style="color: "></i> Unsolved 
                        </div>
                      </span>

                        @endif
                
                    </div>




                </div>

                <div class="panel-body">
                  @if(!Request::is('thread/sortbyfaculty/*'))
                     <span class="badge pull-right" style="background-color: orange; margin: 5px;">
                      <i class="fa fa-tags" aria-hidden="true"></i>{{$thread->category_name}}
                      <i class="fa fa-tags" aria-hidden="true"></i>{{$thread->faculty_name}}
                    </span>
                  @endif 
                    <span class="badge pull-right" style="background-color: green;  margin: 5px;">
                      {{count($thread->comments)}}<i class="fa fa-comments-o" aria-hidden="true"></i>
                    </span>
                 
                    <a href={{route('thread.show',$thread->id)}}>
                        <h4 class="media-heading">{{$thread->title}}</h4>
                        <p>{!! substr(strip_tags($thread->description),0,250)!!}</p>
                    </a>
                </div>
            </div>
            @endforeach
            {{ $threads->links() }}
        </div>
    </div>
</div>

<script
src="https://code.jquery.com/jquery-3.2.1.min.js"
integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
crossorigin="anonymous"></script>
<script>
    
    $(document).ready(function(){

      $('[id *= "dright"]').click(function(event){

        var id= $(this).attr('cid');
        $('#drightt'+id).toggleClass('fa fa-angle-double-right fa fa-angle-double-down');

      });

    });

</script>

@endsection