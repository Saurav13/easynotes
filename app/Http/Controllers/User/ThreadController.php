<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Thread;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;
use App\Category;
use App\Faculty;


class ThreadController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'ThreadSortCategory','ThreadSortFaculty','searchThreads','show']]);
    }
    
    public function index()
    {
        $faculties=Faculty::all();
        $categories=Category::all();
        $search = null;
        $threads=Thread::join('categories','threads.category_name','categories.category_id')->join('users','threads.user_id','users.id')->join('faculties','threads.faculty_id','faculties.faculty_id')->select('threads.*','categories.category_name','users.name','users.picture','faculties.faculty_name')->orderBy('id','desc')->paginate(10);
        return view('frontend.forum.index')->with('categories',$categories)->with('threads',$threads)->with('faculties',$faculties)->with('search',$search); 
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    
    public function create()
    {
        $categories=Category::all();
        $threads=Thread::all();
        
        return view('frontend.forum.createthread')->with('categories',$categories)->with('threads',$threads);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, array(
            'title'=>'required|max:100',
            'description'=>'required',
        ));
    	auth()->user()->threads()->create($request->all());
        return redirect()->route('thread.index');
    }
    

    /**
     * Display the specified resource.
     *
     * @param  \App\Thread  $thread
     * @return \Illuminate\Http\Response
     */
    public function show(Thread $thread)
    {
        $categories=Category::all();
        $faculties = Faculty::all();
        return view('frontend.forum.single')->with('categories',$categories)->with('thread',$thread)->with('faculties',$faculties);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Thread  $thread
     * @return \Illuminate\Http\Response
     */
    public function edit(Thread $thread)
    {
    
    if(auth()->user()->id != $thread->user_id) {
        abort(401,"Unauthorized");
    }  
        $categories=Category::all();
        $faculties=Faculty::all();
        $threads=Thread::all();
         
        return view('frontend.forum.editthread')->with('thread',$thread)->with('categories',$categories)->with('faculties',$faculties)->with('threads',$threads);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Thread  $thread
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Thread $thread)
    {
		if(auth()->user()->id != $thread->user_id) {
			abort(401,"Unauthorized");
		}      
         $this->validate($request, array(
            'title'=>'required|max:100',
            'description'=>'required',
            

        ));
         $threads=Thread::find($thread->id);
         $threads->title=$request->title;
         $threads->description=$request->description;
         $threads->category_name=$request->category_name;
         $threads->faculty_id=$request->faculty_id;

         $threads->save();

         return redirect()->route('thread.show',$threads->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Thread  $thread
     * @return \Illuminate\Http\Response
     */
    public function deleteThread($id)
    {
        $thread=Thread::find($id);
    	if(auth()->user()->id != $thread->user_id) {
			abort(401,"Unauthorized");
		} 
        $thread->delete();
        return redirect()->route('thread.index');
    }

    public function FacultySelect($id)
    {

        $faculties=Faculty::where('category_id','=',$id)->get();
        return response()->json(['faculties'=>$faculties]);
    }

    public function markAsSolution()
    {

        $solutionId=Input::get('solutionId');
        $threadId=Input::get('threadId');
        $thread=Thread::find($threadId);
        if($thread->solution == null)
        {

            $thread->solution=$solutionId;
            if($thread->save()){
                if(request()->ajax()){
                    return response()->json(['status'=>'success']);
                    }
                return back();
                }
            

        }
        else
        {
            $thread->solution=null;
            if($thread->save()){
                if(request()->ajax()){
                    return response()->json(['status'=>'deleted']);
                    }
                return back();
                }
           
        }
    }

    public function ThreadSortCategory($id)
    {
        $faculties=Faculty::all();
        $categories=Category::all();
        $threads=Thread::join('users','threads.user_id','users.id')->select('threads.*','users.name','users.picture')->where('category_name','=',$id)->get();
        return view('frontend.forum.index')->with('threads',$threads)->with('faculties',$faculties)->with('categories',$categories);
    }

    public function ThreadSortFaculty($id)
    {
        $faculties=Faculty::all();
        $categories=Category::all();
        $threads=Thread::join('categories','threads.category_name','categories.category_id')->join('users','threads.user_id','users.id')->select('threads.*','categories.category_name','users.name','users.picture')->where('faculty_id','=',$id)->orderBy('id','desc')->paginate(10);

        
        return view('frontend.forum.index')->with('threads',$threads)->with('faculties',$faculties)->with('categories',$categories);
    }

    public function searchThreads(Request $request)
    {
        $search=$request->search;
        $faculties=Faculty::all();
        $categories=Category::all();
        $threads=Thread::where('title','LIKE','%'.$search.'%')->orWhere('description','LIKE','%'.$search.'%')->join('users','threads.user_id','users.id')->select('threads.*','users.name','users.picture')->orderBy('id','desc')->paginate(10);
        return view('frontend.forum.searchresults')->with('threads',$threads)->with('search',$search)->with('categories',$categories)->with('faculties',$faculties);
    }


}
