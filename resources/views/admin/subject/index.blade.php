@extends('admin.admin')

@section('body')
<br>
<div class="container-fluid">
<div class="row">
	<div class="col-md-6">
		<h3>Subjects</h3>
	</div>
	<div class="col-md-6">
		<form action="{{URL::to('admin/subject/search')}}" method="GET">
		<input type="search" name="key" required placeholder="Search subjects"class="form-control pull-right"/>
	<input type="submit" style="visibility: hidden;" />
	</form>
	</div>
</div>
<br>
<div class="card">
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8">
			<div class="content table-responsive table-full-width">
			<table class="table table-hover table-striped">
				<thead>
				<tr>
					<th>ID</th>
					<th>Subject Name</th>
					<th>Field</th>
					<th>Faculty</th>
					<th>Semester</th>
					<th>Action</th>
				</tr>
				</thead>
				<tbody>
				<tr>
				@foreach($subjects as $subject)
					<td >{{$subject->subject_id}}</td>
					<td id='sname{{$subject->subject_id}}'>{{$subject->subject_name}}</td>
					<td>{{$subject->category_name}}</td>
					<td>{{$subject->faculty_name}}</td>
					<td>{{$subject->semester}}</td>
					<td><input type="button" cid="{{$subject->subject_id}}" class="btn btn-md btn-primary" value="Edit" id="update{{$subject->subject_id}}"/  data-toggle="modal" data-target="#editmodal">
					<input type="button" cid="{{$subject->subject_id}}" class="btn btn-md btn-danger" value="Delete" id="delete{{$subject->subject_id}}"/></td>
		
				</tr>
				@endforeach
				</tbody>
			</table>
		</div>
		</div>
		
		 <!--end of col-md-8-->
		 
		<div class="col-md-4">
			<br>
			<div class="well">
			
				<h4>Add Subject</h4>
				{!! Form::open(['route'=>'subject.store']) !!}
				  Subject Name:<br>
				  <input type="text" name="subject" class="form-control"><br>
					Category:
					<select name="category" class="form-control" id='category'>
					<option selected disabled>Choose here</option>
					@foreach($categories as $category)
					  <option value="{{$category->category_id}}" >{{$category->category_name}}</option>
					@endforeach
					</select>
				  <br>
					Faculty:
					<select name="faculty" class="form-control" id='faculty'>
					<option selected disabled>Choose here</option>
					<option value=""></option>
					</select>
				  <br>
				Semester:
				<select name="semester" class="form-control" id='semester'>
				  <option selected disabled>Choose here</option>
				  <option value="1st">1st</option>
				  <option value="2nd">2nd</option>
				  <option value="3rd">3rd</option>
				  <option value="4th">4th</option>
				  <option value="5th">5th</option>
				  <option value="6th">6th</option>
				  <option value="7th">7th</option>
				  <option value="8th">8th</option>
				</select>
				<br>
				 <br>
				<button class="btn btn-md btn-success">Submit Details</button>

				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
</div>
</div>
<script
     src="https://code.jquery.com/jquery-3.2.1.min.js"
     integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
     crossorigin="anonymous"></script>

<script type="text/javascript">
$(document).ready(function(){
	$('#category').on('change', function(e){
		var cat_id = e.target.value;

		//ajax
		$.get('getfaculty?cat_id=' + cat_id,function(data){

			$('#faculty').empty();
			$.each(data, function(index, facultyObj){

				$('#faculty').append('<option value="'+facultyObj.faculty_id+'" >'+facultyObj.faculty_name+'</option>')

			});
		});
	});

});
	
</script>
<script>
	$(document).ready(function(){
		$('[id *= "delete"]').click(function(event){
			var id = $(this).attr('cid');
			console.log(id);
			swal({
			  title: "Are you sure?",
			  text: "You will not be able to recover this file!",
			  type: "warning",
			  
			  showCancelButton: true,
			  
			}).then(function(){
				$.post("{{route('subject.destroy')}}",{id:id,_token:"{{csrf_token()}}"},function(data){
					swal({
						title:"Deleted Successfully",
						type:"success"

					}).then(function(){
						window.location.reload();
					});
			});
		});
	});
	});
</script>




@stop
<div class="modal" id="editmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Edit Subject</h4>
                      </div>
                      <div class="modal-body">
                 <form action="{{route('subject.update')}}" method="post">
                 <input type="hidden" name="SubjectId" id="SubjectID" >
                 {{csrf_field()}}
                   Subject Name:<br>
                   <input type="" name="subject" class="form-control" id="subjectname"><br>
                 	Category:
                 	<select name="category" class="form-control" id='categorymodal'>
                 	<option selected disabled>Choose here</option>
                 	@foreach($categories as $category)
                 	  <option value="{{$category->category_id}}" >{{$category->category_name}}</option>
                 	@endforeach
                 	</select>
                   <br>
                 	Faculty:
                 	<select name="faculty" class="form-control" id='facultymodal'>
                 	<option selected disabled>Choose here</option>
                 	<option value=""></option>
                 	</select>
                   <br>
                 Semester:
                 <select name="semester" class="form-control" id='semestermodal'>
                   <option selected disabled>Choose here</option>
                   <option value="1st">1st</option>
                   <option value="2nd">2nd</option>
                   <option value="3rd">3rd</option>
                   <option value="4th">4th</option>
                   <option value="5th">5th</option>
                   <option value="6th">6th</option>
                   <option value="7th">7th</option>
                   <option value="8th">8th</option>
                 </select>
                 <br>
                  <br>
                 <input type="submit" class="btn btn-md btn-success" value="Save Changes">

                 </form>
                    </div>
                  </div>
                </div>
                </div>
                <script
                     src="https://code.jquery.com/jquery-3.2.1.min.js"
                     integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
                     crossorigin="anonymous"></script>
 <script type="text/javascript">
$(document).ready(function(){
	$('#categorymodal').on('change', function(e){
		var cat_id = e.target.value;
		console.log(cat_id);

		//ajax
		$.get('getfaculty?cat_id=' + cat_id,function(data){

			$('#facultymodal').empty();
			$.each(data, function(index, facultyObj){

				$('#facultymodal').append('<option value="'+facultyObj.faculty_id+'" >'+facultyObj.faculty_name+'</option>')

			});
		});
	});

});
$(document).ready(function(){
	$('[id *="update"]').click(function(event){
		var id = $(this).attr('cid');
		var sname = $('#sname'+id).html();
		console.log(sname);
		$('#subjectname').val(sname);
		$('#SubjectID').val(id);

	});
});
	
</script>               