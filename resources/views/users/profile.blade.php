@extends('layouts.frontend')

@section('body')
<div class="container" style="margin-top:100px">
	<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#createGroup">Create Group</button>

	<!-- Modal -->
	<div id="createGroup" class="modal fade" role="dialog">
	  <div class="modal-dialog">

	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Create Group</h4>
	      </div>
	      <div class="modal-body">
	        <form method="POST" action="{{ route('groups.create') }}" >
	        	{{ csrf_field() }}
	        	<label for="name">Name</label>
                <input id="name" type="text" name="name"  required >

                <button type="submit" class="btn btn=primary">Create</button>

	        </form>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	    </div>

	  </div>
	</div>
</div>
@endsection