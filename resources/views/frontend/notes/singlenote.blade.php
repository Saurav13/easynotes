@extends('frontend.main')
@section('body')

<style type="text/css">
  div.stars {
  width: 270px;
  display: inline-block;
}
input.star { display: none; }
 
label.star {
  float: right;
  padding: 5px;
  font-size: 20px;
  color: #444;
  transition: all .2s;
}
input.star:checked ~ label.star:before {
  content: '\f005';
  color: #FD4;
  transition: all .25s;
}
input.star-5:checked ~ label.star:before {
  color: #FE7;
  text-shadow: 0 0 20px #952;
}
input.star-1:checked ~ label.star:before { color: #F62; }
label.star:hover { transform: rotate(-15deg) scale(1.3); }
label.star:before {
  content: '\f006';
  font-family: FontAwesome;
}
h1,h3,h5,h6{
	font-weight: 400;
}
p{
	font-size: 18px;
}


</style>

<div class="container containershadow" style="margin-top: 30px; ">
<div class="container row" style="margin-bottom: 0px;">
<legend>
@foreach($notes as $note)
<input type="hidden" id="note_id" name="note_id" value="{{$note->id}}"/>
<input type="hidden" name="user_id" id="user_id" value="{{Auth::User()->id}}"/>
<h1>{{$note->title}}</h1>
<p>{{date('M j,Y',strtotime($note->created_at))}}</p>
 @if(pathinfo($note->file, PATHINFO_EXTENSION) == 'pdf')
<iframe src="{{ asset('notes/'.$note->file.'/'.$note->file)}}"" frameborder="0" style="width:100%;min-height:640px;"></iframe>
@elseif(pathinfo($note->file, PATHINFO_EXTENSION) == 'pptx')
<iframe src="https://view.officeapps.live.com/op/view.aspx?src={{ asset('notes/'.$note->file.'/'.$note->file)}}" frameborder="0" style="width:100%;min-height:640px;"></iframe>
@elseif(pathinfo($note->file, PATHINFO_EXTENSION) == 'ppt')
<iframe src="https://view.officeapps.live.com/op/view.aspx?src={{ asset('notes/'.$note->file.'/'.$note->file)}}" frameborder="0" style="width:100%;min-height:640px;"></iframe>
@elseif(pathinfo($note->file, PATHINFO_EXTENSION) == 'docx')
<iframe src="https://view.officeapps.live.com/op/view.aspx?src={{ asset('notes/'.$note->file.'/'.$note->file)}}" frameborder="0" style="width:100%;min-height:640px;"></iframe>
@elseif(pathinfo($note->file, PATHINFO_EXTENSION) == 'doc')
<iframe src="https://view.officeapps.live.com/op/view.aspx?src={{ asset('notes/'.$note->file.'/'.$note->file)}}" frameborder="0" style="width:100%;min-height:640px;"></iframe>
@elseif(pathinfo($note->file, PATHINFO_EXTENSION) == 'txt')
<iframe src="{{ asset('notes/'.$note->file.'/'.$note->file)}}" frameborder="0" style="width:100%;min-height:640px;"></iframe>
@endif

</legend>
</div>

<div class="container">

	<div class="row">
	<h4><i class="fa fa-download" aria-hidden="true"></i> Download</h4>
	<a href="{{ asset('notes/'.$note->file.'/'.$note->file)}}" download="{{$note->files}}">

	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

		    @if(pathinfo($note->file, PATHINFO_EXTENSION) == 'docx')
			        <td><img src="{{asset('/img/docx.png')}}"> Word Document</td>
			        
			        @elseif(pathinfo($note->file, PATHINFO_EXTENSION) == 'pptx')
			        <td><img src="{{asset('/img/pptx.png')}}"> Powerpoint slides</td>
			       
			        @elseif(pathinfo($note->file, PATHINFO_EXTENSION) == 'pdf')
			        <td><img src="{{asset('/img/pdf.png')}}"> Pdf</td>
			        
			        @elseif(pathinfo($note->file, PATHINFO_EXTENSION) == 'readme')
			        <td><img src="{{asset('/img/readme.png')}}">Readme</td>
			        
			        @elseif(pathinfo($note->file, PATHINFO_EXTENSION) == 'jpeg')
			        <td><img src="{{asset('/img/jpeg.png')}}">Jpeg</td>
			        
			        @elseif(pathinfo($note->file, PATHINFO_EXTENSION) == 'php')
			        <td><img src="{{asset('/img/txt.png')}}" style="height: 20px;  width: 20px;"> Text Document</td>
			
			        @else
			        <td><img src="{{asset('/img/docx.png')}}"> Word Document</td>
			       
			        @endif
			        <br>
</a>
	</div>

@if($rated==false)
	<div class="stars" >
	
<div id="rating" class="row pull-left">

<h4><i class="fa fa-star" aria-hidden="true"></i> Rate This Note:</h4>
&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;
  
    <input class="star star-5" id="star-5" type="radio" name="star"/>
    <label class="star star-5" for="star-5"></label>
    <input class="star star-4" id="star-4" type="radio" name="star"/>
    <label class="star star-4" for="star-4"></label>
    <input class="star star-3" id="star-3" type="radio" name="star"/>
    <label class="star star-3" for="star-3"></label>
    <input class="star star-2" id="star-2" type="radio" name="star"/>
    <label class="star star-2" for="star-2"></label>
    <input class="star star-1" id="star-1" type="radio" name="star"/>
    <label class="star star-1" for="star-1"></label>
  </div>
  </div>
  <p id="feedback" hidden>Rating Submitted. Thank you for your feedback</p>
  @endif
  
	@endforeach

	</div>
</div>

<script
  src="https://code.jquery.com/jquery-3.2.1.min.js"
  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  crossorigin="anonymous"></script>
<script>
	$(document).ready(function(){
		console.log('ready');
		$('[id*="star"]').click(function(){
			var l = $(this).attr('id');
			var k=l.substr(5,1);
			var note_id=$('#note_id').val();
			var user_id=$('#user_id').val();
			$.get('{{route('rate.post')}}',{rate:k, note_id:note_id, user_id:user_id},function(data,status){
				$('#rating').attr('hidden','true');
				$('#feedback').removeAttr('hidden');
			});
		})
	});
</script>



@endsection