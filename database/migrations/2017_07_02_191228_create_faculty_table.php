<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacultyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('faculties', function (Blueprint $table) {
                    $table->increments('faculty_id');
                    $table->string('faculty_name');
                    $table->string('description')->nullable();
                    $table->integer('category_id')->unsigned();
                    $table->foreign('category_id')->references('category_id')->on('categories')->onDelete('cascade');
                    $table->string('files')->nullable();
                    $table->timestamps();
                });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('faculties');
    }
}
