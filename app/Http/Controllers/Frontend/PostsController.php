<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Post;
use Counter;
class PostsController extends Controller
{
    public function index()
    {
        Counter::showAndCount('posts');
        
    	$posts=Post::all();
    	return view('frontend.posts.post')->with('posts',$posts);
    }

    public function single($id)
    {

    	Counter::showAndCount('posts');
        Counter::showAndCount('posts',$id);
        $posts=Post::where('id','=',$id)->get();
    	return view('frontend.posts.singlepost')->with('posts',$posts);
    }
}
