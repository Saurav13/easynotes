@extends('frontend.main')
@section('body')

	<style type="text/css">
		select{
			font-size: 17px;
		}

		label, input, select, textarea {
    font-size: 18px;
    font-weight: normal;
    padding-left: 10px;
    padding-right: 10px;
}

td {
    color: #656565;
    font-weight: 600;
}

.well{
	background-color: rgba(220, 220, 220, 0.12);
	border:0px;

}
	</style>
	
	

	<section id="content" style="padding-top: 0px;" ng-app="DirApp" ng-controller="DirController" ng-init="folkinit()">
		<input type="hidden" value="{{$id}}" id="userid"/>
		<input type="hidden" value="{{$dir}}" id="dir"/>

		<div class="container ">
			<h3>{{$name.'_'.$dir}}@{{2+2}}</h3>
			<ol style="background-color:#edeff1;" class="">
                  <li class="breadcrumb-item"> <a href="#" ng-click="folkinit()">{{$name.'_'.$dir}}</a></li> 
                  <li class="breadcrumb-item" ng-repeat="p in range(5,fopaths.length-2)">
                    <a href="#" ng-click="foback(p)">@{{fopaths[p]}} </a>
                  </li>
                  <li class="breadcrumb-item active" ng-hide="fopaths.length<6">@{{fopaths[fopaths.length-1]}}</li>
                </ol>


			<div class="container">
				


				  <table class="table table-condensed table-hover">
				    <thead>
				      <tr>
				        <th>S.no.</th>
				        <th>Title</th>
				        <th>Description</th>
				        <th>Action</th>
				      </tr>
				    </thead>
				    <tbody>
				      <tr ng-repeat="f in fofolders">
				        <td>1</td>
				        <td> <i class="fa fa-fw fa-folder"></i>@{{f}}</td>
				        <td>
           Folder</td>
				        <td >
				        <button class="btn-theme" ng-click="foopen(f)">Open</button> <a href="{{URL::to('/dir/downloadfofolder')}}/?path=@{{fopath}}&name=@{{f}}" class="btn-theme">Download</a>
				        </td>

				      </tr>

				      <tr ng-repeat="f in fofiles">
				        <td>1</td>
				        <td><i class="fa fa-fw fa-file"></i>@{{f}}</td>
				        <td>File</td>
				  
				        <td >
				        <button class="btn-theme">View</button> <a href="{{URL::to('/dir/downloadfile')}}/?path=@{{fopath}}&name=@{{f}}" class="btn-theme">Download</a>
				        </td>

				      </tr>

				     
				    </tbody>
				  </table>
				</div>
	</section>
<script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.5/angular.min.js"></script>
<script src="{{asset('dirjs/dirs.js')}}"></script>


@endsection