@extends ('admin.admin')


@section('body')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-6">
			<h3>All Posts</h3>			
		</div>
		<div class="col-md-6" >
		<br>
			<a href="{{route('post.create')}}" class="btn btn-md btn-primary pull-right" id="btn1">Create New Post</a>
		</div>
		
	</div>
	<br>
	<div class="card">
		<div class="container-fluid">
	<div class="content table-responsive table-full-width">
                            
			<table class="table table-hover table-striped" id="table1">
				<thead>
					<th>ID</th>
					<th>Title</th>
					<th>Body</th>
					<th>Created At</th>
					<th>Visitors</th>
					<th>Actions</th>
				</thead>
				<tbody>
					

					@foreach($posts as $post)
						<tr>
							<th>{{$post->id}}</th>
							<td>{{$post->title}}</td>
							<td>{{substr(strip_tags($post->description),0,50)}}{{strlen($post->description)>50 ? "...":"" /* checking if the string is more than 50 or not and adding ... if the body has more than 50 characters*/}}</td> 
							<td>{{date('M j,Y',strtotime($post->created_at))}}</td>
							<td>{{Counter::show('posts',$post->id)}}</td>
            
							<td><a href="{{route('post.show',$post->id)}}" class="btn btn-default btn-sm">View</a> <a href="{{route('post.edit',$post->id)}}" class="btn btn-primary btn-sm">Edit</a></td>
						</tr>

					@endforeach
				</tbody>

			</table>
	</div>
</div>
</div>
	</div>
	<script
  src="https://code.jquery.com/jquery-3.2.1.min.js"
  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  crossorigin="anonymous"></script>



@stop
