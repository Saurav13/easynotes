@extends('frontend.main')
@section('body')

<div class="container " style="margin-bottom: 30px;">

        
    <h1>Create your thread</h1>
        <hr>
        <div class="container containershadow">
        <div class="alert alert-info ">
            <p>
                Please try to search for your question first using
                <a href="https://laravel.io/forum" class="alert-link">the search box</a> and make sure you've read our
                <a href="https://laravel.io/rules" class="alert-link">Forum Rules</a> before creating a thread.
            </p>
        </div>
                      
    <form action="{{route('thread.store')}}" enctype="multipart/form-data" method="post">
    {{csrf_field()}}
    <div class="form-group">
        <label for="title">Title</label>
        <input class="form-control" required name="title" type="text" id="cattitle"  value="{{old('title')}}">
    </div>
    <div class="form-group">
        <label for="description">Description</label>
        <textarea class="form-control wysiwyg" required name="description" cols="50" rows="10" id="editor" value="{{old('description')}}"></textarea>
    </div>
  
      <input type="hidden" name="_token" value="{{csrf_token()}}">
      <br>

           <div class="form-group">
           	<label for="category_name">Category</label>
         	<select name="category_name" class="form-control" id='category'>
         	<option selected disabled>Choose here</option>
         	@foreach($categories as $category)
         	  <option value="{{$category->category_id}}" >{{$category->category_name}}</option>
         	@endforeach
         	</select>
           </div>
             <div class="form-group">
                <label for="faculty_id">Faculty</label>
              <select name="faculty_id" class="form-control" id='faculty'>
              <option selected disabled>Choose here</option>
              
              </select>
             </div>
           

    	
      
    <input class="btn btn-theme " type="submit" value="Create Thread">
    <a href="{{route('thread.index')}}" class="btn btn-default btn-">Cancel</a>
     </form>
     <br>
     </div>
    </div>
<script
      src="https://code.jquery.com/jquery-3.2.1.min.js"
      integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
      crossorigin="anonymous">
</script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#category').on('change',function(e){
        console.log('ready');
        var cat_id = e.target.value;

        //ajax
        $.get('getthreadfaculty/'+cat_id,function(data){

          $('#faculty').empty();

          $.each(data.faculties, function(index, faculties){
            console.log(faculties);
            $('#faculty').append('<option value="'+faculties.faculty_id+'" name="'+faculties.faculty_name+'" >'+faculties.faculty_name+'</option>')
            
            //bake cha for faculty heading
          });
    });
  });
});
</script>

<script>
             CKEDITOR.replace( 'editor' );
</script>

@endsection