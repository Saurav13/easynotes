@extends('frontend.main')
@section('body')

<style type="text/css">
  .panel.date {
      margin: 0px;
      width: 60px;
      text-align: center;
  }

  .panel.date .month {
      padding: 2px 0px;
      font-weight: 700;
      text-transform: uppercase;
  }

  .panel.date .day {
      padding: 3px 0px;
      font-weight: 700;
      font-size: 1.5em;
  }

</style>


<section id="featured">
  <!-- start slider -->
  
    <div class="row" style="margin-bottom: 0px;"">
      <div class="col-lg-12">
  <!-- Slider -->
        <div id="main-slider" class="flexslider" style="margin: 0px;">
            <ul class="slides">
            @foreach($slider as $s)
              <li>
                <img src="{{asset('systemimages'.'/'.$s->file)}}" alt="{{$s->title}}" />
                <div class="flex-caption">
                    <h3>{{$s->title}}</h3> 
          <p>{{$s->description}}</p> 
          <a href="{{$s->link}}" target="_blank" class="btn btn-theme">Learn More</a>
                </div>
              </li>
              @endforeach
            </ul>
        </div>
  <!-- end slider -->
      </div>
    </div>
  </section>

<section id="content" style="padding-top: 0px;">
  <div class="container">
    <div class="row">
      <div class="col-lg-8">
      @foreach($posts as $p)
        <article>
            <div class="post-image">
              <div class="post-heading">
              <a href="{{URL::to('blog'.'/'.$p->alias)}}" target="_blank" style="text-decoration: none;">
                <h3 class="hidden-lg" style="margin-bottom: 5px;">{{$p->title}}</h3>
                <h2 class="hidden-xs" style="margin-bottom: 5px;">{{$p->title}}</h2>
              </a>  
                <p><b> Edukunja</b>  |  <i class="fa fa-clock-o" aria-hidden="true"></i> {{$p->created_at->diffForHumans()}}</p>
              </div>

        <a href="{{URL::to('blog'.'/'.$p->alias)}}" target="_blank">
              <img src="{{asset('uploads'.'/'.$p->image)}}" alt="{{$p->title}}" />
        </a>      
            </div>
           
           <div class="content">
            <div class="read-more{{$p->id}}">
                {!!$p->description!!}       
            </div>
           </div>
        </article>
        @endforeach
      </div>
      <br>
      <div class="col-lg-4">
        <aside class="right-sidebar">
        <div class="widget">
          <h3 class="widgetheading"><i class="fa fa-search" aria-hidden="true"></i><a href="{{URL::to('opportunities')}}" style="text-decoration: none; color: black;">  Opportunities</h3>
          <ul class="recent">
          @foreach($opportunities as $o)
            <li>
          <a style="text-decoration: none;" target="_blank" href="{{URL::to('opportunities'.'/'.$o->id)}}">
            <img src="{{asset('uploads/opportunitiesimages'.'/'.$o->picture)}}" style="height: 65px; width: 65px; border-radius: 5%;" class="pull-left" alt="{{$o->title}}" />
            </a>
            <h6><a href="{{URl::to('events'.'/'.$o->id)}}" style="text-decoration: none;">{{str_limit($o->title, $limit = 50, $end = '..')}}</a></h6>
            <p>
            <a style="color: black; text-decoration: none;" target="_blank" href="{{URL::to('events'.'/'.$o->id)}}">
                 {!! strip_tags(str_limit($o->description, $limit = 80, $end = '..')) !!}
            </a>
            </p>
            </li>
            @endforeach
          </ul>
         <a href="{{URl::to('opportunities')}}" style="text-decoration: none;" target="_blank"> <button class="btn btn-block btn-primary">Look out for more opportunities</button></a>
        </div>   
        <div class="widget">
          <h3 class="widgetheading"><i class="fa fa-calendar" aria-hidden="true"></i><a style="color:black; text-decoration: none;" href="{{URL::to('events')}}"> Upcoming Events</a></h3>
          <ul class="recent">
          @foreach($events as $e)
            <a href="{{URl::to('events'.'/'.$e->id)}}" style="text-decoration: none;" target="_blank">
            <div class="row" style="margin-bottom: 0px;">
            <li>
            <div class="col-lg-3 col-sm-3 col-md-3 col-xs-3" style="padding-left: 15px;">
           <div class="panel panel-danger text-center date">

                                    <div class="panel-heading month">
                                        <span class="panel-title strong">
                                           {{date('M',strtotime($e->event_date))}}
                                        </span>
                                    </div>
                                    <div class="panel-body day text-danger">
                                        {{date('j',strtotime($e->event_date))}}
                                    </div>
                                </div>
            </div>
            <div class="col-lg-9 col-sm-9 col-md-9 col-xs-9" style="padding-left: 0px;">
            <h4 style="
    margin-top: 0px;
    margin-bottom: 0px;
">{{str_limit($e->title, $limit = 50, $end = '..')}}</h4>
            <p style="text-decoration: none; color: black;">
               {!! strip_tags(str_limit($e->description, $limit = 55, $end = '..')) !!}
            </p>
            </div>
            </li>
            </div>
            </a>
            <hr style="margin-bottom: 15px; margin-top: 0px;">

            @endforeach
          </ul>
          <a href="{{URL::to('events')}}" style="text-decoration: none;" target="_blank"><button class="btn btn-block btn-primary">Show More Events</button></a>
        </div>     
            </div>  
        </aside>
      </div>
    </div>
  </div>
  </section>			
	<script
	  src="https://code.jquery.com/jquery-3.2.1.min.js"
	  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
	  crossorigin="anonymous"></script>
	<script >
		@foreach($posts as $p)
		$(document).ready(function(){
			var readMoreHtml = $('.read-more'+{{$p->id}}).html();
			console.log(readMoreHtml);
			var lesstext = readMoreHtml.substr(0,300);
			if(readMoreHtml.length > 300)
			{
				$(".read-more"+{{$p->id}}).html(lesstext).append("<a href='' class='read-more-link{{$p->id}}'> Show More...</a>");
			}else{
				$(".read-more"+{{$p->id}}).html(readMoreHtml);
			}
			$("body").on("click",".read-more-link{{$p->id}}",function(event){
				event.preventDefault();
				$(this).parent(".read-more"+{{$p->id}}).html(readMoreHtml).append("<a href='' class='show-less-link{{$p->id}}'>Show Less...</a>")
			});
			$("body").on("click",".show-less-link{{$p->id}}",function(event){
				event.preventDefault();
				$(this).parent(".read-more"+{{$p->id}}).html(readMoreHtml.substr(0,300)).append("<a href='' class='read-more-link{{$p->id}}'>Show more...</a>")
			});

		});
		@endforeach
	</script>

<script type="text/javascript">
	var words = document.getElementsByClassName('word');
var wordArray = [];
var currentWord = 0;

words[currentWord].style.opacity = 1;
for (var i = 0; i < words.length; i++) {
  splitLetters(words[i]);
}

function changeWord() {
  var cw = wordArray[currentWord];
  var nw = currentWord == words.length-1 ? wordArray[0] : wordArray[currentWord+1];
  for (var i = 0; i < cw.length; i++) {
    animateLetterOut(cw, i);
  }
  
  for (var i = 0; i < nw.length; i++) {
    nw[i].className = 'letter behind';
    nw[0].parentElement.style.opacity = 1;
    animateLetterIn(nw, i);
  }
  
  currentWord = (currentWord == wordArray.length-1) ? 0 : currentWord+1;
}

function animateLetterOut(cw, i) {
  setTimeout(function() {
		cw[i].className = 'letter out';
  }, i*80);
}

function animateLetterIn(nw, i) {
  setTimeout(function() {
		nw[i].className = 'letter in';
  }, 340+(i*80));
}

function splitLetters(word) {
  var content = word.innerHTML;
  word.innerHTML = '';
  var letters = [];
  for (var i = 0; i < content.length; i++) {
    var letter = document.createElement('span');
    letter.className = 'letter';
    letter.innerHTML = content.charAt(i);
    word.appendChild(letter);
    letters.push(letter);
  }
  
  wordArray.push(letters);
}

changeWord();
setInterval(changeWord, 4000);


</script>

<script>
	$('.fbsharelink').click( function() 
	{
	    var shareurl = $(this).data('shareurl');
	    window.open('https://www.facebook.com/sharer/sharer.php?u='+escape(shareurl)+'&t='+document.title, '', 
	    'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');
	    return false;
	});
</script>
@endsection