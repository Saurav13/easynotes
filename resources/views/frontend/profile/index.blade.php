@extends('frontend.main')
@section('body')


	<style type="text/css">
		.pullquote-left {
    border-left: 2px solid #356fa1;
}

h1,h3,h3,h4{
	font-weight: 400;
}


.panel-default.panel-heading {
    color: #fff;
    background-color: #4971a7;
    border-color: #ddd;
}
	</style>
	<section id="content" style="padding: 20px;">

		<div class="container containershadow" style="padding-top: 30px; margin:auto;">

			<div class="row">

				<div class="col-md-8" >
					<h1>User's profile</h1>


					<div class="widget">
					<h3 class="widgetheading"></h3>
					<ul class="recent">

						<div class="row ">

							<div class="col-md-4 text-center">
						
						<img src="{{asset('profile-images'.'/'.Auth::User()->picture)}}" class="text-center" alt=""  height="100" width="100" />
						<h4><button class="btn-theme" data-toggle="modal" data-target="#edit">Edit</button></h4>
						</div>

						
						<br>

						<div class="col-md-4">

						<span class="pullquote-left">

							<div class="contianer row">
								<div class="col-md-12">
									<dl class="dl-horizontal">
										<dt>Name : </dt>
										<dd>{{Auth::User()->name}}</dd>
										
										<dt>Email : </dt>
										<dd>{{Auth::User()->email}}</dd>
									
										@if(Auth::User()->address != null)
										<dt>Address : </dt>
										<dd>{{Auth::User()->address}}</dd>
										@endif
										@if(Auth::User()->gender != null)
										<dt>Gender : </dt>
											@if(Auth::User()->gender = 'male')
										<dd>Male</dd>
											
											@else(Auth::User()->gender = 'female')
										<dd>Female</dd>
											@endif
											
										@endif
										@if(Auth::User()->institution != null)
										<dt>Institution : </dt>
										<dd>{{Auth::User()->institution}}</dd>
										@endif
										@if(Auth::User()->contact != null)
										<dt>Contact : </dt>
										<dd>{{Auth::User()->contact}}</dd>
										@endif
					
										@if(Auth::User()->about != null)
										<dt>About : </dt>
										<dd>{{Auth::User()->about}}</dd>
										@endif
				</dl>


									
								</div>
						

							
									
						
							</div>

							
					
						</span>

						</div>

						</div>
						
					
						
						
					</ul>

					<div class="modal fade" id="edit" role="dialog">
					    <div class="modal-dialog">
					    
					      <!-- Modal content-->
					      <div class="modal-content">
					        <div class="modal-header">


				

					    <!-- Modal content-->
					    <form action="{{route('edit.profile')}}" method="POST" enctype="multipart/form-data" >
					    	<input type="hidden" name="id" value="{{Auth::User()->id}}">
					    <input type="hidden" name="_token" value="{{ Session::token() }}">
					    <input type="hidden" name="id" value="{{Auth::User()->id}}">
					    <input type="hidden" name="_token" value="{{ Session::token() }}">
					    <div class="form-group">
					      <label class="control-label" for="name">Name (Full name)</label>  
					      
					     <div class="input-group col-md-6">
					           <div class="input-group-addon">
					            <i class="fa fa-user">
					            </i>
					           </div>

					           <input id="name" name="name" type="text" placeholder="Name (Full name)" class="form-control input-md" required="true" value="{{Auth::User()->name}}">
					          </div>

					        
					      </div>
					  
					    <!-- File Button --> 
					    <div class="form-group">
					      <label class="control-label" for="picture">Upload photo</label>
					      
					        <input id="picture" name="picture" class="input-file" type="file">
					    </div>
					    <div class="form-group">
					      <label class="control-label" for="gender">Gender</label> 
					        <label class="radio-inline" for="Male">
					          <input type="radio" name="gender" id="Male" value="Male" @if(Auth::User()->gender ==  'male' ) checked="checked" @endif>
					          Male
					        </label> 
					        <label class="radio-inline" for="Female">
					          <input type="radio" name="gender" id="Female" value="Female"
					          @if(Auth::User()->gender ==  'female' )
					          checked="checked" 
							@endif
					           >
					          Female
					        </label> 
					        <label class="radio-inline" for="Other">
					          <input type="radio" name="gender" id="Other" value="Other"
					                    @if(Auth::User()->gender ==  'other' )
					                    checked="checked" 
					          		@endif
					          >
					          Other
					        </label>
					    </div>

					    <div class="form-group">
					      <label class="control-label" for="address">Your Address</label>  
					      
					     <div class="input-group col-md-6">
					           <div class="input-group-addon">
					            <i class="fa fa-address">
					            </i>
					           </div>

					           <input id="address" value="{{Auth::User()->address}}" name="address" type="text" placeholder="Your Address.." class="form-control input-md">
					   </div>
					 
					        
					      </div>

					    <!-- Text input-->
					    <div class="form-group">
					      <label class="control-label" for="work">Your Current Institution</label>  
					      
					     <div class="input-group col-md-6">
					           <div class="input-group-addon">
					            <i class="fa fa-briefcase">
					            </i>
					           </div>

					           <input id="work" value="{{Auth::User()->institution}}" name="work" type="text" placeholder="Name of your institution.." class="form-control input-md">
					          </div>
					   
					        
					      </div>

					      <div class="form-group">
					        <label class="control-label" for="contact">Your Contact</label>  
					        
					       <div class="input-group col-md-6">
					             <div class="input-group-addon">
					              <i class="fa fa-phone">
					              </i>
					             </div>

					             <input id="contact" value="{{Auth::User()->contact}}" name="contact" type="text" placeholder="Your Contact number..." class="form-control input-md">
					            </div>
					      					          
					        </div>

					      
					      <div class="form-group">
					      <label class="control-label" for="about">About Me</label>  
					      <div class="input-group col-md-6">
					           <div class="input-group-addon">
					         <i class="fa fa-user"></i>
					            
					           </div>
					          <input id="about" value="{{Auth::User()->about}}" name="about" type="textbox" placeholder="Write about yourself..." class="form-control" >
					          </div>
					     </div>
					    <div class="form-group">
					      <label class="control-label" ></label>  
					      <div>
					      <input type="submit" class="btn btn-md btn-sucess" value="Save Changes" name="Submit">
					      
					        
					      </div>
					    </div>
					  </form>
					    </div>
					    </div>
					
				</div>
			</div>

							<br>
					        <div class="container">
						        
						            <h1>Forum</h1>


						    <div class="row">
						        <div class="col-md-2">
						            <div class="panel panel-default panel-counter">
						                <div class="panel-heading">Threads Posted</div>
						                <div class="panel-body">{{$countthreads}}</div>
						            </div>
						        </div>
						        <div class="col-md-2">
						            <div class="panel panel-default panel-counter">
						                <div class="panel-heading">Replies in Thread</div>
						                <div class="panel-body">{{$countreplies}}</div>
						            </div>
						        </div>
						        <div class="col-md-2">
						            <div class="panel panel-default panel-counter">
						                <div class="panel-heading">Solutions provided</div>
						                <div class="panel-body">{{$countsolution}}</div>
						            </div>
						        </div>
						    </div>


						    <div class="row profile-latest-items">
						    <div class="col-md-6">
						        <h3>Latest Thread</h3>
						        	<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
						        		@foreach($latestthreads as $l)
						        	  <div class="panel panel-default">
						        	    <div class="panel-heading" role="tab" id="headingThree">
						        	      <h4 class="panel-title">
						        	        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree{{$l->id}}" aria-expanded="false" aria-controls="collapseThree">
						        	          {{$l->title}}
						        	        </a>
						        	        <a href="{{URL::to('thread'.'/'.$l->id)}}" class="pull-right" ><button class="btn btn-md btn-primary">View Post</button></a>
						        	      </h4>
						        	    </div>
						        	    <div id="collapseThree{{$l->id}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
						        	      <div class="panel-body">
						        	        {!!$l->description!!}
						        	      </div>
						        	    </div>
						        	  </div>
						        	  @endforeach
						        	</div>
						            </div>
						            	@if($latestreplies != null)
						            <div class="col-md-6">
						                <h3>Latest Replies on threads</h3>
						                	<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
						                		@foreach($latestreplies as $lr)
						                	  <div class="panel panel-default">
						                	    <div class="panel-heading" role="tab" id="headingThree">
						                	      <h4 class="panel-title">
						                	        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$lr->id}}" aria-expanded="false" aria-controls="collapseThree">
						                	          {{$lr->title}}
						                	        </a>
						                	        <a href="{{URL::to('thread'.'/'.$lr->id)}}" class="pull-right" ><button class="btn btn-md btn-primary">View Post</button></a>
						                	      </h4>
						                	    </div>
						                	    <div id="collapse{{$lr->id}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
						                	      <div class="panel-body">
						                	        {!!$lr->description!!}
						                	      </div>
						                	    </div>
						                	  </div>
						                	  @endforeach
						                	</div>
						                    </div>
						                    @endif
						                

						         

						</div>
   					 </div>
				</div>



						
					
				</div>

				<style type="text/css">
					img{
						border-radius: 50px;
					}
				</style>



				<div class="col-md-4">
					


						<div class="widget">

							
						<h3 class="widgetheading">Marked Drives</h3>
					
						  <div class="list-group">
						   	@foreach($follows as $f)
						    <a href="{{URL::to('profile/viewnotes/?id='.$f->id.'&name='.$f->name.'&dir='.$f->dir)}}" class="list-group-item">{{$f->name.'_'.$f->dir}}</a>
						    @endforeach
						    
						    	  
									    
						</div>
							<a class="btn btn-theme btn-block" target="_blank"  href="{{URL::to('directory/search')}}"> <i class="fa fa-search" aria-hidden="true"></i> Find Drives</a>
						</div>

				


							<div class="widget">
						<h3 class="widgetheading">Followed Groups</h3>

						<a class="btn btn-theme btn-block" ><i class="fa fa-twitter" aria-hidden="true"></i> Follow Group</a>
						<br>
					
						  <div class="list-group">
						    <a href="#" class="list-group-item"><i class="fa fa-users" aria-hidden="true"></i> First item</a>
						    <a href="#" class="list-group-item"><i class="fa fa-users" aria-hidden="true"></i> Second item</a>
						    <a href="#" class="list-group-item"><i class="fa fa-users" aria-hidden="true"></i> Third item</a>
						 
						</div>
						</div>


					
					
				</div>

			</div>
		</div>
	
	</section>


{{-- 	<div class="modal fade" id="followdir" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"> <i class="fa fa-twitter" aria-hidden="true"></i> Follow Directory</h4>
        </div>
        <div class="modal-body" style="">
        	
              <div class="form-group">
                <h5>Search User </h5>
                
                <input type="text" class="form-control" id="usr" name="search">
              </div>
			
              <button class="btn btn-theme btn-block" id="searchuser">Search</button>
       

            <hr>
              


            <div class="" style="background-color: transparent;"> --}}
   {{--          <div class="row">

              <div class="col-md-3 hidden" id="ShowUsers">
                <h5>Search results for User</h5>
                 <div class="force-overflow"> 
                <div class="card text-center" style="width: 10rem;" id="appendusers">
                  <div class="card-block hidden" id="resultsuser">
                  
                   
                   <div class="card-block">
                   <p class="card-text" id="user_name"></p>
          
                    
                     
                   
                   </div>
                    
                  
                  </div>
                    
                  
                  </div>
               </div>

               <br>
               
              
          </div>

          </div> --}}


   {{--        <div class="col-md-12 hidden">

          <h5>Directory Availables</h5>
                 <div class="force-overflow">
                 <div class="card text-center" style="width: 10rem;">
                  <img class="card-img-top" src="https://maxcdn.icons8.com/Share/icon/p1em/Very_Basic//folder1600.png" height="25" width="25" style="border-radius: 50px;" alt="Card image cap">
                  <div class="card-block">
                  <p class="card-text">Baam Solti 2</p>
                   <button class="btn btn-default text-center" style="background-color: transparent; color:black; "><i class="fa fa-plus" aria-hidden="true"></i>  </button>
      
                  </div>
               </div>

               <br>

      
        </div>
        
      </div> --}}


{{--           <div class="col-md-12" hidden>

          <h5>Directory Added</h5>



          
                 <div class="force-overflow" >
            
            
                 <div class="card text-center" style="width: 10rem;">
                  <img class="card-img-top" src="https://maxcdn.icons8.com/Share/icon/p1em/Very_Basic//folder1600.png" height="25" width="25" style="border-radius: 50px;" alt="Card image cap">
                  <div class="card-block"  >
                  
                   <button class="btn btn-default text-center" style="background-color: transparent; color:black; "><i class="fa fa-times" aria-hidden="true"></i>  </button>
                   
                    
                  
                  </div>
               </div>

               <br>

      
        </div>
        
      </div>


      <div class="col-md-3 pull-right btn btn-md btn-sucess ">

        <button class="btn btn-theme btn-block text-center">Finish</button>
      </div>
      
    </div>


  </div>

  <div class="modal-footer">
  	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  </div>
  </div>
  </div>
  </div> --}}




	<div class="modal fade" id="followgroup" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"> <i class="fa fa-twitter" aria-hidden="true"></i> Follow Group</h4>
        </div>
        <div class="modal-body">
            <form>
              <div class="form-group">
                <h5>Group Name </h5>
                <input type="text" class="form-control" id="usr">
              </div>
              <button class="btn btn-theme btn-block">Search</button>
            </form>
            <hr>     
            <div class="" style="background-color: transparent;">
            <div class="row">
              <div class="col-md-12">
                <h5>Add New Member</h5>
                 <div class="force-overflow">
                <div class="card text-center" style="width: 10rem;">
                  <img class="card-img-top" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/195612/chat_avatar_01.jpg" alt="Card image cap">
                  <div class="card-block">
                  <p class="card-text">Baam Solti</p>
                   <button class="btn btn-default text-center" style="background-color: transparent; color:black; "><i class="fa fa-plus" aria-hidden="true"></i></button>
                  </div>
               </div>
               <br>
          
          </div>

          </div>


          <div class="col-md-12">

          <h5>Member Added</h5>
                 <div class="force-overflow">
                 <div class="card text-center" style="width: 10rem;">
                  <img class="card-img-top" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/195612/chat_avatar_01.jpg" alt="Card image cap">
                  <div class="card-block">
                  <p class="card-text">Baam Solti</p>
                   <button class="btn btn-default text-center" style="background-color: transparent; color:black; "><i class="fa fa-times" aria-hidden="true"></i>  </button>
                  </div>
               </div>
               <br>
        </div>
        
      </div>


      <div class="col-md-12 ">

    
        <button class="btn btn-theme btn-block text-center">Finish</button>
      </div>
      
    </div>


  </div>

  <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
  </div>
  </div>
  </div>
  </div>




  <script
    src="https://code.jquery.com/jquery-3.2.1.min.js"
    integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
    crossorigin="anonymous"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#searchuser').on('click',function(){

			$('#ShowUsers').removeClass('hidden');
			var search = $('#usr').val();
			console.log($('#oooo').attr('height'));
			$.get('getusers/directory?search='+search,function(data){
				
				if (data.users.length == 0) {
					$('#ShowUsers').removeClass('hidden');
				  $('#appendusers').append('<p>No results</p>');
				}
				if(search.length == 0)
				{
				$('#ShowUsers').removeClass('hidden');
				  $('#appendusers').append('<p>No results for empty search</p>');
					
				}
					$.each(data.users, function(index, users){
						var picture = [users.picture];
						$('#ShowUsers').removeClass('hidden');
						$('#resultsuser').removeClass('hidden');
						$('#appendusers').append('<p class="card-text">'+users.name+'</p>');
						$('#appendusers').append('<img class="card-img-top" height="25" width="25" style="border-radius: 50px;" src="../../profile-images/'+users.picture+'" alt="Card image cap">')
					
					});
				
			});	
		});
	});
</script>
  



@endsection