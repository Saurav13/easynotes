<body>

  <div class="wrapper">
    <div class="sidebar" data-color="purple" data-image="{{asset('assets/img/sidebar-5.jpg')}}">

<!--

Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
Tip 2: you can also add an image using data-image tag

-->

<div class="sidebar-wrapper">
  <div class="logo">
    <a href="http://www.creative-tim.com" class="simple-text">
      Creative Tim
    </a>
  </div>

  <ul class="nav">

    <li class="{{ Request::segment(2)=='' ? 'active': '' }}">
      <a href="{{route('admin.dashboard')}}">
        <i class="pe-7s-graph"></i>
        <p>Dashboard</p>
      </a>
    </li>
    <li class="{{ Request::segment(2)=='category' ? 'active': '' }}">
      <a  href="{{route("cat.index")}}">
        <i class="pe-7s-news-paper"></i>
        <p>Categories</p>
      </a>
    </li>
    <li class="{{ Request::segment(2)=='faculty' ? 'active': '' }}">
      <a href="{{route('faculty.index')}}">
        <i class="pe-7s-note2"></i>
        <p>Faculties</p>
      </a>
    </li>
    <li class="{{ Request::segment(2)=='subject' ? 'active': '' }}">
      <a  href="{{route('subject.index')}}">
        <i class="pe-7s-science"></i>
        <p>Subjects</p>
      </a>
    </li>
    <li class="{{ Request::segment(2)=='qb' ? 'active': '' }}">
      <a  href="{{URL::to('admin/qb')}}">
        <i class="fa fa-question" aria-hidden="true"></i>
        <p>Questions</p>
      </a>
    </li>
    <li class="{{ Request::segment(2)=='post' ? 'active': '' }}">
      <a  href="{{route('post.index')}}">
        <i class="fa fa-newspaper-o" aria-hidden="true"></i>
        <p>Posts</p>
      </a>
    </li>
    <li class="{{ Request::segment(2)=='notes' ? 'active': '' }}">
      <a  href="{{route('notes.index')}}">
       <i class="pe-7s-note2"></i>
        <p>Notes</p>
      </a>
    </li>

    <li class="{{ Request::segment(2)=='events' ? 'active': '' }}">
      <a  href="{{route('events.index')}}"/>
      <i class="fa fa-calendar" aria-hidden="true"></i>
      <p>Events</p>
    </a>
  </li>
    
    <li class="{{ Request::segment(2)=='opportunities' ? 'active': '' }}">
      <a  href="{{route('opportunities.index')}}"/>
      <i class="fa fa-calendar" aria-hidden="true"></i>
      <p>Opportunities</p>
    </a>
  </li>

  <li class="{{ Request::segment(2)=='sliderimage' ? 'active': '' }}">
      <a  href="{{route('sliderimage.index')}}"/>
      <i class="pe-7s-photo" aria-hidden="true"></i>
      <p>Images Manager</p>
    </a>
  </li>
  <li class="{{ Request::segment(2)=='syllabus' ? 'active': '' }}">
      <a  href="{{route('syllabus.index')}}"/>
      <i class="pe-7s-photo" aria-hidden="true"></i>
      <p>Syllabus</p>
    </a>
  </li>
   <li class="{{ Request::segment(2)=='contribution' ? 'active': '' }}">
      <a  href="{{URL::to('/admin/contribution')}}"/>
      <i class="pe-7s-photo" aria-hidden="true"></i>
      <p>Contrubutions</p>
    </a>
  </li>


</ul>

</div>
</div>