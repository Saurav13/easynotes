@extends('frontend.main')
@section('body')

<div class="container">

	
	<br>

	<div class="row forum">
	        <div class="col-md-3">
	            <div class="profile-user-info">
	            <a href="#">
	            <img src="{{asset('profile-images'.'/'.$thread->user->picture)}}" style="height: 150px; width: 150px;">
	        </a>
	    
	    <h2>{{$thread->user->name}}</h2>
	</div>

	            <hr>
	            @if(Auth::User())
	            @if(Auth::User()->id == $thread->user->id)
	            <div class="row">
	            	<a href="{{route('thread.edit',$thread->id)}}"><button class="btn btn-block btn-primary">Edit Thread</button></a>
	            </div>
	            <div class="row">
	            	<button class="btn btn-block btn-danger">Delete Thread</button>
	            </div>
	            @endif
	            @endif
	       
	    </div>
   


		<div class="col-md-9">
			<!-- <script src="//m.servedby-buysellads.com/monetization.js" type="text/javascript"></script> -->
			<div class="bsa-cpc"></div>
			<div class="panel panel-default containershadow">
				<div class="panel-heading thread-info">
					<div class="thread-info-avatar">					        				<img class="img-circle" src="{{asset('profile-images'.'/'.$thread->user->picture)}}" alt="{{asset('profile-images'.'/'.$thread->user->picture)}}">


						<a  class="thread-info-link">{{$thread->user->name}}</a> Posted at: <i class="fa fa-clock-o" aria-hidden="true"></i> {{date('M j,Y',strtotime($thread->created_at))}}


					</div>



				</div>

				<div class="panel-body">

					<h4 class="media-heading">{!!$thread->title!!}</h4>
					<p>{!!$thread->description!!}</p>




					<hr>

					@foreach($thread->comments as $comment)
					<div class="">
						<div class="panel-heading thread-info">
							<div class="thread-info-avatar">
								
									<i class="fa fa-chevron-right" aria-hidden="true"></i> <img class="img-circle" src="{{asset('profile-images'.'/'.$comment->user->picture)}}" style="height: 25px; width: 25px;">

								
								<a class="thread-info-link">{{$comment->user->name}}</a> Posted at: {{date('M j,Y',strtotime($comment->created_at))}}
								@if(Auth::User())
								@if(auth()->user()->id == $comment->user_id)
								<div class="w3-dropdown-hover pull-right">
									<button class="btn btn-default">...</button>
									<div class="w3-dropdown-content w3-bar-block w3-border">
										<a class="w3-bar-item w3-button" data-toggle="modal" href='#{{$comment->id}}'>Edit</a>
										{{-- comment ko delete --}}

										<form action="{{route('comment.destroy',$comment->id)}}" method="POST" class="inline-it">
											{{csrf_field()}}
											{{method_field('DELETE')}}
											<form action="{{route('comment.destroy',$comment->id)}}" method="POST" class="inline-it">
												{{csrf_field()}}
												{{method_field('DELETE')}}
												<input type="submit" class="w3-bar-item w3-button" name="delete" value="Delete"/>
											</form>
											</form>

										</div>
									</div>
									@endif
									@endif
									
									@if(!empty($thread->solution))
									@if($thread->solution == $comment->id)
									<p class="pull-right"><i class="fa fa-check" aria-hidden="true"></i> Solution</p>
									@endif
									@else
									@if(auth()->check())
									@if(auth()->user()->id == $thread->user_id)
									<button id="markassoln{{$comment->id}}" class="pull-right btn btn-success" cid={{$comment->id}} onclick="markAsSolution('{{$thread->id}}','{{$comment->id}}',this)">Mark as Solution</button>
									@endif
									@endif
									@endif		
								</div>
							</div>

							<div class="panel-body">

								<p>{{$comment->body}}</p>
					
								<button cid="{{$comment->id}}" class="pull-right btn" id="toggle-comment-reply{{$comment->id}}"><i class="fa fa-eye" aria-hidden="true"></i></button>
								@if(Auth::check())
								<button class="pull-right btn btn-theme"  id="commentreplyform{{$comment->id}}" cid={{$comment->id}}>Reply</button>


								<a class="{{$comment->isLiked()?"liked":""}}" onclick="likeit('{{$comment->id}}',this)"><i class="fa fa-thumbs-up" aria-hidden="true"></i><span id="{{$comment->id}}-count"> {{$comment->likes()->count()}}</span> Likes</a>

								<br>

								<div class="hidden" id="replyformcomment{{$comment->id}}" >
									<form action="{{route('replycomment.store',$comment->id)}}" method="post" role="form">
										{{csrf_field()}}
										<h5>Reply to Comment</h5>
										<div class="form-group">
											<input type="text" class="form-control" name="body" id="" placeholder="Reply...">
										</div>
										<button type="submit" class="btn btn-primary">Reply</button>
									</form>
								</div>
								@endif
								{{-- reply to comment --}}
								@foreach($comment->comments as $reply)
								<div class="small well text-info reply-list hidden" cid="{{$comment->id}}" style="margin-left: 40px;" id="comment-reply{{$comment->id}}">
									<p>{{$reply->body}}</p>
									<lead>By:{{$reply->user->name}}</lead>
									@if(Auth::User())
									@if(auth()->user()->id == $reply->user_id)
									<div class="actions">
										<a class="btn btn-primary btn-xs" data-toggle="modal" href='#{{$reply->id}}'>Edit</a>
										<div class="modal fade" id="{{$reply->id}}">
											<div class="modal-dialog">
												<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
														<h4 class="modal-title">Edit Comment</h4>
													</div>
													<div class="modal-body">
														<div class="comment-form">
															<form action="{{route('comment.update',$reply->id)}}" method="post" role="form">
																{{csrf_field()}}
																{{method_field('put')}}
																<legend>Edit new Comment</legend>
																<div class="form-group">
																	<input type="text" class="form-control" name="body" id="" placeholder="Add Comment.." value="{{$reply->body}}" >
																</div>
																<button type="submit" class="btn btn-primary">Reply</button>
															</form>
														</div>
													</div>	
												</div>
											</div>
										</div>
										<form action="{{route('comment.destroy',$reply->id)}}" method="POST" class="inline-it">
											{{csrf_field()}}
											{{method_field('DELETE')}}
											<input type="submit" name="delete" value="Delete" class="btn btn-xs btn-danger"/>
										</form>
										@endif
										@endif
									</div>	
								</div>
								@endforeach



							</div>
					</div>

						<hr>

			@if(Auth::User())
				<div class="modal fade" id="{{$comment->id}}">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4 class="modal-title">Edit Comment</h4>
							</div>
							<div class="modal-body">
								<div class="comment-form">
									<form action="{{route('comment.update',$comment->id)}}" method="post" role="form">
										{{csrf_field()}}
										{{method_field('put')}}
										<legend>Edit new Comment</legend>
										<div class="form-group">
											<input type="text" class="form-control" name="body" id="" placeholder="Add Comment.." value="{{$comment->body}}" >
										</div>
										<button type="submit" class="btn btn-primary">Submit</button>
									</form>
								</div>
							</div>
							
						</div>
					</div>
				</div>
				@endif
				@endforeach


<!-- 
	BIG COMMENT -->
	@if(!Auth::User())
					<div>
						<p class="text-center">
							<a href="{{route('login')}}">Sign in</a> to participate in this thread!
						</p>
						<div class="alert alert-info">
							<p>
								Please make sure you've read our
								<a href="https://laravel.io/rules" class="alert-link">Forum Rules</a> before replying to this thread.
							</p>
						</div>
						</div>
	@endif					
				@if(Auth::User())
					<div>
					<form method="POST" action="{{route('threadcomment.store',$thread->id)}}" accept-charset="UTF-8">
						{{csrf_field()}}
						<div class="form-group">
							<textarea class="form-control wysiwyg" placeholder="Add Comment..." required name="body" cols="50" rows="10"></textarea>
						</div>
						<input class="btn btn-primary btn-block" type="submit" value="Comment">
					</form>

				</div>
				@endif
				
				
				</div>
					

				
			</div>
		</div>


</div>
</div>


<script
src="https://code.jquery.com/jquery-3.2.1.min.js"
integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous">
	
</script>
<script type="text/javascript">
	$(document).ready(function(){
		$('[id *="commentreplyform"]').click(function(event){
			console.log('ready');
			var lo= $(this).attr('cid');
			console.log(lo);
			$('#replyformcomment'+lo).toggleClass('hidden');
		});
	});
	$('[id *="toggle-comment-reply"]').click(function(event){
		var k=$(this).attr('cid');
		console.log(k);
		$('#comment-reply'+k).toggleClass('hidden');

	});
</script>
<script type="text/javascript">
	function markAsSolution(threadId,solutionId,elem){
		var csrfToken='{{csrf_token()}}';
		$.post('{{route('markAsSolution')}}',{solutionId: solutionId, threadId: threadId,_token:csrfToken},function(data){
			
		});
	}
	function likeit(commentId,elem){
		var csrfToken='{{csrf_token()}}';
		var likesCount=parseInt($('#'+commentId+"-count").text());
		$.post('{{route('toggleLike')}}',{commentId: commentId,_token:csrfToken},function(data){
			console.log(data);
			if(data.message==='liked'){
				$('#'+commentId+"-count").text(likesCount+1);
				$(elem).addClass('liked');
     				// $(elem).css({color:'red'});
     			}else{
     				$('#'+commentId+"-count").text(likesCount-1);
     				$(elem).removeClass('liked');
     			// $(elem).css({color:'red'})
     		}
     	});
	}
</script>
<script type="text/javascript">
	$('div.ex').click(function(){
		$(this).children('ul').slideToggle().removeClass('hidden');
		return false;
	});
	      $('[id *= "markassoln"]').click(function(){
	        var solution= $(this).attr('cid');
	          if($(this).html()==="Mark as Solution")
	          {
	            $(this).html("Unmark this");
	          
	        }
	        else
	        { 
	          $(this).html("Mark as Solution");
	              
	        }
	});

</script>

@endsection