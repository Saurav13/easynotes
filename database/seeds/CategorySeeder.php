<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
           DB::table('categories')->insert([
            'category_name' => "Tribhuvan Univerity",
            
        ]);

         DB::table('categories')->insert([
            'category_name' => "Kathmandu Univerity",
            
        ]);

         DB::table('categories')->insert([
            'category_name' => "Pokhara Univerity",
            
        ]);
          DB::table('categories')->insert([
             'category_name' => "Purbanchal Univerity",
             
         ]);
    }
}
