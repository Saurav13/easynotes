<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contribution extends Model
{
     public function files(){
        return $this->hasMany('App\Contributionfile');
    }
}
