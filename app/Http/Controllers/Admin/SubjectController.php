<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use App\Subject;
use App\Faculty;
use App\Category;
use Session;

class SubjectController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
         

    	$faculties = Faculty::all();
    	$categories = Category::all();
        $subjects =  Subject::join('categories','subjects.category_id','categories.category_id')->join('faculties','subjects.faculty_id','faculties.faculty_id')->select('subjects.*','faculties.faculty_name','categories.category_name')->orderBy('subject_id','desc')->get();
		return view('admin.subject.index')->with('subjects',$subjects)->with('categories',$categories)->with('faculties',$faculties);
    }

    public function search(Request $request)
    {
        $faculties = Faculty::all();
        $categories = Category::all();
        $subjects =  Subject::where('subject_name','LIKE','%'.$request->key.'%')->join('categories','subjects.category_id','categories.category_id')->join('faculties','subjects.faculty_id','faculties.faculty_id')->select('subjects.*','faculties.faculty_name','categories.category_name')->orderBy('subject_id','desc')->get();
        return view('admin.subject.index')->with('subjects',$subjects)->with('categories',$categories)->with('faculties',$faculties);

    }
    public function CatSelect()
    {
        $cat_id = Input::get('cat_id');
        $faculties=Faculty::where('category_id','=',$cat_id)->get();                         
        return Response::json($faculties);
    }
   

    public function store(Request $request)
    {
        $this->validate($request, array(
            'subject'=>'required|max:100',
            'category'=>'required',
            'faculty'=>'required',
            
        ));
     //store in the database
    $subjects=new Subject;
    $faculties=new Faculty;
    $categories=new Category;
    $subjects->subject_name=$request->subject;
    $subjects->category_id=$request->category;
    $subjects->semester=$request->semester;
    $subjects->faculty_id=$request->faculty;
    //save data
  
    $subjects->save();
       
    Session::flash('success','Subject was added successfully!');

    return redirect()->route('subject.index');  
    }

    public function destroy(Request $request)
    {
        $subjects = Subject::findorFail($request->id);
        $subjects->delete();

        return redirect()->route('subject.index');
    }

    public function update(Request $request)
    {
         $this->validate($request, array(
            'subject'=>'required|max:100',
            'category'=>'required',
            'faculty'=>'required',
            
        ));
        
        $subjects = Subject::find($request->SubjectId);
        $subjects->subject_name=$request->subject;
        $subjects->category_id=$request->category;
        $subjects->faculty_id=$request->faculty;
        $subjects->semester=$request->semester;

        $subjects->save();

        Session::flash('success','Subject was updated succesfully!');

        return redirect()->route('subject.index');

    }
}
  

