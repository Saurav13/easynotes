@extends ('admin.admin')


@section('body')
<div class="container-fluid">
	<div class="card">
    <div class="container-fluid">
		<div class="header">
			<h2>Question Bank</h2>
		</div>
		 @if(Request::segment('4') == 'edit')
          {!! Form::model($qb,['url' => 'admin/qb'.'/'.$qb->id,'method'=>'PATCH','files'=>'true'])!!}
       
        @else
          {!! Form::open([ 'enctype'=>'multipart/form-data','url' => 'admin/qb','files'=>'true'])!!}
        @endif
        <div class="form-group">
        <label for="title">Title</label>
        <input name="title" id="title" type="text" class="form-control" required placeholder="Title"/>
        <br>
        <label for="faculties">Faculty</label>
          {!! Form::select('faculty_id',$faculties,null,['id'=>'faculties', 'class'=>'form-control','required'=>'true']) !!}
        <br>
        <label>Semester</label>
        <select name="semester" class="form-control" required>
        	<option>1st</option>
        	<option>2nd</option>
        	<option>3rd</option>
        	<option>4th</option>
        	<option>5th</option>
        	<option>6th</option>
        	<option>7th</option>
        	<option>8th</option>
        </select>
        <br>
        <input type="file" name="file" class="form-control" required/>
        <br>
         <input type="submit" class="btn btn-success btn-md" value="Save">
       </div>
       <br>
          {!! Form::close() !!}
      <br>
	</div>
  </div>
</div>
@stop