<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAlisaToPostEventOppertunities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts', function (Blueprint $table) {
        $table->string('alias')->unique();    
        });
        Schema::table('events', function (Blueprint $table) {
        $table->string('alias')->unique();    
        });
        Schema::table('opportunities', function (Blueprint $table) {
        $table->string('alias')->unique();    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
