<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use Session;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $categories = Category::all();
        return view('admin.facultycategory.index')->with('categories',$categories);

    }


    public function store(Request $request)
    {
        $this->validate($request, array(
            'title'=>'required|max:255',
            'file'=>'sometimes|image',
            'description'=>'sometimes',

            ));
        $categories = new Category;
        $categories->category_name=$request->title;
        $categories->description=$request->description;


        if($request->hasFile('file'))
        {
            $file=$request->file('file');
            $filename=time() . '.' .$file->getClientOriginalExtension();//image intervention library use garera yo method use garna payo

            $path='uploads';
            $file->move($path,$filename);
            $categories->files=$filename;
        }
        $categories->save();
        Session::flash('success','Category was added successfully!');
        return redirect()->route('cat.index');  
    }


    public function update(Request $request)
    {

        $this->validate($request, array(
            'title'=>'required|max:255',
            'file'=>'sometimes|image',
            'description'=>'sometimes',

            ));

        $categories = Category::findorFail($request->category_id);
        $categories->category_name=$request->title;
        $categories->description=$request->description;

        if($request->hasFile('file'))
        {    
            unlink('uploads/'.$categories->files);
            $file=$request->file('file');
            $filename=time() . '.' .$file->getClientOriginalExtension();
            $path='uploads';
            $file->move($path,$filename);
            $categories->files=$filename;
        }

        Session::flash('success','Category was added successfully!');
        $categories->save();
        return redirect()->route('cat.index');  
    }  



    public function destroy(Request $request)
    {
        $categories = Category::findorFail($request->id);
        $categories->delete();
        if($categories->files!=null)
        {
            unlink('uploads/'.$categories->files);
        }
        return redirect()->route('cat.index');
    }

}
