<div id="load"></div>
<body>
 
<div id="wrapper">

  <style type="text/css">
    ul, menu, dir {
    display: block;
    list-style-type: disc;
  
}
  </style>
   


    <header>
        <div class="navbar navbar-default navbar-static-top" >
            <div class="container" style="height: 20px">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.html"><span>EZ</span>notes</a>
                    
                </div>
                <div class="navbar-collapse collapse ">
                    <ul class="nav navbar-nav">

                        <li class="">


                        <a href="{{route('home')}}"> Home</a></li>


                          
                                <li><a href="{{route('frontend.notes')}}">Notes</a></li>

                               <li><a href="{{route('question.banks')}}" > Questions</a></li>
               
                          
                        </li>
                        <li><a href="{{route('frontendposts.index')}}" > Blog</a></li>
                        <li><a href="{{route('thread.index')}}" >Forum</a></li>
                        <li><a href="{{URL::to('contribute')}}" >Contribute</a></li>
                        <li><a href="contact.html">About</a></li>

                     
                        @if(Auth::User())
                        <li class="dropdown">
                             <a href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false"><img src="{{asset('profile-images'.'/'.Auth::User()->picture)}}" height="30" width="30" style="border-radius: 50px;" style="color: #ddd; ">{{Auth::User()->name}} <i class="fa fa-caret-down" aria-hidden="true"></i> <b class=" icon-angle-down"></b></a>
                            <ul class="dropdown-menu">
                                 <style type="text/css">
                          a:hover {
                            color: black;
              }
                        </style>
                                <li><a href="{{route('user.profile')}}" style="color: #ddd; "><i class="fa fa-user" aria-hidden="true"></i> Profile</a></li>
                                <li> <a href="{{ route('logout') }}"

                                             onclick="event.preventDefault();
                                                      document.getElementById('logout-form').submit();" style="color: #ddd; ">
                                            <i class="fa fa-sign-out" aria-hidden="true"></i> Logout
                                         </a>

                                         <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                             {{ csrf_field() }}
                                         </form>
                                  </li>
               
                            </ul>
                        </li>
                        @endif
                      
                       
              
                    </ul>
                </div>
            </div>
        </div>
    </header>