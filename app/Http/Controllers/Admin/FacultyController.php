<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Faculty;
use App\Category;
use Session;
use Storage;

class FacultyController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $faculties = Faculty::join('categories','faculties.category_id','categories.category_id')->select('faculties.*','categories.category_name')->orderBy('faculty_id','desc')->get();

        $categories = Category::all()->pluck('category_name','category_id');
    	return view('admin.faculty.index')->with('categories',$categories)->with('faculties',$faculties);

    }
     public function search(Request $request)
    {
         $faculties = Faculty::where('faculty_name','LIKE','%'.$request->key.'%')->join('categories','faculties.category_id','categories.category_id')->select('faculties.*','categories.category_name')->orderBy('faculty_id','desc')->get();

        $categories = Category::all()->pluck('category_name','category_id');
        return view('admin.faculty.index')->with('categories',$categories)->with('faculties',$faculties);

    }
    public  function store(Request $request)
    {
    	 $this->validate($request, array(
            'faculty_name'=>'required|max:100',
            'files'=>'sometimes|image',
            'faculty_description'=>'sometimes',
            
        ));
     //store in the database
    $faculties=new Faculty;
    $categories=new Category;
    $faculties->faculty_name=$request->faculty_name;
    $faculties->description=$request->faculty_description;
    $faculties->category_id=$request->category_id;
    //save data
        if($request->hasFile('files')){
        $file=$request->file('files');
        $filename=time() . '.' .$file->getClientOriginalExtension();//image intervention library use garera yo method use garna payo

        $path='uploads';
        $file->move($path,$filename);
         
        $faculties->files=$filename;
        }
  
        $faculties->save();

    Session::flash('success','Faculty was added successfully!');

    return redirect()->route('faculty.index');  
    }
     public function destroy(Request $request)
    {       
             
        $faculties = Faculty::findorFail($request->id);
        if($faculties->files!=null){
        unlink('uploads/'.$faculties->files);
        }
        $faculties->delete();

        return redirect()->route('faculty.index');
    }

    public function update(Request $request)
    {
         $this->validate($request, array(
            'faculty_name'=>'required|max:100',
            'files'=>'sometimes|image',
            'faculty_description'=>'sometimes',
            'category_id'=>'required',
            
        ));

        $faculties=Faculty::find($request->id);
        $faculties->faculty_name=$request->faculty_name;
        $faculties->description=$request->faculty_description;
        $faculties->category_id=$request->category_id;

        if($request->hasFile('files')){

         if($faculties->files!=null){    
        unlink('uploads/'.$faculties->files);
        }
        $file=$request->file('files');
        $filename=time() . '.' .$file->getClientOriginalExtension();//image intervention library use garera yo method use garna payo

        $path='uploads';
        $file->move($path,$filename);
         
        $faculties->files=$filename;

        }
        
        
        $faculties->save();

        return redirect()->route('faculty.index');
    }

}


