<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Input;
use App\Subject;
use App\Note;
use App\Category;
use App\Faculty;
use Session;
use File;
class NoteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        $faculties = Faculty::all();
    	$subjects = Subject::all();
        $notes = Note::join('categories','notes.category','categories.category_id')->join('faculties','notes.faculty','faculties.faculty_id')->select('notes.*','faculties.faculty_name','categories.category_name')->orderBy('id','desc')->paginate(10);
        return view('admin.notes.index')->with('subjects',$subjects)->with('notes',$notes)->with('categories',$categories)->with('faculties',$faculties);
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
             $this->validate($request, array(
                'file'   => 'mimes:doc,pdf,docx,pptx,ppt,txt,zip',
                'category_id' => 'required',
                'faculty_id' => 'required',
                'semester' => 'required',
                'subject_id' => 'required',
                'title' => 'required',
            ));
        $note = new Note;
        $note->category=$request->category_id;
        $note->faculty=$request->faculty_id;
        $note->semester=$request->semester;
        $note->subject_id = $request->subject_id;
        $note->title = $request->title;
        if($request->hasFile('file')){
            $file = $request->file('file');
            $filename = time().rand(111,999). '.' . $file->getClientOriginalExtension();
            $path = public_path('notes/');
            $file->move($path,$filename);
            $note->file = $filename;
        }
        $note->save();

        Session::flash('success','Note was added successfully!');

        return redirect()->route('notes.index'); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        // dd($request);
        $this->validate($request, array(
            'titleedit'=>'required|max:255',
            'semesteredit'=>'required',
            'file'=>'required',
            'facultyedit'=>'required',
            'subjectedit'=>'required',
            'categoryedit'=>'required'
        ));
        $note = Note::find($request->id);
        $note->category=$request->categoryedit;
        $note->faculty=$request->facultyedit;
        $note->semester=$request->semesteredit;
        $note->subject_id = $request->subjectedit;
        $note->title = $request->titleedit;
        if($request->hasFile('file')){
            $file = $request->file('file');
            $filename = time().rand(111,999). '.' . $file->getClientOriginalExtension();
            $path = public_path('notes/');
            $file->move($path,$filename);
            unlink($path.$note->file);
            $note->file = $filename;
        }
        $note->save();

        Session::flash('success','Note was edited successfully!');

        return redirect()->route('notes.index'); 
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteNote(Request $request)
    {
        $note = Note::findorFail($request->id);

        File::deleteDirectory('notes/'.$note->file );

        $note->delete();

        return;
    }

    public function filter(Request $request)
    {
        $subject = Subject::find($request->subject);

        if(!$subject) abort(404);

        $notes = $subject->notes()->paginate(10);

        $subjects = Subject::all();

        return view('admin.notes.index')->with('subjects',$subjects)->with('notes',$notes); 
    }

    public function GetFaculty($id)
    {
        $faculties=Faculty::where('category_id','=',$id)->get();                         
        return response()->json(['faculties'=>$faculties]);
    }

    public function GetSubject()
    {
        $faculty=Input::get('faculty');
        $semester=Input::get('semester');
        $subjects=Subject::where('faculty_id','=',$faculty)->where('semester','=',$semester)->get();
        return response()->json(['subjects'=>$subjects]);
    

    }
}

