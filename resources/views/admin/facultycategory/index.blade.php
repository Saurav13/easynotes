@extends('admin.admin')



@section('body')

 <div class="container-fluid">
<h3>Categories</h3>
<div class="row">
<div class="col-md-2">
<button name="addnew" id="btnt" value="Add Categrory" class="form-control btn btn-sm btn-primary" ><i class="fa fa-plus" aria-hidden="true"></i> Add Category</button>
</div>
</div>
<br>
	<div class="card">


		 <div class="content table-responsive table-full-width">
                        <table class="table table-hover table-striped">
				<thead>
				<tr>
					<th>ID</th>
					<th>Title</th>
					<th>Description</th>
					<th>Image</th>
					<th>Action</th>
				</tr>
				</thead>
				<tbody>
				@foreach($categories as $category)
				<tr>
				<td >{{$category->category_id}}</td>
				<td id="name{{$category->category_id}}">{{$category->category_name}}</td>
				<td id="desc{{$category->category_id}}">{{$category->description}}</td>
				<td>{{$category->files}}</td>
			
					<td><input type="button" data-id="{{$category->category_id}}" class="btn btn-md btn-primary" value="Edit" id="update{{$category->category_id}}"  data-toggle="modal" data-target="#editmodal"/>
				
					
					<input type="button" cid="{{$category->category_id}}" class="btn btn-md btn-danger" value="Delete" id="delete{{$category->category_id}}"/>

				</td>
				</td>
				</tr>
				@endforeach
				</tbody>
			</table>
		</div>
		
	
</div>
</div>

@stop
<!-- Modal -->







	
                 <div class="modal" id="editmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Edit Categories</h4>
                      </div>
                      <div class="modal-body">
                      <form action="{{route('cat.update')}}" method="POST" enctype="multipart/form-data">
                      <input type="hidden" name="category_id" id="cate_id">
                      {{csrf_field()}}
				  Title:<br>
				  <input type="text" id="titlevalue" name="title" class="form-control"><br>
				  Description:<br>
				  <textarea rows="4" cols="50" id="description1" class="form-control" name="description" style="height: 174px; width: 100% " >
				  </textarea>
				  <br>
				 

				  Upload Image:
				<input type="file" name="file">
				 
				 
				 <br>
                      </div>
                      <div class="modal-footer"> 
                        <button type="submit" class="btn btn-success" id="saveedit">Save changes</button>
                      </div>
                      </form>
                    </div>
                  </div>
                </div>


                <div class="modal" id="modal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="closemodal2"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel1">Add Categories</h4>
                      </div>
                      <div class="modal-body">
					{!! Form::open(['route'=>'cat.store','files' => 'true','enctype'=>'multipart/form-data','method'=>'POST']) !!}
				
				  Title:<br>
				  <input type="text" name="title" class="form-control" id="cattitle"><br>
				  Description:<br>
				  <textarea id="catdesc" rows="4" cols="50" class="form-control" name="description" style="height: 174px; width: 100%" >
				  </textarea>
				  <br>

				  Upload Image:
				<input type="file" name="file" id="catimage">
				 
				 
				 <br>
				<button class="btn btn-md btn-success" id="savecat">Submit Details</button>
				 {!! Form::close() !!}
				</div>
				</div>
				</div>
				</div>

				<script
  src="https://code.jquery.com/jquery-3.2.1.min.js"
  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  crossorigin="anonymous"></script>

  {{csrf_field()}}
<script type="text/javascript">
	$(document).ready(function(){
		// console.log($('#td').html())
		$('[id *= "delete"]').click(function(event){
			
			var id= $(this).attr('cid');
			
			swal({
			  title: "Are you sure?",
			  text: "You will not be able to recover this file!",
			  type: "warning",
			  
			  showCancelButton: true,
			  
			}).then(function(){
				$.post("{{route('cat.destroy')}}",{id:id,_token:"{{csrf_token()}}"},function(data){
					swal({
						title:"Deleted Successfully",
						type:"success"

					}).then(function(){
						window.location.reload();
					});
				});
			});
		});
		
		$('#btnt').on('click',function(){
			$('#modal2').modal('toggle');
			console.log('ready');
		});
		$('[id *= "update"]').click(function(){
			var id=$(this).attr('data-id');
			console.log(id);
			var name = $('#name'+id).html();
			var desc= $('#desc'+id).html();
			console.log(desc,name);
			$('#titlevalue').val(name);
			$('#description1').html(desc);
			$('#cate_id').val(id);
			
	});
	});



		

		
</script>
