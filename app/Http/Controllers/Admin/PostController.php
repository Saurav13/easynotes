<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Post;
use App\Category;
use Image;
use Session;
use Storage;
class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

     public function index()
    {
        
        // create a variable and store all the blog posts from the database in  it
        $posts=Post::orderBy('id','desc')->paginate(10);
        //return a view and pass in the aboove variable into the view
        return view('admin.posts.index')->with('posts',$posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
     	$posts=Post::all();
        return view('admin.posts.create')->with('posts',$posts);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


     $this->validate($request, array(
            'title'=>'required|max:255',
            'description'=>'required',
            'image'=>'sometimes|image',
            'alias'=>'required|alpha_dash|unique:posts,alias'
    
        ));
     //store in the database
    $posts=new Post;
    $posts->title=$request->title;
    $posts->description=$request->description;
    $posts->alias=$request->alias;
    if($request->hasFile('image')){
        $file = $request->file('image');
        $filename = time().rand(111,999). '.' . $file->getClientOriginalExtension();
        $path = public_path('uploads/'.$filename);;
        Image::make($file)->resize(800, 450)->save($path);
        $posts->image = $filename;
    }
   
    $posts->save();

    Session::flash('success','The Post was successfully saved!');

     //redirect to another page
    return redirect()->route('post.index');

    }
    
    public function show($id)
    {
        $post= Post::find($id);
        return view('admin.posts.show')->withPost($post);//with('post',$post);yesari ne milcha
    }

    
   
     
    public function edit($id)
    {
        //find post and save it in a variable and pass into the view
        $post= Post::find($id);
        $categories=Category::all();
        return view('admin.posts.edit')->withPost($post)->withCategories($categories);
    }

    
    
     
    public function update(Request $request, $id)
    {
        //validate the data
        $post=Post::find($id);

         $this->validate($request, array(
            'title'=>'required|max:255',
            'description'=>'required',
            'image'=>'image',
            ));

    
        //save the data to the database
        $post= Post::find($id);
        $post->title=$request->input('title');//input will get paramaeters from either the get or post
        $post->description=$request->description;
        if($post->alias!=$request->alias)
        {
             $this->validate($request,array(
                'alias'=>'required|alpha_dash|unique:events,alias'

            ));
            
        }

        $post->alias=$request->alias;
        if ($request->hasFile('image')){
        //add the new photo 
        $image=$request->file('image');
        $filename=time() . '.' .$image->getClientOriginalExtension();//image intervention library use garera yo method use garna payo
        $location=public_path('uploads/'. $filename);
        Image::make($image)->resize(800,400)->save($location); 
        $oldFilename=$post->image;
        //update database
        $post->image=$filename;    
        //delete old photo
        unlink('uploads/'.$oldFilename);
        }

        $post->save();
       
        
        
        //set flash data with success message
         Session::flash('success','The blog post was successfully Updated!');

        //redirect with flash data to posts.show
        return redirect()->route('post.index');     
    }

    public function destroy($id)
    {
        $post= Post::find($id);
        // Storage::delete($post->image);
        if($post->image!=null){
        unlink('uploads/'.$post->image);
        }
         $post->delete();
       
        Session::flash('success','The post was successfully deleted');

        return redirect()->route('post.index');
    }
}

