<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Thread;
use App\Comment;
use Auth;
use DB;
use Counter;

class ProfileController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
    public function profile()
    {
        Counter::showAndCount('user-profile', Auth::User()->id);
    	$id=Auth::User()->id;
    	$follows=DB::table('follows')->where('user_id',$id)->join('users','users.id','=','follows.followed_id')
    							   ->select('users.name','users.id','follows.dir')->get();
    	
        $threads=Thread::orderBy('id','desc')->where('user_id','=',$id)->orderBy('id','desc')->get();
        $solution=Thread::where('user_id','=',$id)->where('solution','!=',null)->get();
        $replies=Comment::orderBy('id','desc')->where('user_id','=',$id)->get();
        $lr=Comment::orderBy('id','desc')->where('user_id','=',$id)->where('commentable_type','=','App\Thread')->pluck('commentable_id');
        $latestrep=count($lr);
        if($latestrep == 0)
        {
        $latestreplies=null;
        }
        else{
        $latestreplies=Thread::orderBy('id','desc')->where('id','=',$lr)->get();
        }
        
        $countreplies=count($replies);
        $countsolution=count($solution);
        $countthreads=count($threads);
        $latestthreads=$threads->take(3);
        return view('frontend.profile.index')->with('follows',$follows)->with('countthreads',$countthreads)->with('countsolution',$countsolution)->with('countreplies',$countreplies)->with('latestthreads',$latestthreads)->with('latestreplies',$latestreplies);

    }

    public function EditProfile(Request $request)
    {
        Counter::showAndCount('user-profile', Auth::User()->id);

        $user=User::find($request->id);
        $user->name=$request->name;
        $user->gender=$request->name;
        $user->address=$request->address;
        $user->institution=$request->work;
        $user->contact=$request->contact;
        $user->about=$request->about;

        if($request->hasFile('picture'))
        {     
        $file=$request->file('picture');
        $filename=time() . '.' .$file->getClientOriginalExtension();
        $path='profile-images';
        $file->move($path,$filename);
        $oldFilename=$user->picture;
        $user->picture=$filename;
        unlink('profile-images/'.$oldFilename);
        }
         $user->save();
        return redirect()->route('user.profile');
        
    }
}
