<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Auth;
use App\User;
use File;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return Response
     */
    public function handleProviderCallback($provider)
    {
        $socialuser = Socialite::driver($provider)->user();
        // return $socialuser->name;
        $finduser =  User::where('email',$socialuser->email)->first();
        if($finduser)
        {
        Auth::login($finduser);
        return redirect()->intended(route('home'));

        }
        else
        {
    
        $user = new User;    
        $user->name=$socialuser->name;
        $user->email=$socialuser->email;
        $user->picture=$socialuser->id . ".jpg";
        $fileContents = file_get_contents($socialuser->avatar_original);
        File::put(public_path() . '/profile-images/' . $socialuser->id . ".jpg", $fileContents);
        $user->save();
        Auth::login($user);
        return redirect()->intended(route('home'));

        }
    }
}
