<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Group;
use Auth;
use App\Thread;
use App\User;
use App\Comment;

class ProfileController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
    }

    public function create_group(Request $request){
    	
    	$group = new Group;

    	$group->name = $request->name;
    	$group->member_no = 1;
    	$group->save();
    	$group->members()->save(Auth::user());

    	return redirect()->route('groups.group',$group->id);
    }

   
}
