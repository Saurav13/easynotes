var k = angular.module('DirApp',[]);
k.controller('DirController',function($scope,$http){
	$scope.astab=false;
	$scope.selecteditem;
	$scope.tempfolders=[]
	$scope.tempfiles=[]
	$scope.tempasfolders=[]
	$scope.tempasfiles=[]
	
	$scope.files=[]
	$scope.folders=[]
	$scope.paths=[]
	$scope.path;
	$scope.fofiles=[]
	$scope.fofolders=[]
	$scope.fopaths=[]
	$scope.fopath;
	$scope.asfiles=[]
	$scope.asfolders=[]
	$scope.aspaths=[]
	$scope.aspath;
	
	$scope.dirs = [];
	$scope.asdirs = [];
	$scope.fodirs = [];

	$scope.delsel
	$scope.delt=0


	$scope.dirname=""
	$scope.diraddress="";
	

	$scope.bodyclick=false;
	$scope.open=function(name){
		$('[id *= "sel"]').removeAttr("style");
		$('[id *= "fsel"]').removeAttr("style");
		
		$scope.delt=0;
		$http({method: "POST",url: "/dir/open",params:{'path' : $scope.path+'/'+ name}

		}).then(function success(response){
			// console.log(response);
				$scope.files=response.data.files;
				$scope.folders=response.data.folders;
				$scope.tempfiles=$scope.files.slice()
				$scope.tempfolders=$scope.folders.slice()
			
				$scope.path=response.data.path;
				$scope.paths=$scope.path.split('/');
				// console.log(response);
		}, function error(response){

		})
	}
	$scope.foopen=function(name){

		$http({method: "POST",url: "/dir/foopen",params:{'path' : $scope.fopath+'/'+ name}

		}).then(function success(response){
			// console.log(response);
				$scope.fofiles=response.data.files;
				$scope.fofolders=response.data.folders;
				$scope.fopath=response.data.path;
				$scope.fopaths=$scope.fopath.split('/');

				// console.log(response);
		}, function error(response){

		})
	}
	$scope.asopen=function(name){
		$('[id *= "asel"]').removeAttr("style");
		$('[id *= "afsel"]').removeAttr("style");
		
		$scope.delt=0;
		$http({method: "POST",url: "/dir/asopen",params:{'path' : $scope.aspath+'/'+ name,'name':name}

		}).then(function success(response){
			// console.log(response);
				$scope.asfiles=response.data.files;
				$scope.asfolders=response.data.folders;
				$scope.tempasfiles=$scope.asfiles.slice()
				$scope.tempasfolders=$scope.asfolders.slice()
			
				$scope.aspath=response.data.path;
				$scope.aspaths=$scope.aspath.split('/');

				// console.log(response);
		}, function error(response){

		})
	}

	$scope.opentree=function(path)
	{
		$('[id *= "sel"]').removeAttr("style");
		$('[id *= "fsel"]').removeAttr("style");
		
		$scope.delt=0;
		$http({method: "POST",url: "/dir/open",params:{'path' : path}

		}).then(function success(response){
			// console.log(response);
				$scope.files=response.data.files;
				$scope.folders=response.data.folders;
				$scope.tempfiles=$scope.files.slice()
				$scope.tempfolders=$scope.folders.slice()
			
				$scope.path=response.data.path;
				$scope.paths=$scope.path.split('/');
				// console.log(response);
		}, function error(response){

		})
	}
	$scope.foopentree=function(path)
	{
		// console.log(path);
		$http({method: "POST",url: "/dir/foopen",params:{'path' : path}

		}).then(function success(response){
			// console.log(response);
				$scope.fofiles=response.data.files;
				$scope.fofolders=response.data.folders;
				$scope.fopath=response.data.path;

				$scope.fopaths=$scope.fopath.split('/');
				// console.log(response);
		}, function error(response){

		})
	}
	$scope.asopentree=function(path)
	{
		$('[id *= "afsel"]').removeAttr("style");
		$('[id *= "asel"]').removeAttr("style");
		$scope.delt=0
		console.log(path);
		$http({method: "POST",url: "/dir/asopen",params:{'path' : path}

		}).then(function success(response){
			// console.log(response);
				$scope.asfiles=response.data.files;
				$scope.asfolders=response.data.folders;
				$scope.tempasfiles=$scope.asfiles.slice()
				$scope.tempasfolders=$scope.asfolders.slice()
			
				$scope.aspath=response.data.path;

				$scope.aspaths=$scope.aspath.split('/');
				// console.log(response);
		}, function error(response){

		})
	}
	$scope.selected=function(p,t){
		$scope.bodyclick=true
		$('[id *= "sel"]').removeAttr("style");
		$('[id *= "fsel"]').removeAttr("style");
		$('[id *= "asel"]').removeAttr("style");
		$('[id *= "afsel"]').removeAttr("style");
		
		var dsel;
		var sel;
		var dlink;

		if (t==1) {
			$scope.renamefolder=p
			dsel="df"
			sel="sel"
			dlink="fff"
		}
		if(t==2)
		{
			dlink="fff"
			sel="fsel"
			dsel="fdf"
		}
		if(t==3)
		{
			dlink="afff"
			$scope.dirname=$scope.asfolders[p]
			sel='asel'
			dsel='adf'
			$scope.Address($scope.asfolders[p]);
			
		}
		if(t==4)
		{
			dlink="afff"
			sel='afsel'
			dsel='afdf'
		}
		var href=$('#'+dsel+p).attr('href');
		// console.log(href)
		$('#'+dlink).attr('href',href);
		// console.log($('#fff').attr('href'));
		$('#'+sel+p).attr("style","background-color:#4492d6; color:white;");

		$scope.delsel=p
		$scope.delt=t

	}
	$scope.back=function(p){
		var path=$scope.paths.slice(0,p+1);
		// $scope.hide()
		$('[id *= "sel"]').removeAttr("style");
		$('[id *= "fsel"]').removeAttr("style");
		
		$scope.delt=0;
		// console.log(path);
		$http({method: "POST",url: "/dir/open",params:{'path' : path.join('/')}

		}).then(function success(response){
			// console.log(response);
				$scope.files=response.data.files;
				$scope.folders=response.data.folders;
				$scope.tempfiles=$scope.files.slice()
				$scope.tempfolders=$scope.folders.slice()
			
				$scope.path=response.data.path;
				$scope.paths=$scope.path.split('/');
				// console.log(response);
		}, function error(response){

		})
	}
	$scope.foback=function(p){
		var path=$scope.fopaths.slice(0,p+1);

		// console.log(path);
		$http({method: "POST",url: "/dir/foopen",params:{'path' : path.join('/')}

		}).then(function success(response){
			// console.log(response);
				$scope.fofiles=response.data.files;
				$scope.fofolders=response.data.folders;
				$scope.fopath=response.data.path;
				$scope.fopaths=$scope.fopath.split('/');
				// console.log(response);
		}, function error(response){

		})
	}
	$scope.asback=function(p){
		var path=$scope.aspaths.slice(0,p+1);
		$('[id *= "asel"]').removeAttr("style");
		$('[id *= "afsel"]').removeAttr("style");
		
		$scope.delt=0;
		
		// console.log(path);
		$http({method: "POST",url: "/dir/asopen",params:{'path' : path.join('/')}

		}).then(function success(response){
			// console.log(response);
				$scope.asfiles=response.data.files;
				$scope.asfolders=response.data.folders;
				$scope.tempasfiles=$scope.asfiles.slice()
				$scope.tempasfolders=$scope.asfolders.slice()
			
				$scope.aspath=response.data.path;
				$scope.aspaths=$scope.aspath.split('/');
				// console.log(response);
		}, function error(response){

		})
	}
	function mouseX(evt) {
    if (evt.pageX) {
        return evt.pageX;
    } else if (evt.clientX) {
       return evt.clientX + (document.documentElement.scrollLeft ?
           document.documentElement.scrollLeft :
           document.body.scrollLeft);
    } else {
        return null;
    }
}

function mouseY(evt) {
    if (evt.pageY) {
        return evt.pageY;
    } else if (evt.clientY) {
       return evt.clientY + (document.documentElement.scrollTop ?
       document.documentElement.scrollTop :
       document.body.scrollTop);
    } else {
        return null;
    }
}
	$scope.range = function(min, max, step) {
    step = step || 1;
    var input = [];
    for (var i = min; i <= max; i += step) {
        input.push(i);
    }
    return input;
};

	$scope.lkinit=function()
	{
		$('[id *= "sel"]').removeAttr("style");
  		$('[id *= "fsel"]').removeAttr("style");
  		
  		$scope.delt=0;   

		$http({method: "POST",url: "/dir/root"
		}).then(function success(response){
				$scope.files=response.data.files;
				$scope.folders=response.data.folders;
				$scope.tempfiles=$scope.files.slice()
				$scope.tempfolders=$scope.folders.slice()
			
				$scope.path=response.data.path;
				$scope.paths=$scope.path.split('/');
				$scope.dirs=response.data.dirs;				
				// console.log(response);
		}, function error(response){

		});
	}
	$scope.folkinit=function()
	{
		var id=$('#userid').val();
		var dir=$('#dir').val();
		$http({method: "POST",url: "/dir/foroot",params:{'id':id,'dir':dir}

		}).then(function success(response){
				$scope.fofiles=response.data.files;
				$scope.fofolders=response.data.folders;
				$scope.fopath=response.data.path;
				$scope.fopaths=$scope.fopath.split('/');
				// console.log($scope.fofiles);
		}, function error(response){

		});
	}
	$scope.asinit=function()
	{
		$('[id *= "asel"]').removeAttr("style");
  		$('[id *= "afsel"]').removeAttr("style");
  		
  		$scope.delt=0;   

		$scope.astab=true;
		$http({method: "POST",url: "/dir/asroot"

		}).then(function success(response){
				$scope.asfiles=response.data.files;
				$scope.asfolders=response.data.folders;
				$scope.aspath=response.data.path;
				$scope.tempasfiles=$scope.asfiles.slice()
				$scope.tempasfolders=$scope.asfolders.slice()
			
				$scope.aspaths=$scope.aspath.split('/');
				$scope.asdirs=response.data.dirs;
				console.log(response.data.dirs);
				// console.log(response);
		}, function error(response){

		});

	}
	$scope.init=function(){
		$http({method: "POST",url: "/dir/root"

		}).then(function success(response){
				$scope.files=response.data.files;
				$scope.folders=response.data.folders;
				$scope.tempfiles=$scope.files.slice()
				$scope.tempfolders=$scope.folders.slice()
			
				$scope.path=response.data.path;
				$scope.paths=$scope.path.split('/');
				$scope.dirs=response.data.dirs;
				console.log(response.data.dirs);
		}, function error(response){

		});
		$http({method: "POST",url: "/dir/foroot"

		}).then(function success(response){
				$scope.fofiles=response.data.files;
				$scope.fofolders=response.data.folders;
				$scope.fopath=response.data.path;
				$scope.fopaths=$scope.fopath.split('/');
				// console.log(response);
		}, function error(response){

		});

		// $http({method: "POST",url: "/dir/POSTtree"

		// }).then(function success(response){
		// 		$scope.dirs=response.data;
		// 		console.log(response);
		// }, function error(response){

		// });
		// $http({method: "POST",url: "/dir/foPOSTtree"

		// }).then(function success(response){
		// 		$scope.fodirs=response;
		// 		// console.log(response);
		// }, function error(response){

		// });
		// $http({method: "POST",url: "/dir/asPOSTtree"

		// }).then(function success(response){
		// 		$scope.asdirs=response.data;
		// 		// console.log(response);
		// }, function error(response){

		// });

	}
	$scope.renametarget;
	$scope.PRename=function(f){
		$scope.renametarget=f;
	}
	$scope.showoptions=function(t){
		// console.log(t)
		if(t=='rnm')
		{
			if($scope.delt==1)
				return true
		}
		else if(t=='del' || t=='dwn')
		{
			if($scope.delt!=0)
				return true
			else
				return false
		}
		else if(t=='vwa')
		{
			if($scope.delt!=0)
				return true
			else
				return false
		}
		
	}
	$scope.Rename= function(){
		var name=$scope.folders[$scope.renametarget];
		// console.log(name,$scope.newname);
		// $('[id *= "sel"]').removeAttr("style");
		// $('[id *= "fsel"]').removeAttr("style");
		
		$http({method: "POST",url: "/dir/renamefolder",params:{'name': name,'newname':$scope.newname,'path' :  $scope.path}

		}).then(function success(response){
				$scope.files=response.data.files;
				$scope.folders=response.data.folders;
				$scope.tempfiles=$scope.files.slice()
				$scope.tempfolders=$scope.folders.slice()
			
				$scope.path=response.data.path;
				$scope.paths=$scope.path.split('/');
				$scope.dirs=response.data.dirs;
				var message=response.data.success;
								// $('#newfolder').attr('style','display:none;');
				// console.log(response);
		}, function error(response){

		});
	}
	$scope.PreDelete=function(f,t){
		$scope.delsel=f;
		$scope.delt=t	
	}
	$scope.Delete=function(f,t){
		$('[id *= "sel"]').removeAttr("style");
		$('[id *= "fsel"]').removeAttr("style");
		$('[id *= "asel"]').removeAttr("style");
		$('[id *= "afsel"]').removeAttr("style");
		
		if(t==1)
		{
		$http({method: "POST",url: "/dir/deletefolder",data:{'name': $scope.folders[f],'path' :  $scope.path}

		}).then(function success(response){
				$scope.files=response.data.files;
				$scope.folders=response.data.folders;
				$scope.tempfiles=$scope.files.slice()
				$scope.tempfolders=$scope.folders.slice()
			
				$scope.path=response.data.path;
				$scope.paths=$scope.path.split('/');
				$scope.dirs=response.data.dirs;
				var message=response.data.success;
								// $('#newfolder').attr('style','display:none;');
				// console.log(response);
		}, function error(response){

		});
		}
		else if(t==2)
		{

			$http({method: "POST",url: "/dir/deletefile",data:{'name': $scope.files[f],'path' :  $scope.path}

		}).then(function success(response){
				$scope.files=response.data.files;
				$scope.folders=response.data.folders;
				$scope.tempfiles=$scope.files.slice()
				$scope.tempfolders=$scope.folders.slice()
			
				$scope.path=response.data.path;
				$scope.paths=$scope.path.split('/');
				$scope.dirs=response.data.dirs;
				var message=response.data.success;
								// $('#newfolder').attr('style','display:none;');
				// console.log(response);
		}, function error(response){

		});
		}
		else if(t==3)
		{
			// console.log($scope.asfolders[f],$scope.aspath);
			$http({method: "POST",url: "/dir/deleteasfolder",data:{'name': $scope.asfolders[f],'path' :  $scope.aspath}

		}).then(function success(response){
				$scope.asfiles=response.data.files;
				$scope.asfolders=response.data.folders;
				$scope.tempasfiles=$scope.asfiles.slice()
				$scope.tempasfolders=$scope.asfolders.slice()
			
				$scope.aspath=response.data.path;
				$scope.aspaths=$scope.aspath.split('/');
				$scope.asdirs=response.data.dirs;

				var message=response.data.success;
								// $('#newfolder').attr('style','display:none;');
				// console.log(response);
		}, function error(response){

		});
		
		}
		else if(t==4)
		{
			$http({method: "POST",url: "/dir/deletefile",data:{'name': $scope.asfiles[f],'path' :  $scope.aspath}

		}).then(function success(response){
				$scope.asfiles=response.data.files;
				$scope.asfolders=response.data.folders;
				$scope.tempasfiles=$scope.asfiles.slice()
				$scope.tempasfolders=$scope.asfolders.slice()
			
				$scope.aspath=response.data.path;
				$scope.aspaths=$scope.aspath.split('/');
				$scope.asdirs=response.data.dirs;
				var message=response.data.success;
								// $('#newfolder').attr('style','display:none;');
				// console.log(response);
		}, function error(response){

		});
		}
	}
	$scope.addFolder= function(){
		$('[id *= "sel"]').removeAttr("style");
		// $('[id *= "sel"]').removeAttr("style");
		
		var name=$('#foldername').val();
		$http({method: "POST",url: "/dir/addfolder",params:{'name':name,'path' :  $scope.path}

		}).then(function success(response){
				$scope.files=response.data.files;
				$scope.folders=response.data.folders;
				$scope.tempfiles=$scope.files.slice()
				$scope.tempfolders=$scope.folders.slice()
			
				$scope.path=response.data.path;
				$scope.paths=$scope.path.split('/');
				$scope.dirs=response.data.dirs;
				var message=response.data.success;
				
				for(var i=0;i<$scope.folders.length;i++)
				{

					if($scope.folders[i]==name)
					{
						console.log(i,name)
						$scope.selected(i,1);
					}
						
						
				}
				alert('successfully created');
								// $('#newfolder').attr('style','display:none;');
				// console.log(response);
		}, function error(response){
				alert('something went wrong');
		});
	}
	$scope.hide= function(){
		for(var i=0;i<$scope.folders.length;i++)
		{
			$('#rmenu'+i).attr('class','hide');
		}

		for(var i=0;i<$scope.files.length;i++)
		{
			$('#frmenu'+i).attr('class','hide');
		}
		for(var i=0;i<$scope.fofolders.length;i++)
		{
			$('#formenu'+i).attr('class','hide');
		}
		for(var i=0;i<$scope.fofiles.length;i++)
		{
			$('#fofrmenu'+i).attr('class','hide');
		}
		for(var i=0;i<$scope.asfolders.length;i++)
		{
			$('#asrmenu'+i).attr('class','hide');
		}
		for(var i=0;i<$scope.asfiles.length;i++)
		{
			$('#asfrmenu'+i).attr('class','hide');
		}

		if($scope.bodyclick==false)
		{
			for(var i=0;i<$scope.folders.length;i++)
			{
				$('#sel'+i).removeAttr('style');
			}
			for(var i=0;i<$scope.files.length;i++)
			{
				$('#fsel'+i).removeAttr('style');
			}
			for(var i=0;i<$scope.asfolders.length;i++)
			{
				$('#asel'+i).removeAttr('style');
			}
			for(var i=0;i<$scope.asfiles.length;i++)
			{
				$('#afsel'+i).removeAttr('style');
			}
		}
		$scope.bodyclick=false
	}
	$scope.rightclick = function(e,n,t){
		$('[id *= "sel"]').removeAttr("style");
		$('[id *= "fsel"]').removeAttr("style");
		$('[id *= "asel"]').removeAttr("style");
		$('[id *= "afsel"]').removeAttr("style");
		$scope.delt=0
		var selector;
		var sel;
		if(t==1){
			selector='#rmenu';
			sel='sel';
			}
		else if (t==2)
		{
			selector='#frmenu';
			sel='fsel';
		}
			
		else if (t==3)
			selector='#formenu';
		else if (t==4)
			selector='#fofrmenu';
		else if (t==5)
			selector='#asrmenu';
		else if (t==6)
			selector='#asfrmenu';
		$scope.hide();
		if(e.which==3)
		{
			$(sel+n).attr("style","background-color:blue");
			// console.log($(sel+n).val())
			$(selector+n).attr('class','show1');
    		var a=mouseX(e);
      		var b= mouseY(e);
      		$(selector+n).attr('style','left:'+a);
      		$(selector+n).attr('style','top:'+b);
		}
		
	}
	$scope.tempmove;
	$scope.tempmovename;
	$scope.pastehide=true;
	$scope.Move=function(f,t)
	{
		if(t==1)
		{
			$scope.tempmove=$scope.path+'/'+$scope.folders[f];
			$scope.tempmovename=$scope.folders[f];
		}
		else if(t==2)
		{
			$scope.tempmove=$scope.path+'/'+$scope.files[f];
			$scope.tempmovename=$scope.files[f];
		}
		// console.log($scope.tempmove)
		$scope.pastehide=false;

	}
	$scope.Paste=function(f)
	{
		var newpath=$scope.path+'/'+$scope.folders[f];
		// console.log(newpath)
		$http({method: "POST",url: "/dir/movefolder",params:{'oldpath':$scope.tempmove,'newpath':newpath ,'path' :  $scope.path,'name':$scope.tempmovename}

		}).then(function success(response){
				$scope.files=response.data.files;
				$scope.folders=response.data.folders;
				$scope.tempfiles=$scope.files.slice()
				$scope.tempfolders=$scope.folders.slice()
			
				$scope.path=response.data.path;
				$scope.paths=$scope.path.split('/');
				$scope.dirs=response.data.dirs;
				var message=response.data.success;
				$scope.pastehide=true;
								// $('#newfolder').attr('style','display:none;');
				// console.log(response);
		}, function error(response){

		});
	}
	$scope.Extract=function(f){
		$http({method: "POST",url: "/dir/extractfile",params:{'path' :  $scope.path,'name':$scope.files[f].slice(0,-4)}

		}).then(function success(response){
				$scope.files=response.data.files;
				$scope.folders=response.data.folders;
				$scope.tempfiles=$scope.files.slice()
				$scope.tempfolders=$scope.folders.slice()
			
				$scope.path=response.data.path;
				$scope.paths=$scope.path.split('/');
				$scope.dirs=response.data.dirs;
				var message=response.data.success;
				$scope.pastehide=true;
								// $('#newfolder').attr('style','display:none;');
				// console.log(response);
		}, function error(response){

		});
	}
	$scope.UploadFile=function(token){
		var formData = new FormData();
		formData.append("file",$scope.file);
		formData.append('path',$scope.path);
		console.log($scope.file+"asdasd");
		// console.log(formData);
		 $http.post("/dir/uploadfile", formData, {
                  transformRequest: angular.identity,
                  headers: {'Content-Type': undefined}
               }).then(function success(response){
				$scope.files=response.data.files;
				$scope.folders=response.data.folders;
				$scope.tempfiles=$scope.files.slice()
				$scope.tempfolders=$scope.folders.slice()
			
				$scope.path=response.data.path;
				$scope.dirs=response.data.dirs;
				// $scope.paths=$scope.path.split('/');
				var message=response.data.success;
				$scope.pastehide=true;
								// $('#newfolder').attr('style','display:none;');
				// console.log(response);
		}, function error(response){

		});
		
	}
	$scope.ifZip= function(f)
	{
		var z = $scope.files[f].split('.');

		if(z[z.length-1]=='zip')
			return true;
		return false;
	}
	$scope.isFile=function(n)
	{
		return n.includes('.');
	}
	$scope.AsCreate=function()
	{
		var name=$('#asname').val();
		console.log($scope.asdirs);
		$http({method: "POST",url: "/dir/addassignment",params:{'name':name}

		}).then(function success(response){
				$scope.asfiles=response.data.files;
				$scope.asfolders=response.data.folders;
				$scope.tempasfiles=$scope.asfiles.slice()
				$scope.tempasfolders=$scope.asfolders.slice()
			
				$scope.aspath=response.data.path;
				$scope.aspaths=$scope.aspath.split('/');
				$scope.asdirs=response.data.dirs;
				console.log($scope.asdirs);
				var message=response.data.success;
								// $('#newfolder').attr('style','display:none;');
				// console.log(response);
		}, function error(response){

		});
	}
	$scope.Address=function(name)
	{
		console.log(name)
		$scope.diraddress="assignment+"+$('#useremail').val().split('@')[0]+"--"+name+"@projectincube.com";
		// console.log($scope.diraddress);
	}
	$scope.copyAddress=function ($event) {
 		$event.target.select();
		  // console.log($(element).val())
 		}
		$scope.search=function(key){
			$scope.folders=[]
			$scope.files=[]
			// console.log(key.length)
			for(var i=0;i<$scope.tempfolders.length;i++)
			{
				console.log($scope.tempfolders[i].slice(0,key.length).toLowerCase())
				if($scope.tempfolders[i].slice(0,key.length).toLowerCase()==key.toLowerCase())
					$scope.folders.push($scope.tempfolders[i])
			}
			for(var i=0;i<$scope.tempfiles.length;i++)
			{
				if($scope.tempfiles[i].slice(0,key.length).toLowerCase()==key.toLowerCase())
					$scope.files.push($scope.tempfiles[i])
			}
			if(key.length==0)
			{
				console.log("zero")
				console.log($scope.tempfolders)
				$scope.folders=$scope.tempfolders.slice();
				$scope.files=$scope.tempfiles.slice();
			}

		}
		$scope.assearch=function(key){
			$scope.asfolders=[]
			$scope.asfiles=[]
			// console.log(key.length)
			for(var i=0;i<$scope.tempasfolders.length;i++)
			{
				console.log($scope.tempasfolders[i].slice(0,key.length).toLowerCase())
				if($scope.tempasfolders[i].slice(0,key.length).toLowerCase()==key.toLowerCase())
					$scope.asfolders.push($scope.tempasfolders[i])
			}
			for(var i=0;i<$scope.tempasfiles.length;i++)
			{
				if($scope.tempasfiles[i].slice(0,key.length).toLowerCase()==key.toLowerCase())
					$scope.asfiles.push($scope.tempasfiles[i])
			}
			if(key.length==0)
			{
				console.log("zero")
				console.log($scope.tempasfolders)
				$scope.asfolders=$scope.tempasfolders.slice();
				$scope.asfiles=$scope.tempasfiles.slice();
			}		
		}
		$scope.isRoot=function()
		{
			// return true;
			// console.log($scope.paths[$scope.paths.length-1]+"idid"+$('#userid').val())
			if($scope.paths[$scope.paths.length-1]==$('#userid').val())
				return true;
			else return false;
		}
});


k.directive("fileread", [function () {
    return {
        scope: {
            fileread: "="
        },
        link: function (scope, element, attributes) {
            element.bind("change", function (changeEvent) {
                scope.$apply(function () {
                    console.log(changeEvent)
                    scope.fileread = changeEvent.target.files[0];
                    // or all selected files:
                    // scope.fileread = changeEvent.target.files;
                });
            });
        }
    }
}]);