@extends('frontend.main')
@section('body')

<div class="container " style="margin-bottom: 30px;">

        
        <hr>
        <div class="container containershadow">
    <h1>Become a Contributor/ Give us feedback</h1>
        <div class="alert alert-info ">
            <p>
                Lets
                <a href="https://laravel.io/forum" class="alert-link"> Build, Learn and Grow</a> together
                <a href="https://laravel.io/rules" class="alert-link"> Become a contributor. Give us feedback!</a> Provide us with any resources and we will add that to our site <a href="https://laravel.io/rules" class="alert-link"> giving you fell credit.</a>
            </p>
        </div>
                      
    <form action="" enctype="multipart/form-data" method="post">
    {{csrf_field()}}
    <div class="form-group">
        <label for="title">Title</label>
        <input class="form-control" required name="title" type="text" id="cattitle"  value="">
    </div>
    <div class="form-group">
        <label for="description">Description</label>
        <textarea class="form-control wysiwyg" required name="description" cols="50" rows="10" id="editor" value=""></textarea>
    </div>
  
      <input type="hidden" name="_token" value="{{csrf_token()}}">
      <br>

         
           

    	
      
    <input class="btn btn-theme " type="submit" value="Submit">
    {{-- <a href="{{route('thread.index')}}" class="btn btn-default btn-">Cancel</a> --}}
     </form>
     <br>
     </div>
    </div>
<script
      src="https://code.jquery.com/jquery-3.2.1.min.js"
      integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
      crossorigin="anonymous">
</script>


<script>
             CKEDITOR.replace( 'editor' );
</script>

@endsection