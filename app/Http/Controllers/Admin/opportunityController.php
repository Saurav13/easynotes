<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Opportunitie;
use Session;
use Image;

class OpportunityController extends Controller
{
    public function index()
    {
    	$opportunities=Opportunitie::all();
    	return view('admin.opportunities.index')->with('opportunities',$opportunities);
    }

    public function store(Request $request)
    {
    	    $this->validate($request,array(
    	        'title'=>'required|max:255',
    	        'description'=>'required',
                 'alias'=>'required|alpha_dash|unique:opportunities,alias'

    	    ));
    	$opportunities=new Opportunitie;
    	$opportunities->title=$request->title;
    	$opportunities->description=$request->description;
        $opportunities->alias=$request->alias;
    	if($request->hasFile('picture'))
    	{
    	    $file=$request->file('picture');
    	    $filename=time() . '.' .$file->getClientOriginalExtension();//image intervention library use garera yo method use garna payo

    	    $path='uploads/opportunitiesimages';
    	    // $file->move($path,$filename);
    	    $location=public_path('uploads/opportunitiesimages'.'/'.$filename);
            Image::make($file)->resize(530,320)->save($location); 
    	    $opportunities->picture=$filename;
    	}
    	$opportunities->save();

    	Session::flash('success','The Opportunities details were added Succesfully');
    	return redirect()->route('opportunities.index');
    }

    public function edit(Request $request)
    {
       $this->validate($request,array(
           'title'=>'required|max:255',
           'description'=>'required',

       ));
         $opportunities = Opportunitie::find($request->id);
         $opportunities->title=$request->title;
         $opportunities->description=$request->description;
           if($opportunities->alias!=$request->alias)
        {
             $this->validate($request,array(
                'alias'=>'required|alpha_dash|unique:opportunities,alias'

            ));
            
        }
        $opportunities->alias=$request->alias;
         if($request->hasFile('picture'))
         {
             if($opportunities->picture!=null){    
            unlink('uploads/opportunitiesimages/'.$opportunities->picture);
            }
             $file=$request->file('picture');
             $filename=time() . '.' .$file->getClientOriginalExtension();//image intervention library use garera yo method use garna payo

             $path='uploads/opportunitiesimages';
             // $file->move($path,$filename);
             $location=public_path('uploads/opportunitiesimages'.'/'. $filename);
             Image::make($file)->resize(530,320)->save($location); 
              
             $opportunities->picture=$filename;
         }
          $opportunities->save();
          Session::flash('success','The Details were Updated Succesfully');
          return redirect()->route('opportunities.index');
        
    }

    public function delete(Request $request)
    {
        $opportunities = Opportunitie::findorFail($request->id);
        if($opportunities->picture!=null){
        unlink('uploads/opportunitiesimages/'.$opportunities->picture);
        }
        $opportunities->delete();
        return redirect()->route('opportunities.index');
    }
}
