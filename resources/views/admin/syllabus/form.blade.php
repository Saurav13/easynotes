@extends ('admin.admin')


@section('body')
<div class="container-fluid">

	<div class="card">
		<div class="header">
			<h3>Add Syllabus</h3>
		</div>
		 <div class="container-fluid">
		 @if(Request::segment('4') == 'edit')
          {!! Form::model($syllabus,['url' => 'admin/syllabus'.'/'.$syllabus->id,'method'=>'PATCH','files'=>'true'])!!}
       
        @else
          {!! Form::open([ 'enctype'=>'multipart/form-data','url' => 'admin/syllabus','files'=>'true'])!!}
        @endif
        <label>Title</label>
        <input name="title" type="text" class="form-control" required placeholder="Title"/>
        <br>
        <label>Faculty</label>
          {!! Form::select('faculty_id',$faculties,null,['class'=>'form-control','required'=>'true']) !!}
        <br>
        <label>Semester</label>
        <select name="semester" class="form-control" required>
        	<option>1st</option>
        	<option>2nd</option>
        	<option>3rd</option>
        	<option>4th</option>
        	<option>5th</option>
        	<option>6th</option>
        	<option>7th</option>
        	<option>8th</option>
        </select>
        <br>
        <label>File</label>
        <input type="file" name="file" class="form-control" required/>
        <br>
         <input type="submit" class="btn btn-success btn-md" value="Save">
       <br>
          {!! Form::close() !!}
      <br>
      </div>
	</div>
</div>
@stop