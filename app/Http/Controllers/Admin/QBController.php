<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Questionbank;
use App\Faculty;
use Redirect;

class QBController extends Controller
{
    public function index()
    {
    	$qb=Questionbank::all();
    	return view('admin.qb.list',compact('qb'));
    }

    public function create()
    {
    	$faculties=Faculty::all()->pluck('faculty_name','faculty_id');
    	return view('admin.qb.form',compact('faculties'));
    }
    public function store(Request $request)
    {


     $this->validate($request, array(
            'title'=>'required|max:255',
            'semester'=>'required',
            'file'=>'required',
            'faculty_id'=>'required'
        ));
    	$q=new Questionbank;
    	$q->title=$request->title;
    	$q->semester=$request->semester;
    	$q->faculty_id=$request->faculty_id;
        $q->link="qqq";
        $q->save();
		$file=$request->file('file');

		$extension = $file->getClientOriginalExtension();
		$fileName = $q->id.'_'.$q->semester.'_'.$q->faculty_id.'.'.$extension;
		$file->move('uploads/qb',$fileName);
		$q->link=$fileName;
		$q->save();
		return Redirect::to('admin/qb');
    }
    public function edit($id)
    {
        $faculties=Faculty::all()->pluck('faculty_name','faculty_id');
        $qb=Questionbank::find($id);
        return view('admin.qb.form',compact('qb','faculties'));


    }
    public function update(Request $request, $id)
    {
         $this->validate($request, array(
            'title'=>'required|max:255',
            'semester'=>'required',
            'file'=>'required',
            'faculty_id'=>'required'
        ));
        $q = Questionbank::find($id);
        $q->title=$request->title;
        $q->semester=$request->semester;
        $q->faculty_id=$request->faculty_id;

        unlink('uploads/qb'.'/'.$q->link);
        $file=$request->file('file');

        $extension = $file->getClientOriginalExtension();
        $fileName = $q->id.'_'.$q->semester.'_'.$q->faculty_id.'.'.$extension;
        $file->move('uploads/qb',$fileName);
        $q->link=$fileName;
        $q->save();
        return Redirect::to('admin/qb');
    }
    public function destroy(Request $request)
    {
        $id=$request->id;
        $q=Questionbank::find($id);
        unlink('uploads/qb'.'/'.$q->link);
        $q->delete();
        return Redirect::to('admin/qb');
    }


}
