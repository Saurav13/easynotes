<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Note;
use App\Faculty;
use App\Subject;
use App\Thread;
use App\Questionbank;
use App\Syllabus;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $total_users=User::count();
        $total_notes=Note::count();
        $total_faculties=Faculty::count();
        $total_subjects=Subject::count();
        $total_threads=Thread::count();
        $total_qbs=Questionbank::count();
        $total_syllabus=Syllabus::count();
        $note_size=$this->getTotalNoteSize()/(1024*1024);
        $qb_size=$this->getTotalQbSize()/(1024*1024);
        $syllabus_size=$this->getTotalSyllabusSize()/(1024*1024);
        
        return view('admin.dashboard',compact('total_users','total_notes','total_faculties','total_subjects','total_threads','total_qbs','total_syllabus','note_size','qb_size','syllabus_size'));
    }

    public function store()
    {
         $this->validate($request, array(
            'faculty_name'=>'required|max:100',
            ''=>'required',
        ));
     //store in the database
    $subjects=new Subject;
    $faculties=new Faculty;
    $subjects->subject_name=$request->subject_name;
    $subjects->faculty_id=$request->faculty_id;
    $subjects->semester=$request->semester;
    //save data
  
    $subjects->save();

    Session::flash('success','The Subject was successfully added!');

    return redirect()->route('subject.create');
    }

    private function getTotalNoteSize()
    {
        $notes=Note::all();
        $size=0;
        foreach($notes as $n)
        {
            $path = public_path('notes/' . $n->file);
            $size=$size+filesize($path);
            // dd(filesize($n->file));
        }
        return $size;
    }
    private function getTotalQbSize()
    {
        $notes=Questionbank::all();
        $size=0;
        foreach($notes as $n)
        {
            $path = public_path('uploads/qb' . $n->file);
            $size=$size+filesize($path);
            // dd(filesize($n->file));
        }
        return $size;
    }
     private function getTotalSyllabusSize()
    {
        $notes=Syllabus::all();
        $size=0;
        foreach($notes as $n)
        {
            $path = public_path('uploads/syllabus' . $n->file);
            $size=$size+filesize($path);
            // dd(filesize($n->file));
        }
        return $size;
    }
}
