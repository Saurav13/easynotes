<?php

use Illuminate\Database\Seeder;

class adminseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->insert([
            'name' => "Incube",
            'email' => 'incube@gmail.com',
            'password' => bcrypt('123456'),
        ]);
    }
}
