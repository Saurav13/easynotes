<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contribution;
class ContributionController extends Controller
{
    public function index()
    {
    	$contributions=Contribution::orderBy('created_at', 'desc')->get();

    	return view('admin.contribution.list')->with('contributions',$contributions);
    }
    public function view($id)
    {
    	$contribution=Contribution::where('id',$id)->first();
    	return view('admin.contribution.view')->with('contribution',$contribution);
    }
    public function delete($id)
    {
    	Contribution::where('id',$id)->delete();
    	return "success";
    }
}
