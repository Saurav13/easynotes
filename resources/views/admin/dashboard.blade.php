@extends ('admin.admin')


@section('body')
<div class="content">
	<div class="container-fluid">
		<div class="card">
			<div class="header">
	            <h4 class="title">Visitors Count</h4>
	        </div>
	        <div class="content table-responsive table-full-width">
	            <table class="table table-hover table-striped">
	                <thead>
	                    <th>ID</th>
	                	<th>Name</th>
	                	<th>Visitors</th>
	                </thead>
	                <tbody>
	                    <tr>
	                    	<td>1</td>
	                    	<td>Home</td>
	                    	<td>{{Counter::show('home')}}</td>
	                    </tr>
	                    <tr>
	                    	<td>2</td>
	                    	<td>Notes</td>
	                    	<td>{{Counter::show('notes')}}</td>
	                    </tr>
	                    <tr>
	                    	<td>3</td>
	                    	<td>QuestionBanks</td>
	                    	<td>{{Counter::show('qb')}}</td>
	                    </tr>
	                    <tr>
	                    	<td>4</td>
	                    	<td>Posts</td>
	                    	<td>{{Counter::show('posts')}}</td>
	                    </tr>
	                    <tr>
	                    	<td>5</td>
	                    	<td>Events</td>
	                    	<td>{{Counter::show('events')}}</td>
	                    </tr>
	                    <tr>
	                    	<td>6</td>
	                    	<td>Opportunities</td>
	                    	<td>{{Counter::show('opportunities')}}</td>
	                    </tr>
	                </tbody>
	            </table>

	        </div>

		</div>
		<div class="card">
			<div class="header">
	            <h4 class="title">Total Counts and Sizes</h4>
	        </div>
	        <div class="content table-responsive table-full-width">
	            <table class="table table-hover table-striped">
	                <thead>
	                    <th>ID</th>
	                	<th>Name</th>
	                	<th>Counts / Size</th>
	                </thead>
	                <tbody>
	                    <tr>
	                    	<td>1</td>
	                    	<td>User</td>
	                    	<td>{{$total_users}}</td>
	                    </tr>
	                    <tr>
	                    	<td>2</td>
	                    	<td>Notes</td>
	                    	<td>{{$total_notes}}</td>
	                    </tr>
	                    <tr>
	                    	<td>3</td>
	                    	<td>QuestionBanks</td>
	                    	<td>{{$total_qbs}}</td>
	                    </tr>
	                    <tr>
	                    	<td>4</td>
	                    	<td>Faculties</td>
	                    	<td>{{$total_faculties}}</td>
	                    </tr>
	                    <tr>
	                    	<td>5</td>
	                    	<td>Subjects</td>
	                    	<td>{{$total_subjects}}</td>
	                    </tr>
	                    <tr>
	                    	<td>6</td>
	                    	<td>Forum Threads</td>
	                    	<td>{{$total_threads}}</td>
	                    </tr>
	                     <tr>
	                    	<td>7</td>
	                    	<td>Syllabus</td>
	                    	<td>{{$total_syllabus}}</td>
	                    </tr>
	                     <tr>
	                    	<td>8</td>
	                    	<td>Notes Size</td>
	                    	<td>{{$note_size}} MB</td>
	                    </tr>
	                     <tr>
	                    	<td>9</td>
	                    	<td>QuestionBank Size</td>
	                    	<td>{{$qb_size}} MB</td>
	                    </tr>
	                     <tr>
	                    	<td>10</td>
	                    	<td>Syllabus Size</td>
	                    	<td>{{$syllabus_size}} MB</td>
	                    </tr>
	                </tbody>
	            </table>

	        </div>
	        
		</div>
	</div>
</div>
@endsection