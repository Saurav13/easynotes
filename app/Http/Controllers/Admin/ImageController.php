<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Sliderimage;
use Session;
use Image;

class ImageController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index()
    {
    	 $sliderimage=Sliderimage::orderBy('id','desc')->paginate(3);
        //return a view and pass in the aboove variable into the view
        return view('admin.image.slider')->with('sliderimage',$sliderimage);
       
    }
    public function insertslider(Request $request)
    {
         $this->validate($request, array(
           
            'file'=>'required|image'
        ));
    	$sliderimage = new Sliderimage;
        // dd($request->hasile('file'));
        $sliderimage->id = $request->id;


        if($request->hasFile('file')){
        $file=$request->file('file');
        $filename=time() . '.' .$file->getClientOriginalExtension();
		$path='';
        // $file->move($path,$filename);
         $location=public_path('systemimages/'. $filename);
        Image::make($file)->resize(1024,360)->save($location); 
        $sliderimage->file=$filename;
        }

        $sliderimage->title = $request->title;
        $sliderimage->description = $request->description;
        $sliderimage->link = $request->link;
        $sliderimage->save();

        Session::flash('success','Note was added successfully!');

        return redirect()->route('sliderimage.index');
    }

    public function delete(Request $request)
    {
        $sliderimage = Sliderimage::findorFail($request->id);
        if($sliderimage->file!=null){
        unlink('systemimages/'.$sliderimage->file);
        }
        $sliderimage->delete();

        return redirect()->route('sliderimage.index');
    }
}
