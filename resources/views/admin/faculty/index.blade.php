@extends('admin.admin')

@section('body')
<br>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-6">
	<h3>Faculties</h3>
</div>
<div class="col-md-6">
	<form action="{{URL::to('admin/faculty/search')}}" method="GET">
		<input type="search" name="key" required placeholder="Search faculties"class="form-control pull-right"/>
		<input type="submit" style="visibility: hidden;" />
	</form>
	</div>
	</div>
	
	<br>
	<div class="card">
		<div class="container-fluid">
		<div class="row">
			<div class="col-md-8">
				<table class="table table-hover table-striped">
					<thead>
						<tr>
							<th>ID</th>
							<th>Faculty Name</th>
							<th>Faculty Description</th>
							<th>Field</th>
							<th>Image</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach($faculties as $faculty)
						<tr>
							<td>{{$faculty->faculty_id}}</td>
							<td id="fname{{$faculty->faculty_id}}">{{$faculty->faculty_name}}</td>
							<td id="desc{{$faculty->faculty_id}}">{{$faculty->description}}</td>
							<td id="cat_name{{$faculty->faculty_id}}">{{$faculty->category_name}}</td>
							<td id="files{{$faculty->faculty_id}}">{{$faculty->files}}</td>
							<td><input type="button" cid="{{$faculty->faculty_id}}"  class="btn btn-md btn-primary" value="Edit" id="edit{{$faculty->faculty_id}}"/  data-toggle="modal" data-target="#myModal1">
								<input type="button" cid="{{$faculty->faculty_id}}"  class="btn btn-md btn-danger" value="Delete" id="delete{{$faculty->faculty_id}}"/>
							</td>
						</tr>

						@endforeach
					</tbody>

			</table>
		</div>

		<!--end of col-md-8-->

		<div class="col-md-4">
			<br>
			<div class="well">

				<h4>Add Faculty</h4>
				{!! Form::open(['route'=>'faculty.store','files' => 'true','enctype'=>'multipart/form-data','method'=>'POST']) !!}
				Faculty Name:<br>
				<input type="text" name="faculty_name" class="form-control"><br>
				Faculty Description:<br>
				<textarea rows="4" cols="50" class="form-control" name="faculty_description" style="height: 174px; width: 100%" >
				</textarea>
				<br>
				<label>Field:</label><br>
				{!! Form::select('category_id',$categories,null,['class'=>'form-control','required'=>'true']) !!}
				<br>

				Upload Image:
				<input type="file" name="files">


				<br>
				<button class="btn btn-md btn-success">Submit Faculty Details</button>

				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
</div>
</div>


<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>

<script>
	$(document).ready(function(){

		$('[id *= "delete"]').click(function(event){

			var id= $(this).attr('cid');

			swal({
				title: "Are you sure?",
				text: "You will not be able to recover this file!",
				type: "warning",

				showCancelButton: true,

			}).then(function(){
				$.post("{{route('faculty.destroy')}}",{id:id,_token:"{{csrf_token()}}"},function(data){
					swal({
						title:"Deleted Successfully",
						type:"success"

					}).then(function(){
						window.location.reload();
					})
				})
			});
		});
	});
</script>
<script type="text/javascript">
	$(document).ready(function(){
		$('[id *="edit"]').click(function(event){
			var id= $(this).attr('cid');
			
			var fname = $('#fname'+id).html();
			var desc = $('#desc'+id).html();
			var catname = $('#cat_name'+id).val();
			
			console.log(catname);
			$('#modalfname').val(fname);
			$('#modalfdesc').html(desc);
			$('#modalcat').val(catname);

			$('#facultyid').val(id);
			
		});
	});
</script>

@stop
<div class="modal" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
		<form action="{{route('faculty.update')}}" method="POST" enctype="multipart/form-data">
			<input type="hidden" name="id" id="facultyid">
			{{csrf_field()}}
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Edit Faculty</h4>
			</div>
			<div class="modal-body">
				Faculty Name:<br>
				<input type="text" name="faculty_name" class="form-control" id="modalfname"><br>
				Faculty Description:<br>
				<textarea rows="4" cols="50" class="form-control" id="modalfdesc" name="faculty_description" style="height: 174px; width: 100%">
				</textarea>
				<br>
				<label>Field:</label><br>
				{!! Form::select('category_id',$categories,null,['class'=>'form-control','id'=>'modalcat','required'=>'true']) !!}
				<br>

				Upload Image:
				<input type="file" name="files">


				<br>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-md btn-success">Save changes</button>
			</div>
			</form>
		</div>
	</div>
</div>