<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupChat extends Model
{
    public function group(){
    	return $this->belongsTo('App\Group');
    }

    public function poster(){
    	return $this->belongsTo('App\User');
    }
}
