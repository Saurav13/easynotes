@extends('frontend.main')

@section('body')

<link rel="stylesheet" type="text/css" href="{{asset('newfrontend/loginform/css/style.css')}}">
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
<div class="container">
    

    <div class="omb_login">
        <h3 class="omb_authTitle">Create<a style="text-decoration: none;"> Account</a></h3>
        <div class="row omb_row-sm-offset-3 omb_socialButtons">
            <div class="col-xs-12 col-sm-3">
                <a href="{{URL::to('login/facebook')}}" class="btn btn-lg btn-block omb_btn-facebook">
                    <i class="fa fa-facebook visible-xs"></i>
                    <span class="hidden-xs"><i class="fa fa-facebook hidden-xs"></i>  Facebook</span>
                </a>
            </div>
            <div class="col-xs-12 col-sm-3">
                <a href="{{URL::to('login/google')}}" class="btn btn-lg btn-block omb_btn-google">
                    <i class="fa fa-google-plus visible-xs"></i>
                    <span class="hidden-xs"><i class="fa fa-google-plus hidden-xs"></i>  Google+</span>
                </a>
            </div>  
        </div>

        <div class="row omb_row-sm-offset-3 omb_loginOr">
            <div class="col-xs-12 col-sm-6">
                <hr class="omb_hrOr">
                <span class="omb_spanOr">or</span>
            </div>
        </div>

        <div class="row omb_row-sm-offset-3">
            <div class="col-xs-12 col-sm-6">    
                <form class="omb_loginForm" method="POST" action="{{route('register')}}" accept-charset="UTF-8">
                {{csrf_field()}}
                    <div class="input-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <span class="input-group-addon"><i class="fa fa-user"></i> Username</span>
                        <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>
                    </div>
                        <span class="help-block">
                    @if ($errors->has('name'))
                            <strong>{{ $errors->first('name') }}</strong>
                    @endif
                        </span>

                    <div class="input-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <span class="input-group-addon"><i class="fa fa-envelope"></i> Email Address </span>
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                    </div>
                    <span class="help-block">
                        @if ($errors->has('email'))
                                                   <strong>{{ $errors->first('email') }}</strong>
                         @endif
                    </span>
                                        
                    <div class="input-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <span class="input-group-addon"><i class="fa fa-lock"></i> Password</span>
                        <input id="password" type="password" class="form-control" name="password" required>
                    </div>
                        <span class="help-block">
                    @if ($errors->has('password'))
                            <strong>{{ $errors->first('password') }}</strong>
                    @endif
                        </span>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-lock"></i> Confirm Password</span>
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                    </div>
                                          
                                            <span class="help-block">
                
                                            </span>
                    
                    <button class="btn btn-lg btn-primary btn-block" type="submit">Register</button>
                    <br>
                    <div class="input-group">
                        
                       <p>Already have an account?<a href="{{route('login')}}"> Click here</a> to login</p>
                    </div>
                </form>
                <br>
            
            </div>
        </div>
        
    </div>
</div>

@endsection
