@extends ('admin.admin')


@section('body')
 <div class="container-fluid">
<h3>Contributions</h3>
<br>
	<div class="card">


		 <div class="content table-responsive table-full-width">
                        <table class="table table-hover table-striped">
				<thead>
				<tr>
					<th>ID</th>
					<th>Name</th>
					<th>Description</th>
					<th>Type</th>
					<th>Action</th>
				</tr>
				</thead>
				<tbody>
				@foreach($contributions as $c)
				<tr>
				<td >{{$c->id}}</td>
				<td >{{$c->name}}</td>
				<td >{{substr($c->description,0,100).'....'}}</td>
				<td>{{$c->type}}</td>
					<td><a href="{{URL::to('admin/contribution'.'/'.$c->id.'/'.'view')}}" class="btn btn-md btn-primary"/>View</a>
				
					
					<input type="button" cid="{{$c->id}}"  class="btn btn-md btn-danger" value="Delete" id="delete{{$c->id}}"/>

				</td>
				</td>
				</tr>
				@endforeach
				</tbody>
			</table>
		</div>
		
	
</div>
</div>
	<script
  src="https://code.jquery.com/jquery-3.2.1.min.js"
  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  crossorigin="anonymous"></script>
<script>
	$(document).ready(function(){

		$('[id *= "delete"]').click(function(event){

			var id= $(this).attr('cid');

			swal({
				title: "Are you sure?",
				text: "You will not be able to recover this file!",
				type: "warning",

				showCancelButton: true,

			}).then(function(){
				$.post("{{URL::to('/admin/contribution')}}"+"/"+id+"/delete",{id:id,_token:"{{csrf_token()}}"},function(data){
					swal({
						title:"Deleted Successfully",
						type:"success"

					}).then(function(){
						window.location.reload();
					})
				})
			});
		});
	});
</script>
@stop