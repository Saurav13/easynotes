<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Note;
use App\Faculty;
use App\Category;
use App\Subject;
use App\Rating;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use App\Syllabus;
use Counter;

class NoteController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
     public function notes()
    {
        Counter::showAndCount('notes');
        $faculties = Faculty::all();
        $categories = Category::all();
    	return view ('frontend.notes.note')->with('faculties',$faculties)->with('categories',$categories);
    }

    public function getfaculty()
    {

    	$cat_id = Input::get('cat_id');
        $faculties=Faculty::where('category_id','=',$cat_id)->get();
        $first=$faculties->first()->faculty_id;
        $subjects=Subject::where('faculty_id','=',$first)->get();
        return response()->json([
            'faculties'=>$faculties,
            'subjects'=>$subjects
        ]);
        
    }
    public function getsubject(Request $request)
    {
        $faculty_id = Input::get('faculty_id');
        $sub=Subject::where('faculty_id','=',$faculty_id)->get();
        return response()->json([
            'sub'=>$sub
        ]);
    }

    public function notesview(Request $request)
    {
        Counter::showAndCount('notes');
        $faculty=$request->faculty;
        $semester=$request->semester;
        $syllabus=Syllabus::where('faculty_id','=',$faculty)->where('semester','=',$semester)->get();
        $sub=$request->id;
        $sname=Subject::where('subject_id','=',$sub)->get();
        $notes=Note::where('subject_id','=',$sub)->get();        

        return view ('frontend.notes.view')->with('notes',$notes)->with('sname',$sname)->with('syllabus',$syllabus);  
    }

    public function SingleNoteView($id)
    {
        Counter::showAndCount('notes');
        $notes=Note::where('id','=',$id)->get();
         if(Rating::where('note_id',$id)->where( 'id', Auth::User()->id)->exists())
            $rated=true;
        else
            $rated = false;
    
        return view('frontend.notes.singlenote')->with('notes',$notes)->withRated($rated);

    }
    
    public function postRating(Request $request)
    {
        
        Note::where('id',$request->note_id)->increment('ratenumber',$request->rate);
        Note::where('id',$request->note_id)->increment('total_raters');

        $rating = new Rating;
        $rating->note_id=$request->note_id;
        $rating->id=$request->user_id;
        $rating->save();
        return "Success";
    }


}
