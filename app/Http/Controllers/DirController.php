<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use ZipArchive;
use RecursiveIteratorIterator;
use RecursiveDirectoryIterator;
use Response;
use Illuminate\Support\Facades\Auth;
use App\User;
use PhpImap;
use App\Follow;
class DirController extends Controller
{
	protected $user;
	private $root= 'D:/easynotes/dirs';
   public function __construct(Request $request)
    {

 		 $this->middleware('auth');
 		 $this->middleware(function ($request, $next) {
 		 	if($request->path)
 		 	{
 		 	$id=explode('/',$request->path)[3];

            if ($id==Auth::user()->id){
                return $next($request);
            }
            else
                abort(403);
        	}
        	else
        		return $next($request);	
        });

    }
 	public function index()
 	{
 		// $apath=$this->root.'/../'.'assignments'.'/'.Auth::User()->id;
 		// $adirs=scandir($apath);
 		// unset($adirs[0]);
 		// unset($adirs[1]);
 		// $email=Auth::User()->email;
 		// $username=substr($email, 0, strrpos($email, '@'));
 		// // dd($username);
 		// // dd($adirs);
	  //   $cstr='{mail.projectincube.com:993/imap/ssl}';
	  //   $imap=imap_open($cstr,'assignment@projectincube.com', 'iHq73f2aS2');
	  //   $list=imap_list($imap,$cstr,"*");
 		// foreach ($adirs as $d) {
 		// 	# code...
			// // dd($d);	
			// 	// dd($list,$cstr.'INBOX'.'.'.$username.'--'.$d);
			// 	if(array_search($cstr.'INBOX'.'.'.$username.'--'.$d, $list))
			// 	{
			// 		$mailbox = new PhpImap\Mailbox('{mail.projectincube.com:993/imap/ssl}INBOX.'.$username.'--'.$d, 'assignment@projectincube.com', 'iHq73f2aS2',$apath.'/'.$d);
		 //    		// dd($mailbox);
		    
		 //    		$mails = [];
		 //    // Read all messaged into an array:

		 //    		$mailsIds = $mailbox->searchMailbox('ALL');
		    		
		 //    		  foreach ($mailsIds as $mailId){
			// 	        $_mail = $mailbox->getMail($mailId);
				   		
			// 	        $mail = array(
			// 	            'date' => $_mail->date,
			// 	            'fromName' => $_mail->fromName,
			// 	            'fromAddress' => $_mail->fromAddress,
			// 	            'subject' => $_mail->subject,
			// 	            'content' => $_mail->textPlain,
			// 	            'attachments' => $_mail->getAttachments()
			// 	        );
			// 	        // $mailbox->deleteMail($mailId);
			// 	        $mails []= $mail;
				    	
			// 	    }
			// 	    // dd($mails);
		 //    		// dd($mailsIds);
		 //    // if(!$mailsIds) {
		 //    //     return;
		 //    // }

		 //    		foreach ($mailsIds as $mailId){
		 //         		// $mailbox->deleteMail($mailId);
		 //    	}
		 //    }
 		// }
 		return view('dirs.root');
 	}
 	public function root()
 	{  	
 		$this->root=$this->root.'/'.Auth::User()->id;
		
 		$dir=$this->root;
		$buff =scandir($dir);
		$files=array();
		$folders=array();
		foreach ($buff as $b) {
			if($b!='.' and $b!='..'){
				if(strpos($b,'.')!=false)
					array_push($files, $b);
				else
					array_push($folders,$b);
			}
		}
		// dd($folders);
		$path=$this->root;
		$dirs=$this->makeTree($this->root);
		// dd($this->root);
		sort($folders);
		sort($files);
		return response()->json([
    		'files' => $files,
    		'folders' => $folders,
			'path'=>$path,
			'dirs'=>$dirs,
			]);
		// return view('dirs.root',compact('files','folders','path'));
 	}
 	public function openDir(Request $request)
 	{
 		// dd($request);
 		$dir=$request->path;
		$buff =scandir($dir);
		$files=array();
		$folders=array();
		foreach ($buff as $b) {
			if($b!='.' and $b!='..'){
				if(strpos($b,'.')!=false)
					array_push($files, $b);
				else
					array_push($folders,$b);
			}
		}
		// dd($folders);
		$path=$dir;
		// dd($dir);
		sort($folders);
		sort($files);
		return response()->json([
    		'files' => $files,
    		'folders' => $folders,
			'path'=>$path
			
			]);
 	}

 	public function addFolder(Request $request)
 	{
 		$dir=$request->path;
		$success="True";
		
		$buff =scandir($dir);
		if(in_array($request->name,$buff))
			$success="Folder Exists";
		else
		{
			// dd($dir,$this->root.Auth::User()->id);
			$name=ucfirst($request->name);
			mkdir($dir.'/'.$name,0777);
			// if($dir==$this->root.'/'.Auth::User()->id)
			// {
			// 	// dd("ASd");
			// 	mkdir($this->root.'/../'.'assignments'.'/'.Auth::User()->id.'/'.$request->name,0777);
				
			// }
		}
			$buff=scandir($dir);
		$files=array();
		$folders=array();
		foreach ($buff as $b) {
			if($b!='.' and $b!='..'){
				if(strpos($b,'.')!=false)
					array_push($files, $b);
				else
					array_push($folders,$b);
			}
		}
		// dd($folders);
		$path=$dir;
		// dd($dir);
		$this->root=$this->root.'/'.Auth::User()->id;
		$dirs=$this->makeTree($this->root);
		return response()->json([
    		'files' => $files,
    		'folders' => $folders,
			'path'=>$path,
			'success'=>$success,
			'dirs'=>$dirs
			]);
 	}
 	public function renameFolder(Request $request)
 	{
 		$dir=$request->path;
		$success="True";
		
		$buff =scandir($dir);
		if(in_array($request->newname,$buff))
			$success="Name Exists";
		else
		{
			rename($dir.'/'.$request->name,$dir.'/'.$request->newname);
			if($dir==$this->root)
			{
				Follow::where('followed_id',Auth::User()->id)
						->where('dir',$request->name)
						->update(['dir'=>$request->newname]);
			}
			// if($dir==$this->root.'/'.Auth::User()->id)
			// {
			// 	// dd("ASd");
			// 	rename($this->root.'/../'.'assignments'.'/'.Auth::User()->id.'/'.$request->name,$this->root.'/../'.'assignments'.'/'.Auth::User()->id.'/'.$request->newname);
				
			// }

			
		}
		$buff=scandir($dir);
		$files=array();
		$folders=array();
		foreach ($buff as $b) {
			if($b!='.' and $b!='..'){
				if(strpos($b,'.')!=false)
					array_push($files, $b);
				else
					array_push($folders,$b);
			}
		}
		// dd($folders);
		$path=$dir;
		// dd($dir);
		$this->root=$this->root.'/'.Auth::User()->id;
		$dirs=$this->makeTree($this->root);
		return response()->json([
    		'files' => $files,
    		'folders' => $folders,
			'path'=>$path,
			'success'=>$success,
			'dirs'=>$dirs
			]);
 	}

	

 	private function delete_directory($dirname) {
      if (is_dir($dirname))
           $dir_handle = opendir($dirname);
	 if (!$dir_handle)
	      return false;
	 while($file = readdir($dir_handle)) {
	       if ($file != "." && $file != "..") {
	            if (!is_dir($dirname."/".$file))
	                 unlink($dirname."/".$file);
	            else
	                 $this->delete_directory($dirname.'/'.$file);
	       }
	 }
	 closedir($dir_handle);
	 rmdir($dirname);
	 return true;
	}
 	public function deleteFolder(Request $request)
 	{
 		$dir=$request->path;
		$success="True";
		
		$this->delete_directory($dir.'/'.$request->name);
		if($dir==$this->root)
			{
				Follow::where('followed_id',Auth::User()->id)
						->where('dir',$request->name)
						->delete();
			}
		// if($dir==$this->root.'/'.Auth::User()->id)
		// 	{
		// 		// dd("ASd");
		// 		$this->delete_directory($this->root.'/../'.'assignments'.'/'.Auth::User()->id.'/'.$request->name);
		
		// 	}
		$buff=scandir($dir);
		$files=array();
		$folders=array();
		foreach ($buff as $b) {
			if($b!='.' and $b!='..'){
				if(strpos($b,'.')!=false)
					array_push($files, $b);
				else
					array_push($folders,$b);
			}
		}
		// dd($folders);
		$path=$dir;
		// dd($dir);
		$this->root=$this->root.'/'.Auth::User()->id;
		$dirs=$this->makeTree($this->root);
		return response()->json([
    		'files' => $files,
    		'folders' => $folders,
			'path'=>$path,
			'success'=>$success,
			'dirs'=>$dirs
			]);
 	}
 	private function move($dirname,$path)
	{
		if(is_dir($dirname)){
			
			$dir_handle = opendir($dirname);
	 		}
		while($file = readdir($dir_handle)) {
	       if ($file != "." && $file != "..") {
	            if (!is_dir($dirname."/".$file))
	                 rename($dirname."/".$file,$path.'/'.$file);
	            else
	            {
	            	mkdir($path.'/'.$file);
	            	$this->move($dirname.'/'.$file,$path.'/'.$file);
	      		}
	       }	
		}
		
	}

 	public function moveFolder(Request $request)
 	{
 		$dir=$request->newpath;
		$success="True";
		$buff =scandir($dir);
		if(in_array($request->name,$buff))
			$success="Same Folder name already exists";
		else{
			if(is_dir($request->oldpath)){
				mkdir($request->newpath.'/'.$request->name);
				$this->move($request->oldpath,$request->newpath.'/'.$request->name);
				$this->delete_directory($request->oldpath);
			}
			else
			{
				 copy($request->oldpath,$request->newpath.'/'.$request->name);
				 unlink($request->oldpath);
			}
		}
		$dir=$request->path;
		$buff=scandir($dir);
		$files=array();
		$folders=array();
		foreach ($buff as $b) {
			if($b!='.' and $b!='..'){
				if(strpos($b,'.')!=false)
					array_push($files, $b);
				else
					array_push($folders,$b);
			}
		}
		// dd($folders);
		$path=$dir;
		// dd($dir);
		$this->root=$this->root.'/'.Auth::User()->id;
		$dirs=$this->makeTree($this->root);
		return response()->json([
    		'files' => $files,
    		'folders' => $folders,
			'path'=>$path,
			'success'=>$success,
			'dirs'=>$dirs
			]);
 	}

 	public function extractFile(Request $request)
 	{
 		$dir=$request->path;
 		$buff=scandir($dir);
		$success="True";
		if(in_array($request->name,$buff))
			$success="Folder Exists";
		else
			mkdir($dir.'/'.$request->name,0777);
		$zip = new ZipArchive;
		if ($zip->open($dir.'/'.$request->name.'.zip') === TRUE) {
    		$zip->extractTo($dir);
    		$zip->close();
    		
		} else {
    		$success= 'zip failed';
		}
		$dir=$request->path;
		$buff=scandir($dir);
		$files=array();
		$folders=array();
		foreach ($buff as $b) {
			if($b!='.' and $b!='..'){
				if(strpos($b,'.')!=false)
					array_push($files, $b);
				else
					array_push($folders,$b);
			}
		}
		// dd($folders);
		$path=$dir;
		// dd($dir);
		$this->root=$this->root.'/'.Auth::User()->id;
		$dirs=$this->makeTree($this->root);
		return response()->json([
    		'files' => $files,
    		'folders' => $folders,
			'path'=>$path,
			'success'=>$success,
			'dirs'=>$dirs
			]);
 	}
 	public function deleteFile(Request $request)
 	{
 		$dir=$request->path;
		$success="True";
		unlink($dir.'/'.$request->name);	
		$buff=scandir($dir);
		$files=array();
		$folders=array();
		foreach ($buff as $b) {
			if($b!='.' and $b!='..'){
				if(strpos($b,'.')!=false)
					array_push($files, $b);
				else
					array_push($folders,$b);
			}
		}
		// dd($folders);
		$path=$dir;
		// dd($dir);
		$this->root=$this->root.'/'.Auth::User()->id;
		$dirs=$this->makeTree($this->root);
		return response()->json([
    		'files' => $files,
    		'folders' => $folders,
			'path'=>$path,
			'success'=>$success,
			'dirs'=>$dirs
			]);
 	}
 	private $count=0;
 	public function makeTree($dir)
 	{
 		$array=array();
 		$buff=scandir($dir);	
 		foreach ($buff as $b) {
			if($b!='.' and $b!='..'){
				if(is_dir($dir.'/'.$b))
				{
					$arr=array("dirs"=>$this->makeTree($dir.'/'.$b),"id"=>$this->count,"name"=>$b,"path"=>$dir.'/'.$b);
					$this->count=$this->count+1;
					array_push($array, $arr);
				}
				else
				{
					$arr=array("name"=>$b,"id"=>$this->count,"path"=>$dir.'/'.$b);
					$this->count=$this->count+1;
					array_push($array,$arr);
				}
			}
		}
		return $array;
 	}
 	public function uploadFile(Request $request)
 	{
 		$dir=$request->path;
		$success="True";
		
 		$file=$request->file('file');
 		$filename=$file->getClientOriginalName();
		$file->move($request->path,$filename);

		$buff=scandir($dir);
		$files=array();
		$folders=array();
		foreach ($buff as $b) {
			if($b!='.' and $b!='..'){
				if(strpos($b,'.')!=false)
					array_push($files, $b);
				else
					array_push($folders,$b);
			}
		}
		// dd($folders);
		$path=$dir;
		// dd($dir);
		$this->root=$this->root.'/'.Auth::User()->id;
		$dirs=$this->makeTree($this->root);
		return response()->json([
    		'files' => $files,
    		'folders' => $folders,
			'path'=>$path,
			'success'=>$success,
			'dirs'=>$dirs
			]);
 	}
 	public function getDirTree()
 	{
 		
 		$this->root=$this->root.'/'.Auth::User()->id;
		// dd($this->root);

 		return json_encode($this->makeTree($this->root));
 	}
 	public function downloadFolder(Request $request)
 	{
 		$dir = $request->path.'/'.$request->name;
		$zip_file = $request->name.'.zip';

        $rootPath = realpath($dir);

		// Initialize archive object
		$zip = new ZipArchive();
		$zip->open($request->path.'/'.$zip_file, ZipArchive::CREATE | ZipArchive::OVERWRITE);

		// Create recursive directory iterator
		/** @var SplFileInfo[] $files */
		$files = new RecursiveIteratorIterator(
		    new RecursiveDirectoryIterator($rootPath),
		    RecursiveIteratorIterator::LEAVES_ONLY
		);
		$c=0;
		foreach ($files as $name => $file)
		{
		    // Skip directories (they would be added automatically)
		    if (!$file->isDir())
		    {
		        // Get real and relative path for current file
		        $filePath = $file->getRealPath();
		        $relativePath = substr($filePath, strlen($rootPath) + 1);

		        // Add current file to archive
		        $zip->addFile($filePath, $relativePath);
		        $c=$c+1;
		    }
		}
		if($c==0)
			return "No files exists within this folder";
		// Zip archive will be created only after closing object
		$zip->close();
	    // dd($zip);
	    return Response::download($request->path.'/'.$zip_file)->deleteFileAfterSend(true);;

 	}
 	public function downloadFile(Request $request)
 	{
 		return Response::download($request->path.'/'.$request->name);
 	}

}
