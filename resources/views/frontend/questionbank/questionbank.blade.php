@extends('frontend.main')
@section('body')

<style type="text/css">
		select{
			font-size: 17px;
		}

		label, input, button, select, textarea {
    font-size: 16px;
    font-weight: normal;
    padding-left: 10px;
    padding-right: 10px;
}

ul.cat li a, ul.folio-detail li a {
    color: #656565;
    font-weight: 600;
}

.well{
	background-color: rgba(220, 220, 220, 0.12);
	border:0px;

}
	</style>

	<div class="container">
	<br>
	
	</div>


	



		<div class="container containershadow">
		<ul class="" style="margin-left: 0px;">

		<h3>Choose you Field</h3>

		<div class="row col-md-8">
		<div class="col-md-4">
		<label>Field :</label>
		<select name="category" id='category' class="form-control">
		<option selected disabled>Choose Field</option>
		@foreach($categories as $category)
		  <option value="{{$category->category_id}}" >{{$category->category_name}}</option>
		@endforeach
		</select>
		</div>
		<div class="col-md-4">
		<label for="faculty">Faculty:</label>
		<select name="faculty" id='faculty' class="form-control" >
		<option selected disabled>Choose Faculty</option>
		</select>	
		</div>
		</div>
		</ul>
		<br>
		</div>
		<br>
		<div class="preloader hidden" id="preloader" align="center">
			<img src="{{asset('img/preloader.gif')}}" style="height: 100px; width: 100px;" >
		</div>
		<div id="noresult" class="hidden" align="center">
			<img src="{{asset('img/noresults.png')}}" style="height: 300px; width: 382px;">
		</div>
			<section id="content" style="padding-top: 0px;" class="hidden">
				<div class="container containershadow">
					<h3 id="facultyheading"></h3>
					<div class="col-lg-3">
					<div class="widget">
							<h4 class="widgetheading" style="color: #a94442;">First Semester</h4>
							<ul id="first" class="cat">
							</ul>
					</div>
					</div>
					<div class="col-lg-3">
					<div class="widget">
							<h4 class="widgetheading" style="color: #a94442;">Second Semester</h4>
							<ul class="cat" id="second">
							</ul>
					</div>
					</div>
					<div class="col-lg-3">
					<div class="widget">
							<h4 class="widgetheading" style="color: #a94442;">Third Semester</h4>
							<ul class="cat" id="third">
							</ul>
					</div>
					</div>
					<div class="col-lg-3">
					<div class="widget">
							<h4 class="widgetheading" style="color: #a94442;">Fourth Semester</h4>
							<ul class="cat" id="fourth">
							</ul>
					</div>
					</div>
					<div class="col-lg-3">
					<div class="widget">
							<h4 class="widgetheading" style="color: #a94442;">Fifth Semester</h4>
							<ul class="cat" id="fifth">
							</ul>
					</div>
					</div>
					<div class="col-lg-3">
					<div class="widget">
							<h4 class="widgetheading" style="color: #a94442;">Sixth Semester</h4>
							<ul class="cat" id="sixth">
							</ul>
					</div>
					</div>
					<div class="col-lg-3">
					<div class="widget">
							<h4 class="widgetheading" style="color: #a94442;">Seventh Semester</h4>
							<ul class="cat" id="seventh">
							</ul>
					</div>
					</div>
					<div class="col-lg-3">
					<div class="widget">
							<h4 class="widgetheading" style="color: #a94442;">Eighth Semester</h4>
							<ul class="cat" id="eighth">
							</ul>
					</div>
					</div>
					</div>
				
			</section>

	
	<script
	  src="https://code.jquery.com/jquery-3.2.1.min.js"
	  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
	  crossorigin="anonymous"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		$('#category').on('change', function(e){
			var cat_id = e.target.value;
			$('#noresult').addClass('hidden');
			$('#preloader').removeClass('hidden');
			$('#content').addClass('hidden');

			//ajax
			$.get('getfacquestions?cat_id=' + cat_id,function(data){

				$('#faculty').empty();
				$.each(data.faculties, function(index, faculties){
					$('#faculty').append('<option value="'+faculties.faculty_id+'" name="'+faculties.faculty_name+'" >'+faculties.faculty_name+'</option>')
					
					//bake cha for faculty heading
				});

					$('#first').empty();
					$('#second').empty();
					$('#third').empty();
					$('#fourth').empty();
					$('#fifth').empty();
					$('#sixth').empty();
					$('#seventh').empty();
					$('#eighth').empty();

					if($.isEmptyObject(data.facquestions)){
						$('#content').addClass('hidden');
					  $('#preloader').addClass('hidden');
					  $('#noresult').fadeIn(1000).removeClass('hidden');
					}
					
				$.each(data.facquestions, function(index,facquestions){
					if(data.facquestions != null)
					{
					$('#noresult').addClass('hidden');
					$('#preloader').addClass('hidden');
					$('#content').fadeIn(1000).removeClass('hidden');
					}
					if(facquestions.semester=='1st')
					{		
						$('#first').append('<li><i class="icon-angle-right"></i>'+facquestions.title+'<a href="{{asset('uploads/qb')}}/'+facquestions.link+'"><button class="btn btn-xs btn-default pull-right">Download</button></a></li>');
				
					}
					if(facquestions.semester=='2nd')
					{
						// $.each(data.subjects,function(index,subjects){
						$('#second').append('<li><i class="icon-angle-right"></i>'+facquestions.title+'<a href="{{asset('uploads/qb')}}/'+facquestions.link+'"><button class="btn btn-xs btn-default pull-right">Download</button></a></li>');
					// });

					}
					if(facquestions.semester=='3rd')
					{
						// $.each(data.subjects,function(index,subjects){
						$('#third').append('<li><i class="icon-angle-right"></i>'+facquestions.title+'<a href="{{asset('uploads/qb')}}/'+facquestions.link+'"><button class="btn btn-xs btn-default pull-right">Download</button></a></li>');
					// });
					}
					if(facquestions.semester=='4th')
					{
						// $.each(data.subjects,function(index,subjects){
						$('#fourth').append('<li><i class="icon-angle-right"></i>'+facquestions.title+'<a href="{{asset('uploads/qb')}}/'+facquestions.link+'"><button class="btn btn-xs btn-default pull-right">Download</button></a></li>');
					}
					if(facquestions.semester=='5th')
					{
						// $.each(data.subjects,function(index,subjects){
						$('#fifth').append('<li><i class="icon-angle-right"></i>'+facquestions.title+'<a href="{{asset('uploads/qb')}}/'+facquestions.link+'"><button class="btn btn-xs btn-default pull-right">Download</button></a></li>');
					// });
					}
					if(facquestions.semester=='6th')
					{
						// $.each(data.subjects,function(index,subjects){
						$('#sixth').append('<li><i class="icon-angle-right"></i>'+facquestions.title+'<a href="{{asset('uploads/qb')}}/'+facquestions.link+'"><button class="btn btn-xs btn-default pull-right">Download</button></a></li>');
					// });
					}
					if(facquestions.semester=='7th')
					{
						// $.each(data.subjects,function(index,subjects){
						$('#seventh').append('<li><i class="icon-angle-right"></i>'+facquestions.title+'<a href="{{asset('uploads/qb')}}/'+facquestions.link+'"><button class="btn btn-xs btn-default pull-right">Download</button></a></li>');
					// });
					}
					if(facquestions.semester=='8th')
					{
						// $.each(data.subjects,function(index,subjects){
						$('#eighth').append('<li><i class="icon-angle-right"></i>'+facquestions.title+'<a href="{{asset('uploads/qb')}}/'+facquestions.link+'"><button class="btn btn-xs btn-default pull-right">Download</button></a></li>');
					// });
					}
				});
				

				$('#faculty').on('change', function(e){
					var faculty_id = e.target.value;
					$('#preloader').removeClass('hidden');
					$('#content').addClass('hidden');
					$('#noresult').addClass('hidden');
					console.log(faculty_id);
					$.get('getquestions?faculty_id=' + faculty_id,function(data){
							$('#first').empty();
							$('#second').empty();
							$('#third').empty();
							$('#fourth').empty();
							$('#fifth').empty();
							$('#sixth').empty();
							$('#seventh').empty();
							$('#eighth').empty();
							if($.isEmptyObject(data.questions)){
							  $('#preloader').addClass('hidden');
							  $('#noresult').fadeIn(1000).removeClass('hidden');
							}
						$.each(data.questions, function(index,questions){
							if(data.questions != null)
							{
							$('#noresult').addClass('hidden');
							$('#preloader').addClass('hidden');
							$('#content').fadeIn(1500).removeClass('hidden');
							}
							if(questions.semester=='1st')
							{
								
								$('#first').append('<li><i class="icon-angle-right"></i>'+questions.title+'<a href="{{asset('uploads/qb')}}/'+questions.link+'"><button class="btn btn-xs btn-default pull-right">Download</button></a></li>');
							 
							}
							if(questions.semester=='2nd')
							{
								// $.each(data.sub,function(index,sub){
								$('#second').append('<li><i class="icon-angle-right"></i>'+questions.title+'<a href="{{asset('uploads/qb')}}/'+questions.link+'"><button class="btn btn-xs btn-default pull-right">Download</button></a></li>');
							// });
							}
							if(questions.semester=='3rd')
							{
								// $.each(data.sub,function(index,sub){
								$('#third').append('<li><i class="icon-angle-right"></i>'+questions.title+'<a href="{{asset('uploads/qb')}}/'+questions.link+'"><button class="btn btn-xs btn-default pull-right">Download</button></a></li>');
							// });
							}
							if(questions.semester=='4th')
							{
								// $.each(data.sub,function(index,sub){
								$('#fourth').append('<li><i class="icon-angle-right"></i>'+questions.title+'<a href="{{asset('uploads/qb')}}/'+questions.link+'"><button class="btn btn-xs btn-default pull-right">Download</button></a></li>');
							// });
							}
							if(questions.semester=='5th')
							{
								// $.each(data.sub,function(index,sub){
								$('#fifth').append('<li><i class="icon-angle-right"></i>'+questions.title+'<a href="{{asset('uploads/qb')}}/'+questions.link+'"><button class="btn btn-xs btn-default pull-right">Download</button></a></li>');
							// });
							}
							if(questions.semester=='6th')
							{
								// $.each(data.sub,function(index,sub){
								$('#sixth').append('<li><i class="icon-angle-right"></i>'+questions.title+'<a href="{{asset('uploads/qb')}}/'+questions.link+'"><button class="btn btn-xs btn-default pull-right">Download</button></a></li>');
								// });
							}
							if(questions.semester=='7th')
							{
								// $.each(data.sub,function(index,sub){
								$('#seventh').append('<li><i class="icon-angle-right"></i>'+questions.title+'<a href="{{asset('uploads/qb')}}/'+questions.link+'"><button class="btn btn-xs btn-default pull-right">Download</button></a></li>');
							// });
							}
							if(questions.semester=='8th')
							{
								// $.each(data.sub,function(index,sub){
								$('#eighth').append('<li><i class="icon-angle-right"></i>'+questions.title+'<a href="{{asset('uploads/qb')}}/'+questions.link+'"><button class="btn btn-xs btn-default pull-right">Download</button></a></li>');
							// });
							}
					});

				});
			});
		});

	});
});
	
	</script>
@endsection