<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Note;
use App\User;
use Illuminate\Support\Facades\Input;
use Auth;
use App\Follow;
use DB;
use Counter;

class SearchController extends Controller
{
        public function __construct()
        {
            $this->middleware('auth');
        }

    public function searchNotes(Request $request)
    {
        Counter::showAndCount('notes');

    	$search=$request->search;
    	 
        $notes=Note::where('title','LIKE','%'.$search.'%')->orderBy('id')->paginate();
        $countnotes = count($notes);
        return view('frontend.notes.notessearch')->withNotes($notes)->with('countnotes',$countnotes)->with('search',$search);
    }

    public function searchUserForDirectory(Request $request)
    {
    	$search=$request->search;
    	$users = 0;
    	if($search != null)
    	{
    	$users=User::where('name','LIKE','%'.$search.'%')->orWhere('email','='.$search)->get();
    	}
    	return response()->json([
    	    'users'=>$users
    	]);

    }

    public function SearchForDirectory()
    {
    	return view('frontend.directory.dirsearch');
    }

    public function SearchUserDirectory(Request $request)
    {
    	$search=$request->search;
    	$users = 0;
    	if($search != null)
    	{
    	$users=User::where('id', '!=', Auth::id())->where('name','LIKE','%'.$search.'%')->orWhere('email','=',$search)->get();
    	}
    	return view('frontend.directory.searchresults')->with('users',$users)->with('search',$search);
    }

    public function DirectoryView($id)
    {

        
        $array=array();
        $user_id=$id;
        $follows=Follow::where('user_id','=',Auth::User()->id)->where('followed_id','=',$id)->get()->pluck('dir');
        // dd($follows);
        $path= 'D:/easynotes/public/dirs'.'/'.$id;
        $dirs=scandir($path);
        $files=array();
        $folders=array();
        $flag=array();
        foreach ($dirs as $b) {
            if($b!='.' and $b!='..'){
                    $f=false;
                    if($follows->contains($b))
                    {    $f=true;
                    }
                    array_push($folders,array($b,$f));
                }
            }
        
        // dd($folders);
        // $folders=$folders->zip($flag)->all();
        // dd($folders);
        return view('frontend.directory.directories')->with('folders',$folders)->with('user_id',$user_id);
    }

    public function DirectoryFollow(Request $request)
    {
        $user_id=Input::get('user_id');
        $currentuser_id=Input::get('currentuser_id');
        $folder_name=Input::get('folder_name');
        $follow=Follow::where('user_id','=',$request->currentuser_id)->where('followed_id','=',$request->user_id)->where('dir','=',$request->folder_name)->get();
        if(count($follow) == null)
        {
            $f=new Follow();
            $f->user_id=$currentuser_id;
            $f->followed_id=$user_id;
            $f->dir=$folder_name;
            $f->save();
            return ('followed');            
        }
        else
        {
            $f=Follow::where('user_id','=',$request->currentuser_id)->where('followed_id','=',$request->user_id)->where('dir','=',$request->folder_name);
            $f->delete();
            return('unfollowed');
        }


    }

    public function DirectoryUnfollow($id)
    {
        $follow=Follow::findorfail($id);

    }

   
}
