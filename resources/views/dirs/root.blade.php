<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Directory</title>
  <!-- Bootstrap core CSS-->
  <link href="{{asset('newfrontend/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="{{asset('newfrontend/vendor/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
  <!-- Page level plugin CSS-->
  <link href="{{asset('newfrontend/vendor/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="{{asset('newfrontend/css/sb-admin.css')}}" rel="stylesheet">

<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head>



<style type="text/css">
.card-body-icon {
  position: absolute;
  z-index: 0;
  top: 7px;
  right: 0px;
  font-size: 3rem;
  -webkit-transform: rotate(15deg);
  -ms-transform: rotate(15deg);
  transform: rotate(0deg);

}

.scrollbar
{
  margin-left: 30px;
  float: left;
  height: 300px;
  width: 65px;
  background: #F5F5F5;
  overflow-x: scroll;
  margin-bottom: 25px;
}

#style-2::-webkit-scrollbar-track
{
  -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
  border-radius: 10px;
  background-color: #F5F5F5;
}

#style-2::-webkit-scrollbar
{
  width: 12px;
  background-color: #F5F5F5;
}

#style-2::-webkit-scrollbar-thumb
{
  border-radius: 10px;
  -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
  background-color: #D62929;
}



.bg-primary {
  background-color: #818182!important;
}

.navbar{
  box-shadow:  0px 0px 20px 0px #3b3b3b, inset 0px 0px 0px 0px #3b3b3b;
}
  


.tabs, label,.breadcrumb,button,.btn{
  box-shadow:  0px 0px 3px 0px #3b3b3b, inset 0px 0px 0px 0px #3b3b3b;
  
}

.breadcrumb{
  margin-right: 15px;
}

#mainNav.navbar-dark .navbar-collapse .navbar-sidenav {
    background-image: url('http://www.ckg-investigations.com/img/bg-grey.jpg');
    box-shadow:  0px 0px 3px 0px #3b3b3b, inset 0px 0px 0px 0px #3b3b3b;
}


footer.sticky-footer {
    position: absolute;
    right: 0;
    bottom: 0;
    width: 100%;
    height: 56px;
    background-color: #ddd;
    line-height: 55px;
    box-shadow:  0px 0px 3px 0px #3b3b3b, inset 0px 0px 0px 0px #3b3b3b;
}

.content-wrapper{
  background-color: #eee;
  background-image: url('http://www.newell-fonda.k12.ia.us/wp-content/uploads/r-grey-abstract-background-for-design_MJmTARLO_L.jpg');
}

#tab-content1,#tab-content2{
  background-color: transparent;
}
.active-state, .page--home .nav-home, .page--about .nav-about, .page--clients .nav-clients, .page--contact .nav-contact {
  background: #fc5a08;
}

.hoz-nav {
  margin: 0;
  padding: 0;
  list-style: none;
  text-align: justfiy;
  width: 100%;
}
.hoz-nav:after {
  display: inline-block;
  content: '';
  width: 100%;
}

.hoz-nav__item {
  background: #05a872;
  width: 100%;
  float: left;
  text-align: center;
}
.hoz-nav__item:hover {
  background: #06c183;
}
.hoz-nav__item a {
  color: #fff;
  display: block;
  text-decoration: none;
  
}
.btn{
    background: #ccc;
} .btn:focus{
    background: #8BC34A;
}





</style>


<body class="fixed-nav sticky-footer" id="page-top" ng-app="DirApp" ng-controller="DirController" ng-init="init()" ng-click="hide()" style="background-image: url(https://www.walldevil.com/wallpapers/a81/6583-network-dot-connection-line.jpg);">

 <div class="modal fade" id="upload" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
  <input type="hidden" name="_token" value="{{csrf_token()}}"/>
<div class="modal-header bg-header">
<h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-upload" aria-hidden="true"></i> Upload File?</h5>
<button class="close" type="button" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">×</span>
</button>
</div>
<div class="modal-body"><h6>Enter new names</h6>
  <input type="file" class="form-control" fileread="file" ng-model="file"/>
  <input type="text" hidden value="@{{path}}"/>
</div>
<div class="modal-footer bg-header">
<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
<input type="button" class="btn btn-primary" data-dismiss="modal" ng-click="UploadFile('{{csrf_token()}}')" value="Upload"/>
</div>
</div>
</div>
</div>

  <!-- Navigation-->
  <nav class="navbar navbar-expand-lg navbar-dark fixed-top " id="mainNav" style="background-color: #356fa1;">

    <a class="navbar-brand" href="index.html">EZ-Directory</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    
    <div class="collapse navbar-collapse  " id="navbarResponsive" >

  <div class="page page--home">
  
  <nav role='navigation'>
    
    <ul class="hoz-nav">
      <li class="hoz-nav__item">
        <a class="nav-home" href="#"> </a>
      </li>
      
      <li class="hoz-nav__item">
        <a class="nav-about" href="#"></a>
      </li>

      </li>
    </ul>
    
  </nav>  
  
</div>
 &nbsp;
  &nbsp; &nbsp;
          
         <button class="btn btn-theme" type="button"  name="notes" id="notes" style="border-radius: 0px;" checked />Notes</button>
         &nbsp;
        <button class="btn btn-theme" type="button"  name="notes" id="assignment" style="border-radius: 0px;" />assignmnet</button>
          
    
        
    

      <ul class="navbar-nav navbar-sidenav  " id="exampleAccordion style-2" style=" overflow-x: scroll; ">

           <script type="text/ng-template" id="categoryTree">
            

           <a class="nav-link nav-link-collapse collapsed " data-toggle="collapse" href="#" data-target="#demo@{{d.id}}" ng-dblclick="opentree(d.path)">
            <i class="fa fa-fw fa-file" ng-show="isFile(d.name)" style="color:black"></i>
            <i class="fa fa-fw fa-folder" ng-hide="isFile(d.name)" style="color:black"></i>
            
            <span class=" " style="color: black;" title="@{{d.name}}"> @{{  d.name.slice(0,5)+'..'}}</span>
          </a>



    <ul ng-if="d.dirs" class="sidenav collapse" id="demo@{{d.id}}" >
        <li ng-repeat="d in d.dirs" ng-include="'categoryTree'" class="nav-item" data-toggle="tooltip" data-placement="right" title="Components">           
        </li>
    </ul>

<script type="text/ng-template" id="categoryTree3">
   
           <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#" data-target="#demo@{{d.id}}" ng-dblclick="asopentree(d.path)">
            <i class="fa fa-fw fa-file" ng-show="isFile(d.name)" style="color:black;"></i>
            <i class="fa fa-fw fa-folder" ng-hide="isFile(d.name)" style="color:black;"></i>
            
            <span class="" style="color:black; " title="@{{d.name}}"> @{{ d.name.slice(0,5)+'..'}}</span>
          </a>
         


    <ul ng-if="d.dirs" class="sidenav collapse" id="demo@{{d.id}}">
        <li ng-repeat="d in d.dirs" ng-include="'categoryTree3'" class="nav-item" data-toggle="tooltip" data-placement="right" title="Components">           
        </li>
    </ul>
</script>

  
    <li ng-repeat="d in dirs" ng-include="'categoryTree'" ng-hide="astab" class="nav-item" data-toggle="tooltip" data-placement="right" title="Components"></li>
  


      <li ng-repeat="d in asdirs" ng-include="'categoryTree3'" ng-show="astab" class="nav-item" data-toggle="tooltip" data-placement="right" title="Components"></li>

  
      </ul>

     
      <ul class="navbar-nav ml-auto">


        <li class="nav-item">
          <input class="form-styling" type="text" ng-hide="astab" placeholder="  Search " ng-change="search(key)" ng-model="key"/>
          
        </li>
        <li class="nav-item">
          <input class="form-styling" type="text" ng-show="astab" placeholder="     Search"  ng-change="assearch(akey)" ng-model="akey"/>
        </li>

      

        

              <li class="nav-item">
                <img src="https://image.flaticon.com/icons/svg/17/17004.svg" height="30" width="30
                " style="border-radius: 50px;"> 
              </li>
              <li class="nav-item">
                <a class="nav-link" data-toggle="modal" data-target="#exampleModal">
                  <i class="fa fa-fw fa-sign-out"></i>Logout</a>
                </li>
              </ul>

            </div>
          </nav>







          <style type="text/css">
          @import url("https://fonts.googleapis.com/css?family=Lato");

          .tabs {

            float: none;
            list-style: none;
            position: relative;
            margin: 0px 0 0 0px;
            text-align: left;
          }
          .tabs li {
            float: left;
            display: block;
          }
          .tabs input[type="radio"] {
            position: absolute;
            top: 0;
            left: -9999px;
          }
          .tabs label {
            display: block;
            padding: 10px;
            border-radius: 2px 2px 0 0;
            font-size: 15px;
            font-weight: normal;
            text-transform: uppercase;
            background: #aaa;
            cursor: pointer;
            position: relative;
            top: 2px;
            -moz-transition: all 0.2s ease-in-out;
            -o-transition: all 0.2s ease-in-out;
            -webkit-transition: all 0.2s ease-in-out;
            transition: all 0.2s ease-in-out;
          }
          .tabs label:hover {
            background: #6988a3;
          }
          .tabs .tab-content {
            z-index: 2;
            display: none;
            overflow: hidden;
            width: 100%;
            font-size: 15px;
            line-height: 25px;
            padding:10px;
            position: absolute;
            top: 53px;
            left: 0;
            background: #fff;
          }
          .tabs [id^="tab"]:checked + label {
            top: 0;
            padding-top: 15px;
            background: #356fa1;
          }
          .tabs [id^="tab"]:checked ~ [id^="tab-content"] {
            display: block;
          }

          p.link {
            clear: both;
            margin: 380px 0 0 15px;
          }
          p.link a {
            text-transform: uppercase;
            text-decoration: none;
            display: inline-block;
            color: #fff;
            padding: 5px 10px;
            margin: 0 5px;
            background-color: #356fa1;
            -moz-transition: all 0.2s ease-in;
            -o-transition: all 0.2s ease-in;
            -webkit-transition: all 0.2s ease-in;
            transition: all 0.2s ease-in;
          }
          p.link a:hover {
            background-color: #356fa1;
          }
          .show1 {
            z-index:1000;
            position: absolute;
            background-color:#C0C0C0;
            border: 1px solid blue;
            padding: 2px;
            display: block;
            margin: 0;
            list-style-type: none;
            list-style: none;
          }

          .hide {
            display: none;
          }

          .show1 li{ list-style: none; }
          .show1 a { border: 0 !important; text-decoration: none; }
          .show1 a:hover { text-decoration: underline !important; }
          .disabled {
              pointer-events: none;
              cursor: default;
          }

          .breadcrumb {
    padding: .40rem 1rem;
    border-radius: 0px;

  }

  a{
    font-family: 'Open Sans', sans-serif;
    color:black;
     text-decoration: none;


  }

  a:hover
  {
    color:#356fa1;
    text-decoration:none;
    cursor:pointer;
   }


          #tab-content1,#tab-content2{
                margin-left: 15px;
          }

          .bg-header{
            background-color: #eee;
          }

          .card{
  box-shadow:  0px 0px 3px 0px #3b3b3b, inset 0px 0px 0px 0px #3b3b3b;
  min-width: 180px;

  
}

.card-body{
  background-color: #fff;
  border-radius: 0px;
}


* {
    font-family: sans-serif;
}

#crouton ul {
    margin: 0;
    padding: 0;
    overflow: hidden;
    width: 100%;
    list-style: none;
}

#crouton li {
    float: left;
    margin: 0 10px;
}

#crouton a {
    background: #ddd;
  padding: .7em 1em;
  float: left;
  text-decoration: none;
  color: #444;
  text-shadow: 0 1px 0 rgba(255,255,255,.5);
  position: relative;
}

#crouton li:first-child a {
    border-top-left-radius: 5px;
    border-bottom-left-radius: 5px;
}

#crouton li:last-child a {
    border-top-right-radius: 5px;
    border-bottom-right-radius: 5px;
    background-color: #bbb;
}

#crouton a:hover {
    background: #99db76;
}

#crouton li:not(:first-child) > a::before {
    content: "";
    position: absolute;
    top: 50%;
    margin-top: -1.5em;   
    border-width: 1.5em 0 1.5em 1em;
    border-style: solid;
    border-color: #ddd #ddd #ddd transparent;
    left: -1em;
}

#crouton li:not(:first-child) > a:hover::before {
    border-color: #99db76 #99db76 #99db76 transparent;
}

#crouton li:not(:last-child) > a::after {
  content: "";
  position: absolute;
  top: 50%;
  margin-top: -1.5em;   
  border-top: 1.5em solid transparent;
  border-bottom: 1.5em solid transparent;
  border-left: 1em solid #ddd;
  right: -1em;
}

#crouton li:not(:last-child) > a:hover::after {
    border-left-color: #99db76;
}

.form-styling {
  width: 100%;
  height: 35px;

  border: none;
  border-radius: 20px;

  background: #c2ccd1;
}


.form-styling2 {
  width: 100%;
  height: 35px;

  padding-left: 20px;
  
  border: none;
  border-radius: 20px;

  background: #fff;
}



#breadcrumb {
  list-style: none;
  display: inline-block;
}
#breadcrumb .icon {
  font-size: 14px;
}
#breadcrumb li {
  float: left;
}
#breadcrumb li a {
  color: #FFF;
  display: block;
  background: #3498db;
  text-decoration: none;
  position: relative;
  height: 40px;
  line-height: 40px;
  padding: 0 10px 0 5px;
  text-align: center;
  margin-right: 23px;
}
#breadcrumb li:nth-child(even) a {
  background-color: #2980b9;
}
#breadcrumb li:nth-child(even) a:before {
  border-color: #2980b9;
  border-left-color: transparent;
}
#breadcrumb li:nth-child(even) a:after {
  border-left-color: #2980b9;
}
#breadcrumb li:first-child a {
  padding-left: 15px;
  -moz-border-radius: 4px 0 0 4px;
  -webkit-border-radius: 4px;
  border-radius: 4px 0 0 4px;
}
#breadcrumb li:first-child a:before {
  border: none;
}
#breadcrumb li:last-child a {
  padding-right: 15px;
  -moz-border-radius: 0 4px 4px 0;
  -webkit-border-radius: 0;
  border-radius: 0 4px 4px 0;
}
#breadcrumb li:last-child a:after {
  border: none;
}
#breadcrumb li a:before, #breadcrumb li a:after {
  content: "";
  position: absolute;
  top: 0;
  border: 0 solid #3498db;
  border-width: 20px 10px;
  width: 0;
  height: 0;
}
#breadcrumb li a:before {
  left: -20px;
  border-left-color: transparent;
}
#breadcrumb li a:after {
  left: 100%;
  border-color: transparent;
  border-left-color: #3498db;
}
#breadcrumb li a:hover {
  background-color: #1abc9c;
}
#breadcrumb li a:hover:before {
  border-color: #1abc9c;
  border-left-color: transparent;
}
#breadcrumb li a:hover:after {
  border-left-color: #1abc9c;
}
#breadcrumb li a:active {
  background-color: #16a085;
}
#breadcrumb li a:active:before {
  border-color: #16a085;
  border-left-color: transparent;
}
#breadcrumb li a:active:after {
  border-left-color: #16a085;
}


.dropdown1 {
    position: relative;
    display: inline-block;
}

.dropdown1-content {
    display: block;
    position: absolute;
    background-color: #f9f9f9;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    padding: 12px 16px;
    z-index: 1;
}

          </style>



          <input type="hidden" value="{{Auth::User()->email}}" id="useremail"/>
          <div class="content-wrapper" style="padding-right: 20px; margin-top: -50px; background-color: transparent;">
               

            <ul class="tabs" role="tablist">
              <li>


                <input hidden type="radio" name="tabs" ng-click="astab=false" id="tab1" ng-model="notes"/>
                <label hidden for="tab1" 
                role="tab" 
                aria-selected="true" 
                aria-controls="panel1" 
                tabindex="0" style="color: white;">Notes</label>


                <div id="tab-content1"  
                class="tab-content container-fluid" 
                role="tabpanel" 
                aria-labelledby="description" 
                aria-hidden="false">

                <div class="pull-left">
      
            <ul class="container-fluid pull-left" id="breadcrumb" style="margin:0px; ">

                  <li class="" style="font-size: 20px;"> <a href="#" ng-click="init()" ><i class="fa fa-home" aria-hidden="true"></i> My Folders</a></li> 
                  <li class="" ng-repeat="p in range(4,paths.length-2)">
                    <a href="" ng-click="back(p)" style="font-size: 20px;">@{{path[p].length>8?path[p].slice(0,6)+'...':path[p]}} </a>
                  </li>
                  <li class="" ng-hide="paths.length<5" style="font-size: 20px;"><a href="#">@{{paths[paths.length-1]}}</a></li>

                </ul>
              </div>

                <div class="container">

                   <button class="btn btn-success pull-right " data-toggle="modal" title="Add Folder" data-target="#newfolder" style="margin-right: 15px;"><i class="fa fa-plus"></i> </button>

                  <input type="hidden" value="{{Auth::User()->id}}" id="userid"/>

                   <button class="btn pull-right" ng-disabled="isRoot()" data-toggle="modal" data-target="#upload" title="upload File" style="margin-right: 10px; background-color: #356fa1 ; color:white;"><i class="fa fa-upload"></i> Upload </button>

                   <a href="#" id="fff" title="download" class="btn btn-success pull-right " ng-show="showoptions('dwn')" style="margin-right: 15px;"><i class="fa fa-download"></i> </a>
                   <button class="btn btn-danger pull-right " data-toggle="modal" data-target="#deletemodal" ng-click="PreDelete(delsel,delt)" ng-show="showoptions('del')" title="delete" style="margin-right: 15px;"><i class="fa fa-remove"></i> </button>

                    <button class="btn btn-warning pull-right " data-toggle="modal" data-target="#renamefolder" title="rename" ng-show="showoptions('rnm')" style="margin-right: 15px;" ><i class="fa fa-clipboard"></i> </button>
</div>

<br>


        

<br>
         <hr>

    

                <div class="row" >

                  <div class="col-xl-2 col-sm-2 mb-3" ng-repeat="f in range(0,folders.length-1)"  >
                    <div class="card text-white bg-primary o-hidden">
                      <a href="" class="text-white "  ng-click="selected(f,1)" ng-dblclick="open(folders[f])" ng-mousedown="rightclick($event,f,1)">
                        <div class="card-body" id="sel@{{f}}" >
                          <div class="card-body-icon">
                            <i class="fa fa-fw fa-folder" style="color: #aaa;"></i>
                          </div>
                          <div class="mr-5" style="color: black;" title="@{{folders[f]}}">@{{folders[f].length>8?folders[f].slice(0,6)+'...':folders[f]}}</div>
                        </div>

                      </a>


                    </div>
                 <div class="w3-dropdown" >
                    <div class="w3-dropdown-content w3-bar-block w3-border hide"  id="rmenu@{{f}}">
                    
 

                          <a href="#" style="text-decoration:none;" class="w3-bar-item w3-button" ng-click="PRename(f)" data-toggle="modal" data-target="#renamefolder">Rename</a>
                          <a href="#" style="text-decoration:none;" class="w3-bar-item w3-button" data-toggle="modal" data-target="#deletemodal" ng-click="PreDelete(f,1)" >Delete</a>
                      <br>
    <!--                       <a href="#" style="text-decoration:none;" class="w3-bar-item w3-button" ng-click="Move(f,1)"> Move</a>
                          
<br>
                          <a class="w3-bar-item w3-button" ng-hide="pastehide" href="#" ng-click="Paste(f)">Paste</a>
     -->                   
                          <a class="w3-bar-item w3-button" id="df@{{f}}" href="{{URL::to('/dir/downloadfolder')}}/?path=@{{path}}&name=@{{folders[f]}}"  >Download</a>
                     

                    </div>
                  </div>
                   
                  </div>


                  <div class="col-xl-2 col-sm-2 mb-3" ng-repeat="f in range(0,files.length-1)" >
                    <div class="card text-white bg-primary o-hidden ">
                      <a href="#" class="text-white " ng-click="selected(f,2)" ng-mousedown="rightclick($event,f,2)">
                        <div class="card-body" id="fsel@{{f}}" >
                          <div class="card-body-icon">
                            <i class="fa fa-fw fa-file" style="color: #aaa;"></i>
                          </div>
                          <div class="mr-5" style="color: black;" title="@{{files[f]}}">@{{files[f].length>8?files[f].slice(0,6)+'...':files[f]}}</div>
                        </div>

                      </a>

                    </div>
                                 <div class="w3-dropdown" >
    
                     <div class="w3-dropdown-content w3-bar-block w3-border hide" id="frmenu@{{f}}"  >
                        
                          <!-- <a href="#" class="w3-bar-item w3-button" ng-click="Move(f,2)"  style="background-color:#fff;">Move</a> -->
                          <a href="#" class="w3-bar-item w3-button" ng-click="PreDelete(f,2)" data-toggle="modal" data-target="#deletemodal" >Delete</a>
                        <div ng-show="ifZip(f)">
                          <a href="#" class="w3-bar-item w3-button" ng-click="Extract(f)"  >Extract</a>
                        </div>
                        <a id="fdf@{{f}}" class="w3-bar-item w3-button" href="{{URL::to('/dir/downloadfile')}}/?path=@{{path}}&name=@{{files[f]}}" >Download</a>
                        
                    </div>
                  </div>
                  </div>



                    <br>
                    <br><br><br><br><br><br>
                     <br><br><br><br><br><br>
                
                </div>
             



              </div>
            </li>

            <li>
              <input hidden type="radio" name="tabs" ng-click="asinit()" id="tab2" ng-model="assignment" />
              <label hidden for="tab2"
              role="tab" 
              aria-selected="false" 
              aria-controls="panel2" 
              tabindex="0" style="color: white;">Assignments</label>
              <div id="tab-content2" 
              class="tab-content"
              role="tabpanel" 
              aria-labelledby="specification" 
              aria-hidden="true">

              <div class="pull-left">
              
                 <ul class="container-fluid pull-left" id="breadcrumb" >
                  <li class="" style="font-size: 20px;"> <a href="#" ng-click="asinit()"><i class="fa fa-home" aria-hidden="true"></i> Assignments</a></li> 
                  <li class="" ng-repeat="p in range(4,aspaths.length-2)" style="font-size: 20px;">
                    <a href="#" ng-click="asback(p)" style="font-size: 20px;">@{{aspath[p].length>8?aspath[p].slice(0,6)+'...':aspath[p]}} </a>
                  </li>
                  <li class="breadcrumb-item active" ng-hide="aspaths.length<5" style="font-size: 25px;"><a href="#">@{{aspaths[aspaths.length-1]}}</a></li>
                </ul>
              </div>
<div class="container ">
                <button class="pull-right btn btn-success" data-toggle="modal" title="create assignment folder" data-target="#newassignment" style="margin-right: 10px;">
            <i class="fa fa-plus"></i> </button>
                     <a href="#" id="afff" class="btn btn-success pull-right " ng-show="showoptions('dwn')" style="margin-right: 15px;" title="download"><i class="fa fa-download"></i> </a>
                   <button class="btn btn-warning pull-right " data-toggle="modal" ng-click="Address(dirname)" title="view folder address" ng-show="showoptions('vwa')" data-target="#viewaddress" style="margin-right: 15px;"><i class="fa fa-link"></i> </button>

<button class="btn btn-danger pull-right"  ng-click="PreDelete(delsel,delt)" data-toggle="modal" data-target="#deletemodal" ng-show="showoptions('del')" style="margin-right: 15px;" title="delete"><i class="fa fa-remove"></i> </button>

     <div class="col-md-6 pull-right container">
   <input class="form-styling2" type="text" id="copyaddresslink" class="form-control" ng-click="copyAddress($event)" ng-model="diraddress" placeholder="Link" />
</div>
</div>



    
   
        
       
  
       <br>
       <br>


           <hr>
     
 
              <div class="row container-fluid">

                  <div class="col-xl-2 col-sm-2 mb-3" ng-repeat="f in range(0,asfolders.length-1)" >
                    <div class="card text-white bg-primary o-hidden">
                      <a href="#" class="text-white " ng-click="selected(f,3)" ng-dblclick="asopen(asfolders[f])" ng-mousedown="rightclick($event,f,5)">
                        <div class="card-body" id="asel@{{f}}">
                          <div class="card-body-icon">
                            <i class="fa fa-fw fa-folder" style="color: #aaa;"></i>
                          </div>
                          <div class="mr-5" style="color: black;" title="@{{asfolders[f]}}">@{{asfolders[f].length>8?asfolders[f].slice(0,6)+'...':asfolders[f]}}</div>
                        </div>

                      </a>




                    </div>
                    <div class="hide" id="asrmenu@{{f}}">
                      <ul>
                        
                        <li>
                            <a href="#" data-toggle="modal" data-target="#viewaddress" ng-click="Address(asfolders[f])">View Address</a>
                        </li>
                        <br>
                        <li>
                            <a href="#" data-toggle="modal" data-target="#deletemodal" ng-click="PreDelete(f,3)">Delete</a>
                        </li>
                        <br>
                        <li>
                          <a id="adf@{{f}}" href="{{URL::to('/dir/downloadasfolder')}}/?path=@{{aspath}}&name=@{{asfolders[f]}}" >Download</a>
                        </li>
                        
                      </ul>
                    </div>
                  </div>
                   <div class="col-xl-2 col-sm-2 mb-3" ng-repeat="f in range(0,asfiles.length-1)">
                    <div class="card text-white bg-primary o-hidden ">
                      <a href="#" class="text-white " ng-click="selected(f,4)" ng-mousedown="rightclick($event,f,6)">
                        <div class="card-body" id="afsel@{{f}}">
                          <div class="card-body-icon">
                            <i class="fa fa-fw fa-file"  style="background-color:#fff; color: #aaa;"></i>
                          </div>
                          <div class="mr-5" style="color: black;" title="@{{asfiles[f]}}">@{{asfiles[f].length>8?asfiles[f].slice(0,6)+'...':asfiles[f]}}</div>
                        </div>

                      </a>

                    </div>



                     <div class="hide" id="asfrmenu@{{f}}">
                      <ul>
                        <li>
                          <a href="#" ng-click="PreDelete(f,4)" data-toggle="modal" data-target="#deletemodal"  style="background-color:#fff;">Delete</a>
                        </li>
                       
                        <br>
                        <li>
                        <a "afdf@{{f}}" href="{{URL::to('/dir/downloadfile')}}/?path=@{{aspath}}&name=@{{asfiles[f]}}"  style="background-color:#fff;">Download</a>
                        </li>
                      </ul>
                    </div>
                  </div>

                    <br>
                    <br><br><br><br><br><br>


              </div>



              
            </div>
          </li>



        </ul>
       </div>




      <!-- Example DataTables Card-->
      <!-- /.container-fluid-->
      <!-- /.content-wrapper-->
      <footer class="sticky-footer">
        <div class="container">
          <div class="text-center">
            <small>Copyright © Incube</small>
          </div>
        </div>
      </footer>
      <!-- Scroll to Top Button-->
      <a class="scroll-to-top rounded" href="#page-top">
        <i class="fa fa-angle-up"></i>
      </a>
      <!-- Logout Modal-->
      <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header bg-header">
              <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-sign-out" aria-hidden="true"></i> Ready to Leave?</h5>
              <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
            <div class="modal-footer bg-header">
              <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
              <a class="btn btn-primary" href="login.html">Logout</a>
            </div>
          </div>
        </div>
      </div>


 <div class="modal fade" id="newassignment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header bg-header">
<h5 class="modal-title" id="exampleModalLabel"> <i class="fa fa-plus" aria-hidden="true"></i> Add New Assignment <i class="fa fa-file" aria-hidden="true"></i></h5>
<button class="close" type="button" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">×</span>
</button>
</div>

<div class="modal-body"><h6>Enter Title</h6>
<input type="text" class="form-control" id="asname"/></div>
<div class="modal-footer bg-header">

<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
<a class="btn btn-primary" href="#" data-dismiss="modal"  ng-click="AsCreate()">Create</a>
</div>
</div>
</div>
</div>      




 <div class="modal fade" id="renamefolder" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header bg-header">
<h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Rename Folder?</h5>
<button class="close" type="button" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">×</span>
</button>
</div>
<div class="modal-body"><h6>Enter new names</h6><input type="text" class="form-control" ng-model="newname"/></div>
<div class="modal-footer bg-header">
<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
<a class="btn btn-primary" href="#" data-dismiss="modal"  ng-click="Rename()">Rename</a>
</div>
</div>
</div>
</div>


<div class="modal fade" id="newfolder" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-header">
        <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-plus" aria-hidden="true"></i> Add Folder?</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body"><h6>Enter Name of Folder</h6><input type="text" class="form-control" id="foldername"/></div>
      <div class="modal-footer bg-header">
        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
        <a class="btn btn-primary" href="#" data-dismiss="modal" ng-click="addFolder()">Add</a>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="viewaddress" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header bg-header">
<h5 class="modal-title" id="exampleModalLabel">Address</h5>
<button class="close" type="button" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">×</span>
</button>
</div>
<input type="text" id="copylink" ng-model="diraddress" disabled/>
<!-- <text class="form-control" id="copylink">@{{diraddress}}</text> -->

<div class="modal-body">
  <!-- <input type="text" class="form-control" ng-model="newname"/></div> -->
<div class="modal-footer bg-header">

<button class="btn btn-secondary" ng-click="copyAddress('#copylink')" type="button">Copy Link</button>
<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>

<!-- <a class="btn btn-primary" href="#" data-dismiss="modal"  ng-click="Rename()">Rename</a> -->
</div>
</div>
</div>
</div>
</div>



<div class="modal fade" id="deletemodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header bg-header">
<h5 class="modal-title" id="exampleModalLabel">Delete?</h5>
<button class="close" type="button" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">×</span>
</button>
</div>
<div class="modal-body">
<text class="form-control">Are you sure want to delete?</text>
  <!-- <input type="text" class="form-control" ng-model="newname"/></div> -->
<div class="modal-footer bg-header">
<button class="btn btn-secondary" type="button" id="deletebutton" data-dismiss="modal" ng-click="Delete(delsel,delt)">Delete</button>

<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
<!-- <a class="btn btn-primary" href="#" data-dismiss="modal"  ng-click="Rename()">Rename</a> -->
</div>
</div>
</div>
</div>
</div>







 
<!-- Bootstrap core JavaScript-->
<script src="{{asset('newfrontend/vendor/jquery/jquery.min.js')}}">
</script>
<script src="{{asset('newfrontend/vendor/popper/popper.min.js')}}"></script>
<script src="{{asset('newfrontend/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<!-- Core plugin JavaScript-->
<script src="{{asset('newfrontend/vendor/jquery-easing/jquery.easing.min.js')}}"></script>
<!-- Page level plugin JavaScript-->
<script src="{{asset('newfrontend/vendor/chart.js/Chart.min.js')}}"></script>
<script src="{{asset('newfrontend/vendor/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('newfrontend/vendor/datatables/dataTables.bootstrap4.js')}}"></script>
<!-- Custom scripts for all pages-->
<script src="{{asset('newfrontend/js/sb-admin.min.js')}}"></script>
<!-- Custom scripts for this page-->
<script src="{{asset('newfrontend/js/sb-admin-datatables.min.js')}}"></script>
<script src="{{asset('newfrontend/js/sb-admin-charts.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.5/angular.min.js"></script>
<script src="{{asset('dirjs/dirs.js')}}"></script>
<script>
$(document).ready(function() {
  $(document).mousedown(function(e){
    // if(e.button==1){
    //   $('#rmenu0').attr('class','hide');
    //   console.log("asd")
    // }
  });
document.oncontextmenu = function() {return false;};
$('[id *= "rightclick"]').click(function(){
  console.log("asd");
})
$('[id *= "rightclick"]').mousedown(function(e){ 

  if( e.button == 2 ) { 
    var id=$(this).attr('id');
    console.log(id);
    $('#rmenu').attr('class','show');
    console.log(mouseX(e));
    var a=mouseX(e);
    var b= mouseY(e);
    $('#rmenu').attr('style','left:'+a);
    $('#rmenu').attr('style','top:'+b);
// document.getElementById("rmenu").style.left = mouseX(event) + 'px';

return false; 
} 
return true; 
});

//     if ($("#test").addEventListener) {
//         $("#test").addEventListener('contextmenu', function(e) {
//             alert("You've tried to open context menu"); //here you draw your own menu
//             e.preventDefault();
//         }, false);
//     } else {

//         //document.getElementById("test").attachEvent('oncontextmenu', function() {
//         //$(".test").bind('contextmenu', function() {
//             $('body').on('contextmenu', 'a.test', function() {


//             //alert("contextmenu"+event);
//             document.getElementById("rmenu").className = "show";  
//             document.getElementById("rmenu").style.top =  mouseY(event) + 'px';
//             document.getElementById("rmenu").style.left = mouseX(event) + 'px';

//             window.event.returnValue = false;


//         });
//     }

// });

// // this is from another SO post...  
//     $(document).bind("click", function(event) {
//         document.getElementById("rmenu").className = "hide";
//     });



function mouseX(evt) {
  if (evt.pageX) {
    return evt.pageX;
  } else if (evt.clientX) {
    return evt.clientX + (document.documentElement.scrollLeft ?
      document.documentElement.scrollLeft :
      document.body.scrollLeft);
  } else {
    return null;
  }
}

function mouseY(evt) {
  if (evt.pageY) {
    return evt.pageY;
  } else if (evt.clientY) {
    return evt.clientY + (document.documentElement.scrollTop ?
      document.documentElement.scrollTop :
      document.body.scrollTop);
  } else {
    return null;
  }
}
});
</script>
<script>
          $(document).ready(function(){
            $('#notes').click(function(){
              $('#tab1').trigger('click');
            })
            $('#assignment').click(function(){
              $('#tab2').trigger('click');
            })
          })
        </script>

<script type="text/javascript">
  /*!
 * This is just to simulate the page changes
 * It alters the container class
 */

$( document ).ready( function() {
  var $navItems = $( '.hoz-nav__item a' ),
      $page = $( '.page' ),
      linkClass = null,
      linkName = null,
      newPageClass = null;
  
  $navItems.on( 'click', function(e) {
    e.preventDefault();
    
    var $this = $( this );
    
    linkClass = $this.attr( 'class' );
    linkName = linkClass.split('-')[1];
    newPageClass = 'page--' + linkName;
    
    $page.attr( 'class', '' );
    
    $page.addClass( 'page ' + newPageClass );
  });

  $('#notes').trigger('click');
});
</script>


</body>

</html>
