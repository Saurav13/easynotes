@extends('admin.admin')
@section('body')
	<div class="container-fluid">
	  <h3>Opportunities        <button name="Add Opportunities Details" id="oppoadd" class="btn btn-md btn-primary" data-toggle="modal" data-target="#addOppo"><i class="fa fa-plus" aria-hidden="true"></i> Add Opportunites Detail</button>
</h3>

	  <br>
      <div class="card">
      <div cass="container-fluid">
        <div class="content table-responsive table-full-width">
	      <table class="table" id="table1">
	        <thead>
	          <th>ID</th>
	          <th>Title</th>
	          <th>Description</th>
	          <th>Image</th>
	          <th>Registered Date</th>
            <th>Visitors</th>
	          <th>Actions</th>
	        </thead>
	        <tbody>


	          @foreach($opportunities as $o)
	          <tr>
	            <th>{{$o->id}}</th>
	            <td id="title{{$o->id}}">{{$o->title}}</td>
	            <td>{{substr(strip_tags($o->description),0,50)}}{{strlen($o->description)>50 ? "...":"" }}</td> 
	            <td><img src="{{asset('uploads/opportunitiesimages/'. $o->picture)}}" height="50" width="50" style="border: 2px solid; border-radius: 10px;"></td>
	            <td>{{date('M j,Y',strtotime($o->created_at))}}</td>
	            <td>{{Counter::show('opportunities',$o->id)}}</td>
              <td><a class="btn btn-default btn-sm"  data-toggle="modal" data-target="#showOppo" cid="{{$o->id}}"  id="view{{$o->id}}">View</a> 
	              <a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#editOppo" cid={{$o->id}} id="update{{$o->id}}">Edit</a>
	                <a  cid="{{$o->id}}"  class="btn btn-danger btn-sm" value="Delete" id="delete{{$o->id}}">Delete
	                </a>
	              
	              <input type="hidden" id="description{{$o->id}}" value="{{$o->description}}" />
	              <input type="hidden" id="picture{{$o->id}}" value="{{$o->picture}}" />
	            </tr>

	            @endforeach
	          </tbody>

	        </table>
	       </div>
	      </div>

	    </div>
	  </div>
     <script>
   $('#addtitle').keyup(function(){
              var text=$('#addtitle').val();

              var alias=text.split(' ').join('-').toLowerCase();
              // console.log(text,alias);
              $('#addalias').val(alias);
            });
    $('#edittitle').keyup(function(){
              var text=$('#edittitle').val();

              var alias=text.split(' ').join('-').toLowerCase();
              // console.log(text,alias);
              $('#editalias').val(alias);
            });
  </script>
@endsection

{{-- Add opportunities --}}
<div id="addOppo" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Opportunities Detail</h4>
      </div>
      <div class="modal-body">
        <form action="{{route('opportunities.store')}}" enctype="multipart/form-data" method="POST">
          {{csrf_field()}}
          <label for="title">Title:</label>
          <input type="text" name="title" id="addtitle" class="form-control">

           <label for="alias">Alias</label>
            <input type="text" required name="alias" id="addalias" class="form-control"/>

          {{Form::label('description','Description:')}}
          {{Form::textarea('description',null/*'Enter the description' yo garyo bhane chain placeholder jastai kam garcha*/,array('class'=>'form-control','id'=>'addeditor'))}}
          <input type="file" id="upload" hidden/>
          <label for="picture">Image:</label>
          <input type="file" name="picture">
          <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
   
          <script>
            tinymce.init({
            selector: "textarea#addeditor",
            
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern"
            ],
            toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
            toolbar2: "print preview | forecolor backcolor emoticons | template",
            image_advtab: true,
            file_picker_callback: function(callback, value, meta) {
            if (meta.filetype == 'image') {
                $('#upload').trigger('click');
                $('#upload').on('change', function() {
                var file = this.files[0];
                var reader = new FileReader();
                reader.onload = function(e) {
                    callback(e.target.result, {
                    alt: ''
                    });
                };
                reader.readAsDataURL(file);
                });
            }
            },
            templates: [
          {title: 'Newsletter1', description: 'Notice', url: "/templates/newsletter.html"}
        ]
        });
          </script>	
        </div>
        <div class="modal-footer">
          <input type="submit" name="submit" value="Add Details" class="btn btn-success">

        </div>
      </form>
    </div>

  </div>
</div>
  <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
  <script>
    $(document).ready(function(){
          tinymce.init({
            selector: "textarea#editdescription",
            
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern"
            ],
            toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
            toolbar2: "print preview | forecolor backcolor emoticons | template",
            image_advtab: true,
            file_picker_callback: function(callback, value, meta) {
            if (meta.filetype == 'image') {
                $('#upload1').trigger('click');
                $('#upload1').on('change', function() {
                var file = this.files[0];
                var reader = new FileReader();
                reader.onload = function(e) {
                    callback(e.target.result, {
                    alt: ''
                    });
                };
                reader.readAsDataURL(file);
                });
            }
            },
            templates: [
          {title: 'Newsletter1', description: 'Notice', url: "/templates/newsletter.html"}
        ]
        });
           
      $('[id *= "update"]').click(function(){

        var id= $(this).attr('cid');
        console.log(id);
        var title = $('#title'+id).html();
        var desc = $('#description'+id).val();
        var eid = $()
        $('#edittitle').val(title);
        var editor=tinymce.get('editdescription');
        editor.setContent(desc);
        $('#idOppo').val(id);
      });
    });
  </script>
  <script>
    $(document).ready(function(){
      $('[id *= "view"]').click(function(){

        var id= $(this).attr('cid');
        var title = $('#title'+id).html();
        var description = $('#description'+id).val();
        var date = $('#date'+id).html();
        var time = $('#time'+id).html();
        var picture = $('#picture'+id).val();
        console.log(picture);
        $('#showtitle').html(title);
        console.log(title);
        $('#editor2').html(description);
        $('#showdate').html(date);
        $('#showpicture').attr('src',"../../uploads/"+picture);

        $('#showtime').html("Time:"+time);
      });
    });
  </script>
    <script>
  $(document).ready(function(){

    $('[id *= "delete"]').click(function(event){

      var id= $(this).attr('cid');

      swal({
        title: "Are you sure?",
        text: "You will not be able to recover this file!",
        type: "warning",

        showCancelButton: true,

      }).then(function(){
        $.post("{{route('events.delete')}}",{id:id,_token:"{{csrf_token()}}"},function(data){
          swal({
            title:"Deleted Successfully",
            type:"success"

          }).then(function(){
            window.location.reload();
          })
        })
      });
    });
  });
</script>

 <script>
  $(document).ready(function(){

    $('[id *= "delete"]').click(function(event){

      var id= $(this).attr('cid');

      swal({
        title: "Are you sure?",
        text: "You will not be able to recover this file!",
        type: "warning",

        showCancelButton: true,

      }).then(function(){
        $.post("{{route('opportunities.delete')}}",{id:id,_token:"{{csrf_token()}}"},function(data){
          swal({
            title:"Deleted Successfully",
            type:"success"

          }).then(function(){
            window.location.reload();
          })
        })
      });
    });
  });
</script>
<script>
  $(document).ready(function(){
    $('[id *= "view"]').click(function(){

      var id= $(this).attr('cid');
      var title = $('#title'+id).html();
      var description = $('#description'+id).val();
      var picture = $('#picture'+id).val();
      console.log(picture);
      $('#showtitle').html(title);
      console.log(title);
      $('#editor2').html(description);
      $('#showpicture').attr('src',"../../uploads/opportunitiesimages/"+picture);
    });
  });
</script>

{{-- Edit Opportunities --}}
  <div id="editOppo" class="modal fade" role="dialog">
    <div class="modal-dialog">


      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit Opportunities Details</h4>
        </div>
        <div class="modal-body">
        {{-- {!!Form::model(['route'=>['opportunities.edit'],'method'=>"POST",'files'=>'true']) !!} --}}
        <form action="{{route('opportunities.edit')}}" method="POST" enctype="multipart/form-data">
            {{csrf_field()}}
            <label for="title">Title:</label>
            <input type="text" name="title" class="form-control" id="edittitle">

            <label for="alias">Alias</label>
            <input type="text" required name="alias" id="editalias" class="form-control"/>


             {{Form::label('description','Description:')}}
            {{Form::textarea('description',null,array('class'=>'form-control','id'=>'editdescription'))}} 
            <input type="file" id="upload1" hidden/>
            <label for="picture">Image:</label>
            <input type="file" name="picture">
            <script src="{{asset('/assets/ckeditor/ckeditor.js')}}"></script>
          
          </div>
          <div class="modal-footer">
            <input type="submit" name="submit" value="Update Details" class="btn btn-success">
            <input type="hidden" id="idOppo" name="id"/>

          </div>
          </form>
        
      </div>

    </div>
  </div>
</div>

<div id="showOppo" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title pull-left" id="showtitle"></h4>
        {{-- <h4 class="modal-title pull-right" id="showdate"></h4> --}}
      </div>
      <br>
      <div  align="center">
        <img src="" align="center" id="showpicture" style="height: 200px; width: 400px;" />
      </div>
      <br>
      <div class="modal-body">
        <p id="editor2"></p>
      </div>
      <div class="modal-footer">
        <form action="#" method="post">
          {{csrf_field()}}
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

        </form>
      </div>


    </div>

  </div>
</div>
