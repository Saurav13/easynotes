
<html>
  <head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title></title>
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,900" rel="stylesheet" type="text/css"/>
    <link href="{{asset('frontend-assets/css/bootstrap.min.css')}}" rel="stylesheet" />

<style>
  .reset, html, body, div, span, applet, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, a, abbr, acronym, address, big, cite, code, del, dfn, em, img, ins, kbd, q, s, samp, small, strike, strong, sub, sup, tt, var, b, u, i, center, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td, article, aside, canvas, details, embed, figure, figcaption, footer, header, hgroup, menu, nav, output, ruby, section, summary, time, mark, audio, video {
  margin: 0;
  padding: 0;
  border: 0;
  font: inherit;
  font-size: 100%;
  vertical-align: baseline;
  }

  .reset_2, caption, th, td {
    text-align: left;
    font-weight: normal;
    vertical-align: middle;
  }

  html {
    line-height: 1;
    position: relative;
    height: 100%;
    font-size: 16px;
    font-family: Lato, sans-serif;
  }

  body {
    height: 100%;
    width: 100%;
    margin: 0;
    padding-top: 53px;
    padding-bottom: 64px;
  }

  blockquote {
    quotes: none;
  }
  blockquote:before {
    content: "";
    content: none;
  }
  blockquote:after {
    content: "";
    content: none;
  }

  a img {
    border: none;
  }

  q {
    quotes: none;
  }
  q:before {
    content: "";
    content: none;
  }
  q:after {
    content: "";
    content: none;
  }

  ol {
    list-style: none;
  }

  ul {
    list-style: none;
  }

  table {
    border-collapse: collapse;
    border-spacing: 0;
  }

  article {
    display: block;
  }

  aside {
    display: block;
  }

  details {
    display: block;
  }

  figure {
    display: block;
  }

  figcaption {
    display: block;
  }

  footer {
    display: block;
  }

  header {
    display: block;
  }

  hgroup {
    display: block;
  }

  menu {
    display: block;
  }

  nav {
    display: block;
  }

  section {
    display: block;
  }

  summary {
    display: block;
  }

  main {
    display: block;
  }

  * {
    box-sizing: border-box;
  }

  .header {
    position: fixed;
    top: 0;
    left: 0;
    height: 53px;
    width: 100%;
  }

  .main {
    height: 100%;
  }

  .footer {
    position: absolute;
    left: 0;
    bottom: 0;
    height: 64px;
    width: 100%;
  }

  .team-menu {
    position: relative;
    width: 220px;
    height: 53px;
    line-height: 53px;
    font-weight: 900;
    padding: 0 1rem;
    color: #ffffff;
    background: #3e313c;
    border-bottom: 2px solid #372c36;
    float: left;
    cursor: pointer;
  }

  .channel-menu_name {
    display: inline-block;
    padding: 0 0.5rem 0 1.5rem;
    color: #555459;
    font-size: 1.4rem;
    font-weight: 900;
    line-height: 53px;
    cursor: pointer;
  }

  .channel-menu_prefix {
    color: #9e9ea6;
    padding-right: 0.1rem;
    font-weight: 500;
  }

  .listings {
    height: 100%;
    width: 220px;
    float: left;
    color: #ab9ba9;
    background-color: #4d394b;
    overflow-y: auto;
    overflow-x: hidden;
  }

  .message-history {
    margin-left: 220px;
    overflow-y: auto;
    overflow-x: hidden;
    height: 100%;
    padding: 0 18px 1rem 1.5rem;
    overflow: scroll;
    overflow-x: hidden;
  }

  .listings_channels {
    margin: 1rem 0 2rem;
  }

  .listings_header {
    text-align: left;
    font-size: 0.8rem;
    line-height: 1.25rem;
    margin: 0 1rem 0.1rem;
    text-transform: uppercase;
    font-weight: 700;
    color: #ab9ba9;
    width: 165px;
    position: relative;
  }

  .channel_list {
    list-style-type: none;
    text-align: left;
    color: #ab9ba9;
  }

  .channel {
    height: 24px;
    line-height: 24px;
    border-top-right-radius: 0.25rem;
    border-bottom-right-radius: 0.25rem;
    margin-right: 17px;
    color: #ffffff;
    padding-left: 1rem;
  }
  .channel.active {
    background: #4c9689;
  }

  .unread {
    color: #ffffff;
    background: #eb4d5c;
    border-radius: 9px;
    padding: 2px 9px;
    font-size: 0.8rem;
    line-height: 14px;
    font-weight: 700;
    vertical-align: baseline;
    white-space: nowrap;
    text-shadow: 0 1px 0 rgba(0, 0, 0, 0.2);
    float: right;
    margin-right: 3px;
    margin-top: 3px;
  }

  .channel_prefix {
    color: #b2d5c9;
  }

  .disclaimer {
    font-size: 0.8rem;
    padding-left: 1rem;
    margin-right: 17px;
  }

  .message {
    position: relative;
    margin-top: 0.5rem;
    padding: 0.25rem 2rem 0.1rem 3rem;
    min-height: 36px;
  }

  .message_profile-pic {
    position: absolute;
    left: 0;
    display: block;
    border-radius: 0.2rem;
    width: 36px;
    height: 36px;
    background-image: url(http://findicons.com/icon/download/102283/fh02/300/png);
    background-size: cover;
  }

  .message_username {
    font-weight: 900;
    padding-right: 0.25rem;
    color: #3d3c40 !important;
    margin-left: 0;
    font-style: normal;
    text-decoration: none;
  }

  .message_timestamp {
    text-align: left;
    display: inline;
    position: relative;
    top: 0;
    left: 0;
    color: #babbbf;
    font-size: 12px;
    line-height: 1.2rem;
    width: 36px;
    margin-right: 0;
    margin-left: 0;
  }

  .message_content {
    color: #8b898f;
    font-style: italic;
    display: block;
    min-height: 1rem;
  }

  .user-menu {
    float: left;
    width: 220px;
    height: 100%;
    cursor: pointer;
    background: #3e313c;
    border-top: 2px solid #372c36;
    padding: 7px 0 9px 8px;
    height: 4rem;
    position: fixed;
    bottom: 0;
    left: 0;
  }

  .user-menu_profile-pic {
    display: inline-block;
    float: left;
    border-radius: 0.2rem;
    width: 48px;
    height: 48px;
    background-image: url(http://findicons.com/icon/download/102283/fh02/300/png);
    background-size: cover;
    margin-right: 8px;
  }

  .user-menu_username {
    display: block;
    color: #ffffff;
    font-weight: 900;
    line-height: 1.5rem;
    margin-top: 0.2rem;
    max-width: 120px;
  }

  .connection_icon {
    width: 12px;
    height: 12px;
  }

  .connection_status {
    color: #ab9ba9;
  }

  .input-box {
    height: 100%;
    margin-left: 220px;
  }

  .input-box_text {
    font-size: 0.95rem;
    width: 90%;
    margin-left: 2%;
    margin-bottom: auto;
    line-height: 1.2rem;
    border: 2px solid #e0e0e0;
    border-radius: 0.2rem;
    background-clip: padding-box;
    color: #3d3c40;
    box-shadow: none;
    outline: 0;
    bottom: 0;
    min-height: 41px !important;
    padding: 9px 5px 9px 8px;
  }
  .online{
    background: rgb(66, 183, 42); border-radius: 50%; display: inline-block; height: 6px; margin-bottom: 3px; width: 6px;
  }
</style>

  </head>
  <body>
    <div class="header">
      <div class="team-menu">{{ $group->name }}</div>
      <div class="channel-menu"><span class="channel-menu_name">{{ Auth::user()->name }}</span></div>
    </div>
    <div class="main">
      <div class="listings">
        <div class="listings_direct-messages">
          <br>
          <h2 class="listings_header">Group Members <button data-toggle="modal" data-target="#addMembers" class="btn btn-primary pull-right" style="background-color:transparent"><i class="glyphicon glyphicon-plus"></i></button></h2>
          <br>
          <ul class="channel_list">
            @foreach($group->members as $member)
              <li class="channel"><span class="channel_name"><span class="prefix"> </span>{{ $member->name }}</span>&nbsp;&nbsp;&nbsp;<span class="online"></span></li>
            @endforeach          
          </ul>
        </div>
      </div>
      <div class="message-history">
        @foreach($group->group_chats as $message)
          <div class="message {{ $message->poster->id==Auth::user()->id ? 'pull-right':'' }}" style="clear:both"><span class="{{ $message->poster->id==Auth::user()->id ? '':'message_profile-pic' }}"></span><a class="message_username" href="">{{ $message->poster->name }}</a><span class="message_timestamp">{{ date('h:ia', strtotime($message->created_at)) }}</span><span class="message_star"></span><span class="message_content">{{ $message->content }}</span></div>
          <br>
        @endforeach
          
      </div>
    </div>
    <div class="footer">
      <div class="user-menu"><span class="user-menu_profile-pic"></span><span class="user-menu_username">{{ Auth::user()->name }}</span><img class="connection_icon" src="data:image/png;base64,iVBORw0KGgoAAAAN..."/><span class="connection_status">online</span></div>
      <div class="input-box">
        <form method="POST" action="{{ route('group.chat',$group->id) }}">
          {{ csrf_field() }}
          <input class="input-box_text" type="text" name="content"/>
          <button class="btn btn-primary" type="submit">Post</button>
        </form>
      </div>
    </div>
    <div id="addMembers" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Members</h4>
        </div>
        <div class="modal-body">
          <form method="POST" action="{{ route('groups.addMembers',$group->id) }}" >
            {{ csrf_field() }}
            <label for="chooseUsers">Select Members</label><br><br>
            <select class="form-control select2-multi"  name='newmembers[]' id="chooseUsers" multiple="multiple" width="100%">
              
            </select>
            <br><br>
            <button type="submit" class="btn btn-primary">Add</button>

          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>
  </body>

    <!--  Core Bootstrap Script -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="{{asset('frontend-assets/js/bootstrap.min.js')}}"  ></script>
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.min.css" rel="stylesheet" />
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/js/select2.min.js"></script>
<script type='text/javascript'>
    $('.select2-multi').select2({
      placeholder: "Select members",
      ajax: {
        url: "/user/group/"+{{$group->id}}+'/search',
        dataType: 'json',
        delay: 250,
        data: function (params) {
          return {
            q: params.term, // search term
          };
        },
        processResults: function (data, params) {

          var newData = [];
          for ( var i = 0; i < data.length; i++ ) {
                            newData.push({
                                id: data[i].id,  //id part present in data
                                text: data[i].name  //string to be displayed
                            });
                          }
          return {
            results: newData
          };
          
        },
        cache: true
      },
      escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
      minimumInputLength: 1
    });
  </script>
  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
  <script src="/js/socket.io.js"></script>
  <script type="text/javascript">
    var socket = io('http://localhost:6379');
    var channel = 'go-online:'+{{ Auth::user()->id }};
    socket.on(channel, function (data) {
      if('{{Auth::user()->name}}' == data.userName){
        var d = parseInt($('#notification').text())+1;
        $( "#notification" ).removeAttr('hidden');
        $( "#notification" ).html(d);
        var audio = new Audio('/ding.mp3');
        audio.play();
        document.title = "(" + d + ") " + 'Naipala';
      }
    });

  </script>
</html>