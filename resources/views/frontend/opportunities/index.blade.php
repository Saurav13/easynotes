@extends('frontend.main')
@section('body')
<div id="features-sec" class="container set-pad" style="padding-top: 10px;">
        <div class="row text-center">
            <div class="col-lg-8 col-lg-offset-2 col-md-8 col-sm-8 col-md-offset-2 col-sm-offset-2">
                <h1 data-scroll-reveal="enter from the bottom after 0.2s"  class="header-line">Opportunities</h1>
               
            </div>

        </div>
        <div class="row" >

            @foreach($oppo as $p)
       <!--      <a href="{{route('frontendopportunities.single',$p->id)}}">
            <div class="col-lg-4  col-md-4 col-sm-4" data-scroll-reveal="enter from the bottom after 0.4s">
                <div class="about-div">
                    <h3>{{$p->title}}</h3>
                    <hr />
                    <img style="height: 250px; width: 350px;" src="{{asset('uploads'.'/'.$p->image)}}" alt="{{$p->image}}" />
                    <hr />
                    <p >
                      {{substr(strip_tags($p->description),0,100)}}{{strlen($p->description)>100 ? "...":"" }}</p>
                </div>
            </div>
        </a> -->


                         <div class="example-2 card">
                    <div class="wrapper" style="background-image: url({{asset('uploads/opportunitiesimages'.'/'.$p->picture)}});">
                      <div class="header">
                        <div class="date">
                          <span class="day">12</span>
                          <span class="month">Aug</span>
                          <span class="year">2016</span>
                        </div>
                     <!--    <ul class="menu-content">
                          <li>
                            <a href="#" class="fa fa-bookmark-o"></a>
                          </li>
                          <li><a href="#" class="fa fa-heart-o"><span>18</span></a></li>
                          <li><a href="#" class="fa fa-comment-o"><span>3</span></a></li>
                        </ul> -->
                      </div>
                      <div class="data">
                        <div class="content">
                          <span class="author">Jane Doe</span>
                          <h1 class="title"><a href="{{route('frontendopportunities.single',$p->id)}}">{{$p->title}}</a></h1>
                          <p class="text">{{substr(strip_tags($p->description),0,100)}}{{strlen($p->description)>100 ? "...":"" }}</p>
                          <a href="{{route('frontendopportunities.single',$p->id)}}" class="button">Read more</a>
                        </div>
                      </div>
                    </div>
                  </div>
            @endforeach
           
           


        </div>
    </div>


    <style type="text/css">
  

a {
  text-decoration: none;
}

h1 {
  font-family: "Open Sans", sans-serif;
  font-weight: 300;
}

.row1{
  max-width: 1300px;
  margin: 0px auto 0;
}

@media screen and (min-width: 600px){
.card {
  float: left;
  padding: 1.7rem;
  width: 25%;
  border-radius: 10px;
}
}


@media screen and (max-width: 699px) and (min-width: 300px){
.card {
  float: left;
  padding: 1.7rem;
  width: 100%;


}
}
.card .menu-content {
  margin: 0;
  padding: 0;
  list-style-type: none;
}
.card .menu-content::before, .card .menu-content::after {
  content: '';
  display: table;
}
.card .menu-content::after {
  clear: both;
}
.card .menu-content li {
  display: inline-block;
}
.card .menu-content a {
  color: #fff;
}
.card .menu-content span {
  position: absolute;
  left: 50%;
  top: 0;
  font-size: 10px;
  font-weight: 700;
  font-family: 'Open Sans';
  -webkit-transform: translate(-50%, 0);
          transform: translate(-50%, 0);
}


.card .wrapper {
  background-color: #fff;
  min-height: 400px;
  min-width: 200px;
  position: relative;
  overflow: hidden;
  border-radius: 10px;
      box-shadow: 0px 0px 20px 0px #77afdf, inset 0px 0px 0px 0px #65a6de;
}

@media screen and (max-width: 300px){
.card .wrapper {
  background-color: #fff;
  min-height: 400px;
  width: 100%;

  

  

    box-shadow: 0px 0px 20px 0px #77afdf, inset 0px 0px 0px 0px #65a6de;
}
}

.card .wrapper {
  background-color: #fff;
  min-height: 400px;
  width: 100%;

  

    box-shadow: 0px 0px 20px 0px #77afdf, inset 0px 0px 0px 0px #65a6de;
}


.card .wrapper:hover .data {
  -webkit-transform: translateY(0);
          transform: translateY(0);

          background: -moz-linear-gradient(top, rgba(0,0,0,0.65) 0%, rgba(0,0,0,0) 100%); /* FF3.6-15 */
background: -webkit-linear-gradient(top, rgba(0,0,0,0.65) 0%,rgba(0,0,0,0) 100%); /* Chrome10-25,Safari5.1-6 */
background: linear-gradient(to bottom, rgba(0,0,0,0.65) 0%,rgba(0,0,0,0) 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#a6000000', endColorstr='#00000000',GradientType=0 ); /* IE6-9 */


}
.card .data {
  position: absolute;
  bottom: 0;
  width: 100%;
  -webkit-transform: translateY(calc(70px + 1em));
          transform: translateY(calc(70px + 1em));
  -webkit-transition: -webkit-transform 0.3s;
  transition: -webkit-transform 0.3s;
  transition: transform 0.3s;
  transition: transform 0.3s, -webkit-transform 0.3s;
}
.card .data .content {
  padding: 1em;
  position: relative;
  z-index: 1;
}
.card .author {
  font-size: 12px;
}
.card .title {
  margin-top: 10px;
}
.card .text {
  height: 70px;
  margin: 0;
}
.card input[type='checkbox'] {
  display: none;
}
.card input[type='checkbox']:checked + .menu-content {
  -webkit-transform: translateY(-60px);
          transform: translateY(-60px);
}

.example-1 .wrapper {
  background: url(http://res.cloudinary.com/thedailybeast/image/upload/v1492110403/articles/2016/08/12/whose-gold-medal-is-worth-the-most/160811-glasser-olympic-medal-tease_mvemzf.jpg) center/cover no-repeat;
}
.example-1 .date {
  position: absolute;
  top: 0;
  left: 0;
  background-color: #77d7b9;
  color: #fff;
  padding: 0.8em;
}
.example-1 .date span {
  display: block;
  text-align: center;
}
.example-1 .date .day {
  font-weight: 700;
  font-size: 24px;
  text-shadow: 2px 3px 2px rgba(0, 0, 0, 0.18);
}
.example-1 .date .month {
  text-transform: uppercase;
}
.example-1 .date .month,
.example-1 .date .year {
  font-size: 12px;
}
.example-1 .content {
  background-color: #fff;
  box-shadow: 0 5px 30px 10px rgba(0, 0, 0, 0.3);
}
.example-1 .title a {
  color: gray;
}
.example-1 .menu-button {
  position: absolute;
  z-index: 999;
  top: 16px;
  right: 16px;
  width: 25px;
  text-align: center;
  cursor: pointer;
}
.example-1 .menu-button span {
  width: 5px;
  height: 5px;
  background-color: gray;
  color: gray;
  position: relative;
  display: inline-block;
  border-radius: 50%;
}
.example-1 .menu-button span::after, .example-1 .menu-button span::before {
  content: '';
  display: block;
  width: 5px;
  height: 5px;
  background-color: currentColor;
  position: absolute;
  border-radius: 50%;
}
.example-1 .menu-button span::before {
  left: -10px;
}
.example-1 .menu-button span::after {
  right: -10px;
}
.example-1 .menu-content {
  text-align: center;
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  z-index: -1;
  -webkit-transition: -webkit-transform 0.3s;
  transition: -webkit-transform 0.3s;
  transition: transform 0.3s;
  transition: transform 0.3s, -webkit-transform 0.3s;
  -webkit-transform: translateY(0);
          transform: translateY(0);
}
.example-1 .menu-content li {
  width: 33.333333%;
  float: left;
  background-color: #77d7b9;
  height: 60px;
  position: relative;
}
.example-1 .menu-content a {
  position: absolute;
  top: 50%;
  left: 50%;
  -webkit-transform: translate(-50%, -50%);
          transform: translate(-50%, -50%);
  font-size: 24px;
}
.example-1 .menu-content span {
  top: -10px;
}

.example-2 .wrapper {
  /*background: url(https://tvseriescritic.files.wordpress.com/2016/10/stranger-things-bicycle-lights-children.jpg) center/cover no-repeat;*/
}
.example-2 .wrapper:hover .menu-content span {
  -webkit-transform: translate(-50%, -10px);
          transform: translate(-50%, -10px);
  opacity: 1;
}
.example-2 .header {
  color: #fff;
  padding: 1em;
}
.example-2 .header::before, .example-2 .header::after {
  content: '';
  display: table;
}
.example-2 .header::after {
  clear: both;
}
.example-2 .header .date {
  float: left;
  font-size: 12px;
}
.example-2 .menu-content {
  float: right;
}
.example-2 .menu-content li {
  margin: 0 5px;
  position: relative;
}
.example-2 .menu-content span {
  -webkit-transition: all 0.3s;
  transition: all 0.3s;
  opacity: 0;
}
.example-2 .data {
  color: #fff;
  -webkit-transform: translateY(calc(70px + 4em));
          transform: translateY(calc(70px + 4em));
}
.example-2 .title a {
  color: #fff;
}
.example-2 .button {
  display: block;
  width: 100px;
  margin: 2em auto 1em;
  text-align: center;
  font-size: 12px;
  color: #fff;
  line-height: 1;
  position: relative;
  font-weight: 700;
}
.example-2 .button::after {
  content: '\2192';
  opacity: 0;
  position: absolute;
  right: 0;
  top: 50%;
  -webkit-transform: translate(0, -50%);
          transform: translate(0, -50%);
  -webkit-transition: all 0.3s;
  transition: all 0.3s;
}
.example-2 .button:hover::after {
  -webkit-transform: translate(5px, -50%);
          transform: translate(5px, -50%);
  opacity: 1;
}

</style>

@endsection