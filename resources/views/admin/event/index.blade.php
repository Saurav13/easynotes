@extends ('admin.admin')


@section('body')
<div class="container-fluid">
  <h3>Events      <button name="Add Event" id="eventadd" value="Add Categrory" class="btn btn-md btn-primary" data-toggle="modal" data-target="#addEvent"><i class="fa fa-plus" aria-hidden="true"></i> Add Event</button>
</h3>
<br>
   <div class="card">
      <div cass="container-fluid">
        <div class="content table-responsive table-full-width">
      <table class="table table-hover table-striped" id="table1">
        <thead>
          <th>ID</th>
          <th>Title</th>
          <th>Description</th>
          <th>Event Date</th>
          <th>Event Time</th>
          <th>Registered Date</th>
          <th>Visitors</th>
          <th>Image</th>
          <th>Actions</th>
        </thead>
        <tbody>


          @foreach($events as $event)
          <tr>
            <th>{{$event->id}}</th>
            <td id="title{{$event->id}}">{{$event->title}}</td>
            <td>{{substr(strip_tags($event->description),0,50)}}{{strlen($event->description)>50 ? "...":"" /* checking if the string is more than 50 or not and adding ... if the body has more than 50 characters*/}}</td> 
            <td id="date{{$event->id}}">{{date('M j,Y',strtotime($event->event_date))}}</td>
            <td id="time{{$event->id}}">{{$event->event_time}}</td>
            <td>{{date('M j,Y',strtotime($event->created_at))}}</td>
            <td>{{Counter::show('events',$event->id)}}</td>
              
            <td><img src="{{asset('uploads/'. $event->image)}}" height="50" width="50" style="border: 2px solid; border-radius: 10px;"></td>
            <td><a class="btn btn-default btn-sm"  data-toggle="modal" data-target="#showEvent" cid="{{$event->id}}"  id="view{{$event->id}}">View</a> 
              <a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#editEvent" cid={{$event->id}} id="update{{$event->id}}">Edit</a>
                <a  cid="{{$event->id}}"  class="btn btn-danger btn-sm" value="Delete" id="delete{{$event->id}}">Delete
                </a>
              
              <input type="hidden" id="description{{$event->id}}" value="{{$event->description}}" />
              <input type="hidden" id="picture{{$event->id}}" value="{{$event->image}}" />
            </tr>

            @endforeach
          </tbody>

        </table>
      </div>
    </div>
  </div>
       
  </div>
  <script>
   $('#addtitle').keyup(function(){
              var text=$('#addtitle').val();

              var alias=text.split(' ').join('-').toLowerCase();
              // console.log(text,alias);
              $('#addalias').val(alias);
            });
    $('#edittitle').keyup(function(){
              var text=$('#edittitle').val();

              var alias=text.split(' ').join('-').toLowerCase();
              // console.log(text,alias);
              $('#editalias').val(alias);
            });
  </script>

  @stop


  <div id="addEvent" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add an Event</h4>
        </div>
        <div class="modal-body">
          <form action="{{route('events.store')}}" enctype="multipart/form-data" method="POST">
            {{csrf_field()}}
            <label for="title">Event Title:</label>
            <input type="text" id="addtitle" name="title" class="form-control">

            <label for="alias">Alias</label>
            <input type="text" required name="alias" id="addalias" class="form-control"/>

            {{Form::label('description','Event Description:')}}
            {{Form::textarea('description',null/*'Enter the description' yo garyo bhane chain placeholder jastai kam garcha*/,array('class'=>'form-control','id'=>'addeditor'))}}
              <input type="file" id="upload" hidden/>
            <label for="event_date">Event Date:</label>
            <input type="date" name="event_date" class="form-control">

            <label for="event_time">Event Time:</label>
            <input type="time" name="event_time" class="form-control">

            <label for="image">Event Image:</label>
            <input type="file" name="image">
             <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
            <script>
              tinymce.init({
            selector: "textarea#addeditor",
            
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern"
            ],
            toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
            toolbar2: "print preview | forecolor backcolor emoticons | template",
            image_advtab: true,
            file_picker_callback: function(callback, value, meta) {
            if (meta.filetype == 'image') {
                $('#upload').trigger('click');
                $('#upload').on('change', function() {
                var file = this.files[0];
                var reader = new FileReader();
                reader.onload = function(e) {
                    callback(e.target.result, {
                    alt: ''
                    });
                };
                reader.readAsDataURL(file);
                });
            }
            },
            templates: [
          {title: 'Newsletter1', description: 'Notice', url: "/templates/newsletter.html"}
        ]
        });
              </script>
             </div>
          <div class="modal-footer">
            <input type="submit" name="submit" value="Add Event" class="btn btn-success">

          </div>
        </form>
      </div>

    </div>
  </div>

  {{-- Edit Modal --}}
  <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
  <script>
  tinymce.init({
            selector: "textarea#editdescription",
            
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern"
            ],
            toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
            toolbar2: "print preview | forecolor backcolor emoticons | template",
            image_advtab: true,
            file_picker_callback: function(callback, value, meta) {
            if (meta.filetype == 'image') {
                $('#upload1').trigger('click');
                $('#upload1').on('change', function() {
                var file = this.files[0];
                var reader = new FileReader();
                reader.onload = function(e) {
                    callback(e.target.result, {
                    alt: ''
                    });
                };
                reader.readAsDataURL(file);
                });
            }
            },
            templates: [
          {title: 'Newsletter1', description: 'Notice', url: "/templates/newsletter.html"}
        ]
        });
   $(document).ready(function(){
        
      $('[id *= "update"]').click(function(){

        var id= $(this).attr('cid');
        console.log(id);
        var title = $('#title'+id).html();
        var desc = $('#description'+id).val();
        var date = $('#date'+id).html();
        var time = $('#time'+id).html();
        // $('#editdescription').empty();
        $('#edittitle').val(title);
        var editor=tinymce.get('editdescription');
        // var content=editor.getContent();
        
        // content=content.replace(desc);
        // console.log(content);
        editor.setContent(desc);
        // CKEDITOR.instances.editdescription.setData('');
        // CKEDITOR.instances.editdescription.setData(desc);
        // $('#editdescription').html(desc);
        $('#editdate').val(date);
        $('#edittime').val(time);
        $('#eventid').val(id);
      });
    });
  </script>
  <script>
    $(document).ready(function(){
      $('[id *= "view"]').click(function(){

        var id= $(this).attr('cid');
        var title = $('#title'+id).html();
        var description = $('#description'+id).val();
        var date = $('#date'+id).html();
        var time = $('#time'+id).html();
        var picture = $('#picture'+id).val();
        console.log(picture);
        $('#showtitle').html(title);
        console.log(title);
        $('#editor2').html(description);
        $('#showdate').html(date);
        $('#showpicture').attr('src',"../../uploads/"+picture);

        $('#showtime').html("Time:"+time);
      });
    });
  </script>
    <script>
  $(document).ready(function(){
 

    $('[id *= "delete"]').click(function(event){

      var id= $(this).attr('cid');

      swal({
        title: "Are you sure?",
        text: "You will not be able to recover this file!",
        type: "warning",

        showCancelButton: true,

      }).then(function(){
        $.post("{{route('events.delete')}}",{id:id,_token:"{{csrf_token()}}"},function(data){
          swal({
            title:"Deleted Successfully",
            type:"success"

          }).then(function(){
            window.location.reload();
          })
        })
      });
    });
  });
</script>
  <div id="editEvent" class="modal fade" role="dialog">
    <div class="modal-dialog">


      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit Event</h4>
        </div>
        <div class="modal-body">
        {{-- {!!Form::model(['route'=>['events.edit'],'method'=>"POST",'files'=>'true']) !!} --}}
          <form action="{{route('events.edit')}}" enctype="multipart/form-data" method="POST">
            {{csrf_field()}}
            <label for="title">Event Title:</label>
            <input type="text" name="title" class="form-control" id="edittitle">
           
            <label for="editalias">Alias</label>
            <input type="text" required name="alias" id="editalias" class="form-control"/>

            {{-- <label for="description">Event Description:</label>
            <input type="textarea" name="description" id="editdescription" class="form-control" placeholder="Enter description"> --}}
             {{Form::label('description','Event Description:')}}
            {{Form::textarea('description',null,array('class'=>'form-control','id'=>'editdescription'))}} 
            <input type="file" id="upload1" hidden/>
            <label for="event_date">Event Date:</label>
            <input type="date" name="event_date" class="form-control" id="editdate">

            <label for="event_time">Event Time:</label>
            <input type="time" name="event_time" class="form-control" id="edittime">

            <label for="image">Event Image:</label>

            <input type="file" name="image">
            <script src="{{asset('/assets/ckeditor/ckeditor.js')}}"></script>
          
          </div>
          <div class="modal-footer">
            <input type="submit" name="submit" value="Update Event" class="btn btn-success">
            <input type="hidden" id="eventid" name="id"/>

          </div>
          {{-- {!! Form::close() !!} --}}
        </form>
      </div>

    </div>
  </div>
</div>

{{-- Show Event --}}
<div id="showEvent" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title pull-left" id="showtitle"></h4>
        <h4 class="modal-title pull-right" id="showdate"></h4>
      </div>
      <br>
      <div  align="center">
        <img src="" align="center" id="showpicture" style="height: 200px; width: 400px;" />
      </div>
      <br>
      <div class="modal-body">
        <p id="editor2"></p>
      </div>
      <p id="showtime" align="center"></p>
      <div class="modal-footer">
        <form action="#" method="post">
          {{csrf_field()}}
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

        </form>
      </div>


    </div>

  </div>
</div>




