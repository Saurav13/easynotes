
@extends('admin.admin')



@section('body')

  <div class="container-fluid">

    <h3>Notes  <button name="addnew" id="btnt" value="Add Categrory" class="btn btn-sm btn-primary" ><i class="fa fa-plus" aria-hidden="true"></i> Add Note</button>
       
       
      <div class="form-group pull-right">
        <form method="GET" action="{{ route('notes.filter') }}">
          <select id="filter" name="subject" class="form-control" placeholder="Filter Notes" onchange= "this.form.submit()">
            <option disabled selected value> -- select a subject to filter-- </option>
            @foreach($subjects as $subject)
              <option value="{{$subject->subject_id}}">{{ $subject->subject_name }}</option>
            @endforeach
          </select>
        </form>
      </div>
    </h3> 
      
    <div class="card">
      <div cass="container-fluid">
        <div class="content table-responsive table-full-width">
                            
          <table class="table table-hover table-striped">
            <thead>
            <tr>
              <th>S.N</th>
              <th>Title</th>
              <th>Category</th>
              <th>Faculty</th>
              <th>Semester</th>
              <th>Subject</th>
              <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($notes as $note)
            <tr>
              <td>{{$loop->iteration}}</td>
              <td id="title{{$note->id}}">{{$note->title}}</td>
              <td id="category{{$note->id}}">{{$note->category_name}}</td>
              <td id="faculty{{$note->id}}">{{$note->faculty_name}}</td>
              <td id="semester{{$note->id}}">{{$note->semester}}</td>
              <td id="subject_id{{$note->id}}" sub-id="{{$note->subject->subject_id}}">{{$note->subject->subject_name}}</td>

              <td id="file{{$note->id}}" hidden>{{$note->file}}</td>
            
              <td>
                <a href="{{ asset('notes/'.$note->file)}}" target="_blank" class="btn btn-success">View note</a>
                <input type="button" data-id="{{$note->id}}" class="btn btn-md btn-primary" value="Edit" id="update{{$note->id}}" data-toggle="modal" data-target="#editmodal">
                <input type="button" cid="{{$note->id}}" class="btn btn-md btn-danger" value="Delete" id="delete{{$note->id}}"/>

              </td>
            </tr>
            @endforeach
            </tbody>
          </table>
        </div>
    </div>
    </div>
    <div class="text-center">{!! $notes->appends($_GET)->links() !!} </div>
  </div>

@stop
  
<div class="modal" id="editmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Note</h4>
      </div>
      <div class="modal-body">
        <form action="{{route('notes.update')}}" method="POST" enctype="multipart/form-data">
          {{ csrf_field() }}
          <input id="nId" name="id" hidden/>
          <label for="categoryedit">Category:</label>
          <select name="categoryedit" class="form-control" id='categoryedit_id' required>
              <option selected disabled>Choose here</option>
            @foreach($categories as $category)
              <option value="{{ $category->category_id }}">{{ $category->category_name }}</option>
          @endforeach
          </select>
          <br>
          <label for="facultyedit">Faculty:</label>
          <select name="facultyedit" class="form-control" id='facultyedit_id' required>
          <option selected disabled>Choose here</option>
            
          </select><br>
          <label>Semester: *</label><br>
          <select name="semesteredit" class="form-control" id="semesteredit_id">
            <option selected disabled>Choose here</option>
            <option value="1st">1st</option>
            <option value="2nd">2nd</option>
            <option value="3rd">3rd</option>
            <option value="4th">4th</option>
            <option value="5th">5th</option>
            <option value="6th">6th</option>
            <option value="7th">7th</option>
            <option value="8th">8th</option>
           
          </select><br>
          <label for="subjectedit">Subject:</label>
          <select name="subjectedit" class="form-control" id='subjectedit_id' required>
          <option selected disabled>Choose here</option>
            
          </select><br>

          <label for="titleedit">Title:</label>
          <input type="text" name="titleedit" class="form-control" id="titleedit_id" required><br>
          
          <label for="file">File: *</label>
             <input type="file" placeholder="Upload pdf" name="file" accept="application/pdf" required/>
          <br> 
          <button class="btn btn-md btn-success" id="savenote">Submit Details</button>
        </form>
      </div>
    </div>
  </div>
</div>


<div class="modal" id="modal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="closemodal2"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel1">Add Note</h4>
      </div>
      <div class="modal-body">
        <form action="{{ route('notes.store') }}" method="POST" enctype="multipart/form-data" id='upload'>

          {{ csrf_field() }}
          <label for="subject_id">Category: *</label>
          <select name="category_id" class="form-control" id='category_id' required>
            <option selected disabled>Choose here</option>
            @foreach($categories as $category)
                <option value="{{ $category->category_id }}">{{ $category->category_name }}</option>
            @endforeach
          </select><br>
          <label for="faculty_id">Faculty: *</label>
          <select name="faculty_id" class="form-control" id='faculty_id' required>
          <option selected disabled>Choose here</option>
          </select><br>
          <label>Semester: *</label><br>
          <select name="semester" class="form-control" id="semester">
            <option selected disabled>Choose here</option>
            <option value="1st">1st</option>
            <option value="2nd">2nd</option>
            <option value="3rd">3rd</option>
            <option value="4th">4th</option>
            <option value="5th">5th</option>
            <option value="6th">6th</option>
            <option value="7th">7th</option>
            <option value="8th">8th</option>
           
          </select>
          <label for="subject_id">Subject: *</label>
          <select name="subject_id" class="form-control" id='subject_id' required>
          <option selected disabled>Choose here</option>
            
          </select><br>

          <label for="title">Title: *</label>
          <input type="text" name="title" class="form-control" required><br>

          <label for="file">File: *</label>
          <input type="file" placeholder="Upload pdf" name="file" accept="application/pdf" required/>
          <br> 
          <button class="btn btn-md btn-success" id="savenote">Submit Details</button>
        </form>
      </div>
    </div>
  </div>
</div>

<script src="https://code.jquery.com/jquery-3.2.1.min.js" ></script>

<script type="text/javascript">
  $(document).ready(function(){
    // console.log($('#td').html())
    $('[id *= "delete"]').click(function(event){
      
      var id= $(this).attr('cid');
      
      swal({
        title: "Are you sure?",
        text: "You will not be able to recover this file!",
        type: "warning",
        
        showCancelButton: true,
        
      }).then(function(){
        $.post("{{route('notes.delete')}}",{id:id,_token:"{{csrf_token()}}"},function(data){
          swal({
            title:"Deleted Successfully",
            type:"success"

          }).then(function(){
            window.location.reload();
          });
        });
      });
    });
    
    $('#btnt').on('click',function(){
      $('#modal2').modal('toggle');
    });

    $('[id *= "update"]').click(function(){
      var id=$(this).attr('data-id');
      var title = $('#title'+id).html();
      var faculty = $('#faculty'+id).html();
      var semester = $('#semester'+id).html();
      var category = $('#category'+id).html();
      var subject = $('#subject_id'+id).html();
      console.log(category,title,faculty,semester,subject);

      var file= $('#file'+id).html();

      var subject_id= $('#subject_id'+id).attr('sub-id');


      // console.log(title,subject_id,semester,category,faculty);
      $('#nId').val(id);
      $('#titleedit_id').val(title);
      $('#categoryedit_id').append('<option value="'+category+'" name="'+category+'" >'+categroy+'</option>');
      $('#facultyedit_id').val(faculty);
      $('select#subjectedit_id').val(subject_id);
      $('select#semester_idEdit').val(semester);
      
      
  });
    $('#category_id').on('change',function(e){
      var cat_id=e.target.value;

          $.get('notesfaculty/'+cat_id,function(data){
            console.log(cat_id);
            $('#faculty_id').empty();
            $('#subject_id').empty();

            $.each(data.faculties, function(index, faculties){
              console.log(faculties);
              $('#faculty_id').append('<option value="'+faculties.faculty_id+'" name="'+faculties.faculty_name+'" >'+faculties.faculty_name+'</option>')
              
              //bake cha for faculty heading
            });
      });
    });
    $('#semester').change(function(){
        var semester=$(this).val();
        var faculty=$('#faculty_id').val();
        $.get("{{URL::to('admin/notesfaculty/getsub')}}",{semester:semester,faculty:faculty},function(data,subjects)
        {
          $('#subject_id').empty();
          $.each(data.subjects, function(index, subjects){
            $('#subject_id').append('<option value="'+subjects.subject_id+'" name="'+subjects.subject_name+'" >'+subjects.subject_name+'</option>')
            
            //bake cha for faculty heading
          });
          

        });
        
    });

    // edit

    $('#categoryedit_id').on('change',function(e){
      var cat_id=e.target.value;

          $.get('notesfaculty/'+cat_id,function(data){
            console.log(cat_id);
            $('#facultyedit_id').empty();
            $('#subjectedit_id').empty();

            $.each(data.faculties, function(index, faculties){
              console.log(faculties);
              $('#facultyedit_id').append('<option value="'+faculties.faculty_id+'" name="'+faculties.faculty_name+'" >'+faculties.faculty_name+'</option>')
              
              //bake cha for faculty heading
            });
      });
    });
    $('#semesteredit_id').change(function(){
        var semester=$(this).val();
        var faculty=$('#facultyedit_id').val();
        $.get("{{URL::to('admin/notesfaculty/getsub')}}",{semester:semester,faculty:faculty},function(data,subjects)
        {
          $('#subjectedit_id').empty();
          $.each(data.subjects, function(index, subjects){
            $('#subjectedit_id').append('<option value="'+subjects.subject_id+'" name="'+subjects.subject_name+'" >'+subjects.subject_name+'</option>')
            
            //bake cha for faculty heading
          });
          

        });
        
    });
  });




    

    
</script>
