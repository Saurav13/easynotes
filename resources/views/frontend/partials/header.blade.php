<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>EZnotes</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="" />
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<meta name="author" content="http://bootstraptaste.com" />
<!-- css -->
<link href="{{asset('newfrontend/css/bootstrap.min.css')}}" rel="stylesheet" />
<link href="{{asset('newfrontend/css/fancybox/jquery.fancybox.css')}}" rel="stylesheet">
<link href="{{asset('newfrontend/css/jcarousel.css')}}" rel="stylesheet" />
<link href="{{asset('newfrontend/css/flexslider.css')}}" rel="stylesheet" />
<link href="{{asset('newfrontend/css/style.css')}}" rel="stylesheet" />

<!-- Theme skin -->
<link href="{{asset('newfrontend/skins/default.css')}}" rel="stylesheet" />
<link href="https://fonts.googleapis.com/css?family=Cairo" rel="stylesheet">
  <link href="{{asset('/assets/css/sweetalert2.css')}}" rel="stylesheet" />

<script src="{{asset('/assets/js/sweetalert2.min.js')}}"></script>


<script src="{{asset('/assets/ckeditor/ckeditor.js')}}"></script>

<style type="text/css">
	h1,h3,h3,h4,post-heading{
	font-weight: 400;

	
}

h1,h2,h3,h4,h5,h6,a,span,dt,dd,p,.panel-heading{
	font-family: 'Cairo', sans-serif;
}

article .post-heading h3 a {
    font-weight: 500;
    color: #353535;
}

.widget h4, .widget h5 {
    font-weight: 500;
    margin-bottom: 20px;
}
</style>
 
<style type="text/css">
  #load{
    width:100%;
    height:100%;
    position:fixed;
    z-index:9999;
    background:url("{{asset('img/pageloading.gif')}}") no-repeat center center rgba(0,0,0,0.6)
}
	
</style>


</head>


