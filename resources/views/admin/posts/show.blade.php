@extends('admin.admin')



@section('body')

<div class="container-fluid">
	<div class="card">
		<div class="container-fluid">
	<div class="row">
		<div class="col-md-8">
			<h2>{{ $post->title }}</h2>
			@if($post->image!=null)
			<img src="{{asset('uploads/'. $post->image)}}" height="200" width="400">
			@endif

			<p class="lead" >{!!$post->description!!}</p>
			<hr>



		</div>
		<br>

		<div class="col-md-2" URL Slug:ass="well">


			<dl class="dl-horizontal">
				<label>Created At:</label>
				<p>{{date('M j,Y h:ia',strtotime($post->created_at))}}</p>	
			</dl>
			<dl class="dl-horizontal">
				<label>Last Updated:</label>
				<p>{{date('M j,Y h:ia',strtotime($post->updated_at))}}</p>	
			</dl>	
			<hr>
			<div class="row">
				<div class="col-sm-6">
					{!! Html::linkRoute('post.edit','Edit',array($post->id), array('class'=>"btn btn-primary btn-block")) !!}


				</div>
				<div class="col-sm-6">
					{!! Form::open(['route'=>['post.destroy', $post->id],'method'=>"DELETE"]) !!}


					{!! Form::submit('Delete', ['class'=>'btn btn-danger btn-block']) !!}

					{!! Form::close()!!}
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-sm-12">
					{{ Html::linkRoute('post.index','See All posts',array(),['class'=>'btn btn-default btn-block btn-h1-spacing'])}}
				</div>

			</div>

		</div>
</div>
</div>
	</div>
</div>

@endsection