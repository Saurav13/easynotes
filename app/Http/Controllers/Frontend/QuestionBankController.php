<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Questionbank;
use App\Faculty;
use App\Category;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Counter;

class QuestionBankController extends Controller
{
   
    public function questionbank()
    {
        Counter::showAndCount('qb');

    	$qb=Questionbank::all();
    	$faculties = Faculty::all();
        $categories = Category::all();
    	return view ('frontend.questionbank.questionbank')->with('qb',$qb)->with('faculties',$faculties)->with('categories',$categories);
    }
    public function getFacultyAndQuestions(Request $request)
    {
        $cat_id = Input::get('cat_id');
        $faculties=Faculty::where('category_id','=',$cat_id)->get();
        $first=$faculties->first()->faculty_id;
        $questions=Questionbank::where('faculty_id','=',$first)->get();
        return response()->json([
            'faculties'=>$faculties,
            'facquestions'=>$questions
        ]);
        
    }

    public function getquestions(Request $request)
    {
        $faculty_id = Input::get('faculty_id');
        $questions=Questionbank::where('faculty_id','=',$faculty_id)->get();
        return response()->json([
            'questions'=>$questions
        ]);
    }


}
