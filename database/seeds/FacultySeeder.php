<?php

use Illuminate\Database\Seeder;

class FacultySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('faculties')->insert([
            'faculty_name' => "BIM",
            'category_id' => '4',
            
        ]);
        DB::table('faculties')->insert([
            'faculty_name' => "BBA",
            'category_id' => '4',
            
        ]);
        DB::table('faculties')->insert([
            'faculty_name' => "BHM",
            'category_id' => '4',
            
        ]);
        DB::table('faculties')->insert([
            'faculty_name' => "Bsc.Csit",
            'category_id' => '4',
            
        ]);
        DB::table('faculties')->insert([
            'faculty_name' => "MBA",
            'category_id' => '4',
            
        ]);
        DB::table('faculties')->insert([
            'faculty_name' => "BBS",
            'category_id' => '4',
            
        ]);

        DB::table('faculties')->insert([
            'faculty_name' => "BIT",
            'category_id' => '3',
            
        ]);
        DB::table('faculties')->insert([
            'faculty_name' => "BBA",
            'category_id' => '3',
            
        ]);
        DB::table('faculties')->insert([
            'faculty_name' => "MBA",
            'category_id' => '2',
            
        ]);
        DB::table('faculties')->insert([
            'faculty_name' => "BBA",
            'category_id' => '2',
            
        ]);
        DB::table('faculties')->insert([
            'faculty_name' => "BBA",
            'category_id' => '1',
            
        ]);
        DB::table('faculties')->insert([
            'faculty_name' => "BBIS",
            'category_id' => '1',
            
        ]);
    }

}
