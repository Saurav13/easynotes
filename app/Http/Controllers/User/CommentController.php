<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Comment;
use App\Thread;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    
    public function addThreadComment(Request $request, Thread $thread)
    {
        $this->validate($request,[
            'body'=>'required'
            ]);
        $comment=new Comment();
        $comment->body=$request->body;
        $comment->user_id=auth()->user()->id;

        $thread->comments()->save($comment);
        return back();
    }

   
   public function addReplyComment(Request $request, Comment $comment)
   {
       $this->validate($request,[
           'body'=>'required'
           ]);
       $reply=new Comment();
       $reply->body=$request->body;
       $reply->user_id=auth()->user()->id;

       $comment->comments()->save($reply);
       return back();
   }
   
    public function update(Request $request, Comment $comment)
    {
       
         $this->validate($request,[
            'body'=>'required'
            ]);

         $comment->update($request->all());
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comment)
    {
        $comment->delete();
        return back();
    }
}
