@extends('frontend.main')

@section('body')

      <div class="container">
        <div class="col-md-3 pull-right">
          <form action="{{route('searchforusers.directory')}}" method="POST">
          	{{csrf_field()}}
            <div class="form-group">
              <h5>Search Users</h5>
              <input type="text" value="" class="form-control" id="usr" name="search">
            </div>
            <button class="btn btn-theme btn-block">Search</button>
          </form>
        </div>
          <hr>     
         
          <div class="" style="background-color: transparent;">
          <h5>Directories</h5>
          <div class="row">
            <div class="col-md-4">
              @if($folders == null)
              <div class="container">
                <div class="row">
                  <div class="col-md-4">
                    <h1>This user has not uploaded any files yet</h1>
                  </div>
                </div>
              </div>
              @else
               <div class="force-overflow">
          @foreach($folders as $f)
          <div class="" id="follow{{$f[0]}}">
            
            <li>{{$f[0]}} 
            @if($f[1]==false)
              <button class="btn btn-md btn-default pull-right" cid="{{$f[0]}}" id="addToDrive{{$f[0]}}">Add to drive</button>
            @else
             <button class="btn btn-md btn-default pull-right" cid="{{$f[0]}}" id="addToDrive{{$f[0]}}">Followed</button>
            @endif
            </li>
          </div>
            
            <input type="hidden" id="user_id" name="user_id" value="{{$user_id}}">
            <input type="hidden" id="currentuser_id" name="currentuser_id" value="{{Auth::User()->id}}">
          @endforeach
             <br>
              </div>
              @endif

        </div>
    
  </div>
</div>
</div>
<script
    src="https://code.jquery.com/jquery-3.2.1.min.js"
    integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
    crossorigin="anonymous"></script>
    <script type="text/javascript">
      
    $(document).ready(function(){
        
          $('[id *= "addToDrive"]').click(function(){
            var folder_name= $(this).attr('cid');
            var user_id= $('#user_id').val();
            var currentuser_id=$('#currentuser_id').val();
              if($(this).html()==="Add to drive")
              {
                $(this).html("Followed");
              
            }
            else
            { 
              $(this).html("Add to drive");
                  
            }
            console.log($(this).html());
            console.log(folder_name,user_id,currentuser_id);
            $.post("{{route('drive.follow')}}",{folder_name:folder_name,user_id:user_id,currentuser_id:currentuser_id,_token:"{{csrf_token()}}"},function(data){
              
          });
    });
  });
    </script>


@endsection