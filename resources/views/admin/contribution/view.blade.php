@extends ('admin.admin')


@section('body')
<div class="container-fluid">
	<div class="card">
		<div class="container-fluid">
			<label for="name">Name</label>
			<text class="form-control" id="name">{{$contribution->name}}</text>
			<br>
			<label for="description">Description</label>
			
			<textarea class="form-control">{{$contribution->description}}</textarea>
			<br>
			<label for="type">Type</label>
			<text class="form-control">{{$contribution->type}}</text>
			<br>
			@if($contribution->files()->count()>0)
			<label for="files">Files: </label>
			              <table class="table table-hover table-striped" id="files">
				<thead>
				<tr>
					<th>File</th>
				</tr>
				</thead>
				<tbody>
				@foreach($contribution->files as $f)
				<tr>
				<td ><a href="asset('contributions'.'/'.$f->file)" download>{{$f->file}}</a></td>
					
				
				</tr>
				@endforeach
				</tbody>
			</table>
			@else
			no files available..
			@endif
			<br>
			<br>
		</div>
	</div>
</div>

@stop