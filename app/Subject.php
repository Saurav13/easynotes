<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
	protected $tables = 'subjects';
    protected $primaryKey= 'subject_id';

    public function notes(){
        return $this->hasMany('App\Note','subject_id','subject_id');
    }

}
