@extends('frontend.main')

@section('body')

      <div class="container">
        <div class="col-md-3 pull-right">
          <form action="{{route('searchforusers.directory')}}" method="POST">
          	{{csrf_field()}}
            <div class="form-group">
              <h5>Search Users</h5>
              <input type="text" value="{{$search}}" class="form-control" id="usr" name="search">
            </div>
            <button class="btn btn-theme btn-block">Search</button>
          </form>
        </div>
          <hr>     
          @if($search == null)
          
            <h4>No results for empty search</h4>
          
          @endif
          @if(count($users) == 0)
          
            <h4>User not found</h4>
          
          @elseif($search != null)
          <div class="" style="background-color: transparent;">
          <h5>Found members</h5>
          <div class="row">
            @foreach($users as $u)
            <div class="col-md-2">
               <div class="force-overflow">
            <a href="{{route('getusers.directory',$u->id)}}">              
              <div class="card text-center" style="width: 10rem;">
                <img class="card-img-top" src="{{asset('profile-images'.'/'.$u->picture)}}" alt="Card image cap" style="height: 150px; width: 150px; border-radius: 50%;">
                <div class="card-block">
                  <br>
                <p class="card-text">{{$u->name}}</p>
                </div>
             </div>
            </a>
             <br>
              </div>

        </div>
          @endforeach
    
  </div>
</div>
@endif
</div>



@endsection