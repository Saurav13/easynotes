@extends ('admin.admin')


@section('body')
<div class="container-fluid">
  <h3>Images     <button name="Add Event" id="eventadd" value="Add Categrory" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#addEvent"><i class="fa fa-plus" aria-hidden="true"></i> Add Image</button>
</h3>
  
  <br>
   <div class="card">
      <div cass="container-fluid">
        <div class="content table-responsive table-full-width">
      <table class="table" id="table1">
        <thead>
          <th>ID</th>
          <th>Image</th>
          <th>Slider Title</th>
          <th>Slider Description</th>
          <th>Slider Link</th>
          <th>Actions</th>
        </thead>
        <tbody>


          @foreach($sliderimage as $s)
          <tr>
            <td>{{$s->id}}</td>
            <td><img src="{{asset('systemimages/'. $s->file)}}" height="20" width="45" style="border: 0px solid; border-radius: 10px;"></td>
            <td>{{$s->title}}</td>
            <td>{{$s->description}}</td>
            <td>{{$s->link}}<a  cid="{{$s->id}}"  class="btn btn-danger btn-sm pull-right" value="Delete" id="delete{{$s->id}}">Delete
                </a></td>
                
              <input type="hidden" id="picture{{$s->id}}" value="{{$s->image}}" />
            </tr>

            @endforeach
          </tbody>

        </table>

        </div>
      </div>

    </div>
  </div>

  @stop


  <div id="addEvent" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Image</h4>
        </div>
        <div class="modal-body">
          <form action="{{route('insert.sliderimage')}}" enctype="multipart/form-data" method="POST">
            {{csrf_field()}}

        
            <label for="image">Slider Image:</label>
            <input type="file" name="file">
            <label for="title">Slider Title:</label>
            <input type="text" name="title" class="form-control">
            <label for="description">Slider Description:</label>
            <input type="text" name="description" class="form-control">
            <label for="link">Attach Link:</label>
            <input type="text" name="link" class="form-control">
          </div>
          <div class="modal-footer">
            <input type="submit" name="submit" value="Add Slider" class="btn btn-success">

          </div>
        </form>
      </div>

    </div>
  </div>

  
  <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <script>
  $(document).ready(function(){

    $('[id *= "delete"]').click(function(event){

      var id= $(this).attr('cid');

      swal({
        title: "Are you sure?",
        text: "You will not be able to recover this file!",
        type: "warning",

        showCancelButton: true,

      }).then(function(){
        $.post("{{route('sliderimage.delete')}}",{id:id,_token:"{{csrf_token()}}"},function(data){
          swal({
            title:"Deleted Successfully",
            type:"success"

          }).then(function(){
            window.location.reload();
          })
        })
      });
    });
  });
</script>






