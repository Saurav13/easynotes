<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Follow;
use Auth;
use ZipArchive;
use RecursiveIteratorIterator;
use RecursiveDirectoryIterator;
use Response;

class FollowDirController extends Controller
{
	private $root= 'D:/easynotes/dirs';
	public function __construct()
    {
 		 $this->middleware('auth');
    }
    public function index(Request $request)
    {
    	return view('frontend.profile.notes')->with('id',$request->id)->with('name',$request->name)->with('dir',$request->dir);
    }
    public function root(Request $request)
 	{  	
 		$this->root=$this->root.'/'.$request->id.'/'.$request->dir;
		
 		$dir=$this->root;
		$buff =scandir($dir);
		$files=array();
		$folders=array();
		foreach ($buff as $b) {
			if($b!='.' and $b!='..'){
				if(strpos($b,'.')!=false)
					array_push($files, $b);
				else
					array_push($folders,$b);
			}
		}
		// dd($files);
		$path=$this->root;
		return response()->json([
    		'files' => $files,
    		'folders' => $folders,
			'path'=>$path
			]);
		// return view('dirs.root',compact('files','folders','path'));
 	}
 	public function downloadFolder(Request $request)
 	{
 		$id=explode('/', $request->path)[4];
		$fids=Follow::where('user_id',Auth::User()->id)->get()->pluck('followed_id')->toArray();
		// dd($id);
		if(in_array($id, $fids))
		{

			$dir = $request->path.'/'.$request->name;
			$zip_file = $request->name.'.zip';

	        $rootPath = realpath($dir);

			// Initialize archive object
			$zip = new ZipArchive();
			$zip->open($request->path.'/'.$zip_file, ZipArchive::CREATE | ZipArchive::OVERWRITE);

			// Create recursive directory iterator
			/** @var SplFileInfo[] $files */
			$files = new RecursiveIteratorIterator(
			    new RecursiveDirectoryIterator($rootPath),
			    RecursiveIteratorIterator::LEAVES_ONLY
			);
			$c=0;
			foreach ($files as $name => $file)
			{
			    // Skip directories (they would be added automatically)
			    if (!$file->isDir())
			    {
			        // Get real and relative path for current file
			        $filePath = $file->getRealPath();
			        $relativePath = substr($filePath, strlen($rootPath) + 1);

			        // Add current file to archive
			        $zip->addFile($filePath, $relativePath);
			        $c=$c+1;
			    }
			}
			if($c==0)
				return "No files exists within this folder";
			// Zip archive will be created only after closing object
			$zip->close();
		    // dd($zip);
		    return Response::download($request->path.'/'.$zip_file)->deleteFileAfterSend(true);;
		}
		else
		{
			return "Not Accessible";
		}
 	}

 	public function downloadFile(Request $request)
 	{
 		$id=explode('/', $request->path)[4];
		$fids=Follow::where('user_id',Auth::User()->id)->get()->pluck('followed_id')->toArray();
		// dd($id);
		if(in_array($id, $fids))
		{
 			return Response::download($request->path.'/'.$request->name);
 		}
 		else
 		{
 			return "Not Accessible";
 		}
 	}
 		public function openDir(Request $request)
 	{
 		$id=explode('/', $request->path)[4];
		$fids=Follow::where('user_id',Auth::User()->id)->get()->pluck('followed_id')->toArray();
		// dd($request->path);
		if(in_array($id, $fids))
		{

	 		$dir=$request->path;
			$buff =scandir($dir);
			$files=array();
			$folders=array();
			foreach ($buff as $b) {
				if($b!='.' and $b!='..'){
					if(strpos($b,'.')!=false)
						array_push($files, $b);
					else
						array_push($folders,$b);
				}
			}
			// dd($folders);
			$path=$dir;
			return response()->json([
	    		'files' => $files,
	    		'folders' => $folders,
				'path'=> $path
				]);
		}
		else
		{
			return "Not Accessible";
		}
 	}
 	private $count=0;
 	public function makeTree($dir)
 	{
 		$array=array();
 		$buff=scandir($dir);	
 		foreach ($buff as $b) {
			if($b!='.' and $b!='..'){
				if(is_dir($dir.'/'.$b))
				{
					$arr=array("dirs"=>$this->makeTree($dir.'/'.$b),"id"=>"fo".$this->count,"name"=>$b,"path"=>$dir.'/'.$b);
					$this->count=$this->count+1;
					array_push($array, $arr);
				}
				else
				{
					$arr=array("name"=>$b,"id"=>"fo".$this->count,"path"=>$dir.'/'.$b);
					$this->count=$this->count+1;
					array_push($array,$arr);
				}
			}
		}
		return $array;
 	}
 	public function getDirTree()
 	{
 		$array=array();
 		$uid=Auth::User()->id;
		$fids=Follow::where('user_id',Auth::User()->id)->get()->pluck('followed_id')->toArray();
		foreach ($fids as $id) {
			$arr=array("dirs"=>$this->makeTree($this->root.'/'.$id),"id"=>"fo".$this->count,"name"=>(string)$id,"path"=>$this->root.'/'.$id);
			$this->count=$this->count+1;
			array_push($array, $arr);
		}
		// dd($this->root);

 		return json_encode($array);
 	}


}
