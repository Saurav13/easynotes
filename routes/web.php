<?php
// use Counter;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', ['middleware' => 'guest', function()
{
	Counter::showAndCount('home');
     return view('frontend.landing');
}]);



Route::resource('/thread','User\ThreadController');
Route::post('/delete/thread/{id}','User\ThreadController@deleteThread');
Route::resource('comment','User\CommentController',['only'=>['update','destroy']]);
Route::post('comment/create/{thread}','User\CommentController@addThreadComment')->name('threadcomment.store');
Route::post('reply/create/{comment}','User\CommentController@addReplyComment')->name('replycomment.store');
Route::post('thread/mark-as-solution','User\ThreadController@markAsSolution')->name('markAsSolution');
Route::post('comment/like','User\LikeController@toggleLike')->name('toggleLike');

Route::get('thread/sortbycategory/{id}','User\ThreadController@ThreadSortCategory')->name('threadsort.category');
Route::get('thread/sortbyfaculty/{id}','User\ThreadController@ThreadSortFaculty')->name('threadsort.faculty');
Route::get('thread/getthreadfaculty/{id}','User\ThreadController@FacultySelect');
Route::post('thread/search','User\ThreadController@searchThreads')->name('search.threads');



Route::prefix('admin')->group(function(){

Route::get('/login','Auth\AdminLoginController@showLoginForm')->name('admin.login');
Route::post('/login','Auth\AdminLoginController@login')->name('admin.login.submit');
Route::get('/','Admin\AdminController@index')->name('admin.dashboard');



Route::prefix('category')->group(function(){

Route::get('/','Admin\CategoryController@index')->name('cat.index');
Route::post('/','Admin\CategoryController@store')->name('cat.store');
Route::post('/delete','Admin\CategoryController@destroy')->name('cat.destroy');
Route::post('/edit','Admin\CategoryController@update')->name('cat.update');
});
Route::prefix('faculty')->group(function(){

Route::get('/','Admin\FacultyController@index')->name('faculty.index');
Route::post('/','Admin\FacultyController@store')->name('faculty.store');
Route::post('/delete','Admin\FacultyController@destroy')->name('faculty.destroy');
Route::post('/update','Admin\FacultyController@update')->name('faculty.update');
Route::get('/search','Admin\FacultyController@search');

});

Route::prefix('subject')->group(function(){

Route::get('/','Admin\SubjectController@index')->name('subject.index');
Route::post('/','Admin\SubjectController@store')->name('subject.store');
Route::post('/delete','Admin\SubjectController@destroy')->name('subject.destroy');
Route::post('/update','Admin\SubjectController@update')->name('subject.update');
Route::get('/search','Admin\SubjectController@search');
});


Route::prefix('contribution')->group(function(){

Route::get('/','Admin\ContributionController@index');
Route::get('/{id}/view','Admin\ContributionController@view');
Route::post('/{id}/delete','Admin\ContributionController@delete');
});

Route::resource('post','Admin\PostController');

Route::get('events','Admin\EventController@index')->name('events.index');
Route::post('events','Admin\EventController@store')->name('events.store');
Route::post('events/delete','Admin\EventController@delete')->name('events.delete');
Route::post('events/edit','Admin\EventController@edit')->name('events.edit');

Route::post('qb/delete','Admin\QBController@destroy')->name('qb.delete');
Route::resource('qb','Admin\QBController');
// Route::post('qb/delete/{id}','Admin\QBController@destroy')->name;

Route::post('syllabus/delete','Admin\SyllabusController@destroy')->name('syllabus.delete');
Route::resource('syllabus','Admin\SyllabusController');

Route::get('opportunities','Admin\OpportunityController@index')->name('opportunities.index');
Route::post('opportunities','Admin\OpportunityController@store')->name('opportunities.store');
Route::post('opportunities/edit','Admin\OpportunityController@edit')->name('opportunities.edit');
Route::post('opportunities/delete','Admin\OpportunityController@delete')->name('opportunities.delete');

Route::get('/notes','Admin\NoteController@index')->name('notes.index');
Route::post('/notes','Admin\NoteController@store')->name('notes.store');
Route::post('/notes/update','Admin\NoteController@update')->name('notes.update');
Route::post('/notes/delete','Admin\NoteController@deleteNote')->name('notes.delete');
Route::get('/notes/filter','Admin\NoteController@filter')->name('notes.filter');
Route::get('notesfaculty/getsub','Admin\NoteController@GetSubject');
Route::get('notesfaculty/{id}','Admin\NoteController@GetFaculty');
Route::get('/sliderimage','Admin\ImageController@index')->name('sliderimage.index');
Route::post('/sliderimage','Admin\ImageController@insertslider')->name('insert.sliderimage');
Route::post('/sliderimage/delete','Admin\ImageController@delete')->name('sliderimage.delete');

});




Auth::routes();

Route::get('/home', 'Frontend\HomePageController@index')->name('home');

Route::post('/user/groups/create','User\ProfileController@create_group')->name('groups.create');
Route::post('user/group/{id}/addMembers','User\GroupController@addMembers')->name('groups.addMembers');
Route::post('user/group/{id}/removeMember','User\GroupController@removeMember')->name('groups.removeMember');
Route::get('/user/group/{id}/search','User\GroupController@searchUser');
Route::post('user/group/{id}/post','User\GroupController@message')->name('group.chat');
Route::get('/user/group/{id}','User\GroupController@group')->name('groups.group');

Route::get('admin/getfaculty/','Admin\SubjectController@CatSelect');


Route::get('/dir','DirController@index');
Route::post('/dir/root','DirController@root');
Route::post('/dir/gettree','DirController@getDirTree');
Route::post('/dir/open','DirController@openDir');
Route::post('/dir/addfolder','DirController@addFolder');
Route::post('/dir/movefolder','DirController@moveFolder');
Route::post('/dir/renamefolder','DirController@renameFolder');
Route::post('/dir/deletefolder','DirController@deleteFolder');
Route::post('/dir/deletefile','DirController@deleteFile');
Route::post('/dir/extractfile','DirController@extractFile');
Route::post('/dir/uploadfile','DirController@uploadFile');
Route::get('/dir/downloadfolder','DirController@downloadFolder');
Route::get('/dir/downloadfile','DirController@downloadFile');

Route::post('/dir/foroot','FollowDirController@root');
Route::get('/dir/downloadfofolder','FollowDirController@downloadFolder');
Route::post('/dir/foopen','FollowDirController@openDir');
Route::post('/dir/fogettree','FollowDirController@getDirTree');

Route::post('/dir/asroot','AssignmentController@root');
Route::post('/dir/asopen','AssignmentController@openDir');
Route::post('/dir/asgettree','AssignmentController@getDirTree');
Route::post('/dir/addassignment','AssignmentController@createAssignment');
Route::get('/dir/downloadasfolder','AssignmentController@downloadFolder');
Route::post('/dir/deleteasfolder','AssignmentController@deleteFolder');

Route::get('/profile/viewnotes','FollowDirController@index');

//note routes
Route::get('/note','Frontend\NoteController@notes')->name('frontend.notes');
Route::get('/getfaculty','Frontend\NoteController@getfaculty');
Route::get('/getsubject','Frontend\NoteController@getsubject');
Route::get('/note/view','Frontend\NoteController@notesview')->name('view.notes');
Route::get('note/view/{id}','Frontend\NoteController@SingleNoteView')->name('view.single');
Route::get('/note/ratenote','Frontend\NoteController@postRating')->name('rate.post');

Route::post('note/search','Frontend\SearchController@searchNotes')->name('search.notes');


Route::get('/profile','Frontend\ProfileController@profile')->name('user.profile');
Route::post('/profile/edit','Frontend\ProfileController@EditProfile')->name('edit.profile');

//facebook
Route::get('login/{provider}', 'Auth\LoginController@redirectToProvider');
Route::get('login/{provider}/callback', 'Auth\LoginController@handleProviderCallback');

//questionbank frontend
Route::get('/question','Frontend\QuestionBankController@questionbank')->name('question.banks');
Route::get('/question/view','Frontend\QuestionBankController@questionview');
Route::get('/getfacquestions','Frontend\QuestionBankController@getFacultyAndQuestions');
Route::get('/getquestions','Frontend\QuestionBankController@getquestions');

//frontend posts
Route::get('/blog','Frontend\PostsController@index')->name('frontendposts.index');
Route::get('/blog/{id}','Frontend\PostsController@single')->name('frontendpost.single');

Route::get('/events','Frontend\EventsController@index')->name('frontendevents.index');
Route::get('/events/{id}','Frontend\EventsController@single')->name('frontendevents.single');

Route::get('/opportunities','Frontend\OpportunitiesController@index')->name('frontendopportunities.index');
Route::get('/opportunities/{id}','Frontend\OpportunitiesController@single')->name('frontendopportunities.single');


Route::get('/getusers/directory','Frontend\SearchController@searchUserForDirectory')->name('search.users.directory');

Route::get('/directory/search','Frontend\SearchController@SearchForDirectory')->name('directory.follow');

Route::post('/directory/search','Frontend\SearchController@SearchUserDirectory')->name('searchforusers.directory');

Route::get('users/directory/{id}','Frontend\SearchController@DirectoryView')->name('getusers.directory');
Route::post('followdrive','Frontend\SearchController@DirectoryFollow')->name('drive.follow');

Route::get('contribute','Frontend\HomePageController@contribute');
