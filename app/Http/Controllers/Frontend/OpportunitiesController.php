<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Opportunitie;
use Counter;
class OpportunitiesController extends Controller
{
    public function index()
    {
        Counter::showAndCount('opportunities');
    	$oppo=Opportunitie::all();
    	return view('frontend.opportunities.index')->with('oppo',$oppo);
    }

    public function single($id)
    {
    Counter::showAndCount('opportunities');
    Counter::showAndCount('opportunities',$id);
    $oppo=Opportunitie::where('id','=',$id)->get();
   	return view('frontend.opportunities.single')->with('oppo',$oppo);
   }
}
