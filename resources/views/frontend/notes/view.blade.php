@extends('frontend.main')
@section('body')
<section id="content" style="padding-top: 0px;">

		<style type="text/css">
			select{
				font-size: 17px;
			}

			label, input, select, textarea {
	    font-size: 18px;
	    font-weight: normal;
	    padding-left: 10px;
	    padding-right: 10px;
	}

	td {
	    color: #656565;
	    font-weight: 400;
	    font-size: 15px;
	}
	th{
		font-weight: 600;
	}

	.well{
		background-color: rgba(220, 220, 220, 0.12);
		border:0px;

	}
		</style>

		<section id="content">
		<div class="container">
		<h1>Note</h1>
	    <hr>
		</div>
	<div class="container containershadow">
			<h3 class="text-center"><i>Subject Name: @foreach($sname as $s)</i> {{$s->subject_name}} @endforeach  </h3>

		<h5 class="text-center">  @foreach($syllabus as $sy)<a href="{{ asset('uploads/syllabus'.'/'.$sy->link)}}" download="{{$sy->link}}"><button class="btn-theme">Download Syllabus</button></a></h5>
		@endforeach      
		
		<div class="">

			  <table class="table table-condensed table-hover">
			    <thead>
			      <tr>
			        <th>Title</th>
			        <th>File Extension</th>
			        <th>Action</th>
			        <th>Rating</th>
			        
			      </tr>
			    </thead>
			    <tbody>
			    	@foreach($notes as $note)
			      <tr>
			      
			        <td >{{$note->title}}</td>
			        @if(pathinfo($note->file, PATHINFO_EXTENSION) == 'docx')
			        <td><img src="{{asset('/img/docx.png')}}" style="height: 50px; width: 50px;"> Word Document</td>
			        
			        @elseif(pathinfo($note->file, PATHINFO_EXTENSION) == 'pptx')
			        <td><img src="{{asset('/img/pptx.png')}}" style="height: 50px; width: 50px;"> Powerpoint slides</td>
			       
			        @elseif(pathinfo($note->file, PATHINFO_EXTENSION) == 'pdf')
			        <td><img src="{{asset('/img/pdf.png')}}" style="height: 50px; width: 50px;"> Pdf</td>
			        
			        @elseif(pathinfo($note->file, PATHINFO_EXTENSION) == 'readme')
			        <td><img src="{{asset('/img/readme.png')}}" style="height: 50px; width: 50px;">Readme</td>
			        
			        @elseif(pathinfo($note->file, PATHINFO_EXTENSION) == 'jpeg')
			        <td><img src="{{asset('/img/jpeg.png')}}" style="height: 50px; width: 50px;">Jpeg</td>
			        
			        @elseif(pathinfo($note->file, PATHINFO_EXTENSION) == 'php')
			        <td><img src="{{asset('/img/txt.png')}}" style="height: 20px;  width: 20px;"> Text Document</td>
			
			        @else
			        <td><img src="{{asset('/img/docx.png')}}"> Word Document</td>
			        @endif

			        <td>
			        <a href="{{route('view.single',$note->id)}}" ><button class="btn-theme" target="_blank"><i class="fa fa-eye" aria-hidden="true"></i></button>
			         <a href="{{ asset('notes/'.$note->file.'/'.$note->file)}}" download="{{$note->file}}"><button class="btn-theme"><i class="fa fa-download" aria-hidden="true"></i></button>
			        </td>
			        <td> <img src="{{asset('/img/Five-Stars.jpg')}}" style="height: 15px; width: 50px;">
			        @if($note->total_raters!=0)
			        {{$note->ratenumber/$note->total_raters}} /5
			        @else
			        0/5
			        @endif
			        </td>
			     
			    
			      </tr>
			          @endforeach
			    </tbody>
			  </table>
			</div>
</section>
@endsection