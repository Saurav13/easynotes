<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Response;
use ZipArchive;
use Assignment;
use PhpImap;
use RecursiveIteratorIterator;
use RecursiveDirectoryIterator;


class AssignmentController extends Controller
{
    private $root= 'D:/easynotes/assignments';
    public function __construct()
    {
    	$this->middleware('auth');
    }
    public function root()
 	{  	
 		$this->root=$this->root.'/'.Auth::User()->id;
		
 		$dir=$this->root;
		$buff =scandir($dir);
		$files=array();
		$folders=array();
		foreach ($buff as $b) {
			if($b!='.' and $b!='..'){
				if(strpos($b,'.')!=false)
					array_push($files, $b);
				else
					array_push($folders,$b);
			}
		}
		// dd($files);
		$path=$this->root;
		$dirs=$this->makeTree($this->root);
		return response()->json([
    		'files' => $files,
    		'folders' => $folders,
			'path'=>$path,
			'dirs'=>$dirs
			]);
		// return view('dirs.root',compact('files','folders','path'));
 	}
 	public function openDir(Request $request)
 	{

 		// dd($request);
 		$apath=$this->root.'/../'.'assignments'.'/'.Auth::User()->id;
 		// $adirs=scandir($apath);
 		// unset($adirs[0]);
 		// unset($adirs[1]);
 		$email=Auth::User()->email;
 		$username=substr($email, 0, strrpos($email, '@'));
 		// dd($username);
 		// dd($adirs);
	    $cstr='{mail.projectincube.com:993/imap/ssl}';
	    $imap=imap_open($cstr,'assignment@projectincube.com', 'iHq73f2aS2');
	    $list=imap_list($imap,$cstr,"*");
 			# code...
			// dd($d);	
				// dd($list,$cstr.'INBOX'.'.'.$username.'--'.$request->name);
				if(array_search($cstr.'INBOX'.'.'.$username.'--'.$request->name, $list))
				{
					$mailbox = new PhpImap\Mailbox('{mail.projectincube.com:993/imap/ssl}INBOX.'.$username.'--'.$request->name, 'assignment@projectincube.com', 'iHq73f2aS2',$apath.'/'.$request->name);
		    		// dd($mailbox);
		    
		    		$mails = [];
		    // Read all messaged into an array:

		    		$mailsIds = $mailbox->searchMailbox('ALL');
		    		
		    		  foreach ($mailsIds as $mailId){
				        $_mail = $mailbox->getMail($mailId);
				   		
				        $mail = array(
				            'date' => $_mail->date,
				            'fromName' => $_mail->fromName,
				            'fromAddress' => $_mail->fromAddress,
				            'subject' => $_mail->subject,
				            'content' => $_mail->textPlain,
				            'attachments' => $_mail->getAttachments()
				        );
				        $mailbox->deleteMail($mailId);
				        $mails []= $mail;
				    	
				    }
				    // dd($mails);
		    		// dd($mailsIds);
		    // if(!$mailsIds) {
		    //     return;
		    // }

		    		foreach ($mailsIds as $mailId){
		         		$mailbox->deleteMail($mailId);
		    	}
		    }
 		




 		$dir=$request->path;
		$buff =scandir($dir);
		$files=array();
		$folders=array();
		foreach ($buff as $b) {
			if($b!='.' and $b!='..'){
				if(strpos($b,'.')!=false)
					array_push($files, $b);
				else
					array_push($folders,$b);
			}
		}
		// dd($folders);
		$path=$dir;
		// dd($dir);
		// dd($files);
		return response()->json([
    		'files' => $files,
    		'folders' => $folders,
			'path'=>$path
			]);
 	}
 	private $count=0;
 	public function makeTree($dir)
 	{
 		$array=array();
 		$buff=scandir($dir);	
 		foreach ($buff as $b) {
			if($b!='.' and $b!='..'){
				if(is_dir($dir.'/'.$b))
				{
					$arr=array("dirs"=>$this->makeTree($dir.'/'.$b),"id"=>"as".$this->count,"name"=>$b,"path"=>$dir.'/'.$b);
					$this->count=$this->count+1;
					array_push($array, $arr);
				}
				else
				{
					$arr=array("name"=>$b,"id"=>"as".$this->count,"path"=>$dir.'/'.$b);
					$this->count=$this->count+1;
					array_push($array,$arr);
				}
			}
		}
		return $array;
 	}
 	public function getDirTree()
 	{
 		
 		$this->root=$this->root.'/'.Auth::User()->id;
		// dd($this->root);

 		return json_encode($this->makeTree($this->root));
 	}
 	public function createAssignment(Request $request)
 	{
 		$dir=$this->root.'/'.Auth::User()->id;
		
		$success="True";
		
		$buff =scandir($dir);
		// dd($buff,$request->name);
		if(in_array($request->name,$buff))
			$success="Folder Exists";
		else
		{
			// dd($dir,$this->root.Auth::User()->id);
			mkdir($dir.'/'.$request->name,0777);
			// if($dir==$this->root.'/'.Auth::User()->id)
			// {
			// 	// dd("ASd");
			// 	mkdir($this->root.'/../'.'assignments'.'/'.Auth::User()->id.'/'.$request->name,0777);
				
			// }
			
		}
		$buff=scandir($dir);
		$files=array();
		$folders=array();
		foreach ($buff as $b) {
			if($b!='.' and $b!='..'){
				if(strpos($b,'.')!=false)
					array_push($files, $b);
				else
					array_push($folders,$b);
			}
		}
		// dd($folders);
		$path=$dir;
		// dd($dir);
		$dirs=$this->makeTree($dir);
		// dd($dirs);
		return response()->json([
    		'files' => $files,
    		'folders' => $folders,
			'path'=>$path,
			'success'=>$success,
			'dirs'=>$dirs
			]);		
 	}

 	public function downloadFolder(Request $request)
 	{
 		 $apath=$this->root.'/../'.'assignments'.'/'.Auth::User()->id;
 		// $adirs=scandir($apath);
 		// unset($adirs[0]);
 		// unset($adirs[1]);
 		$email=Auth::User()->email;
 		$username=substr($email, 0, strrpos($email, '@'));
 		// dd($username);
 		// dd($adirs);
	    $cstr='{mail.projectincube.com:993/imap/ssl}';
	    $imap=imap_open($cstr,'assignment@projectincube.com', 'iHq73f2aS2');
	    $list=imap_list($imap,$cstr,"*");
 			# code...
			// dd($d);	
				// dd($list,$cstr.'INBOX'.'.'.$username.'--'.$d);
				if(array_search($cstr.'INBOX'.'.'.$username.'--'.$request->name, $list))
				{
					$mailbox = new PhpImap\Mailbox('{mail.projectincube.com:993/imap/ssl}INBOX.'.$username.'--'.$request->name, 'assignment@projectincube.com', 'iHq73f2aS2',$apath.'/'.$request->name);
		    		// dd($mailbox);
		    
		    		$mails = [];
		    // Read all messaged into an array:

		    		$mailsIds = $mailbox->searchMailbox('ALL');
		    		
		    		  foreach ($mailsIds as $mailId){
				        $_mail = $mailbox->getMail($mailId);
				   		
				        $mail = array(
				            'date' => $_mail->date,
				            'fromName' => $_mail->fromName,
				            'fromAddress' => $_mail->fromAddress,
				            'subject' => $_mail->subject,
				            'content' => $_mail->textPlain,
				            'attachments' => $_mail->getAttachments()
				        );
				        $mailbox->deleteMail($mailId);
				        $mails []= $mail;
				    	
				    }
				    // dd($mails);
		    		// dd($mailsIds);
		    // if(!$mailsIds) {
		    //     return;
		    // }

		    		foreach ($mailsIds as $mailId){
		         		$mailbox->deleteMail($mailId);
		    	}
		    }
 		

 		$dir = $request->path.'/'.$request->name;
		$zip_file = $request->name.'.zip';

        $rootPath = realpath($dir);

		// Initialize archive object
		$zip = new ZipArchive();
		$zip->open($request->path.'/'.$zip_file, ZipArchive::CREATE | ZipArchive::OVERWRITE);

		// Create recursive directory iterator
		/** @var SplFileInfo[] $files */
		$files = new RecursiveIteratorIterator(
		    new RecursiveDirectoryIterator($rootPath),
		    RecursiveIteratorIterator::LEAVES_ONLY
		);
		$c=0;
		foreach ($files as $name => $file)
		{
		    // Skip directories (they would be added automatically)
		    if (!$file->isDir())
		    {
		        // Get real and relative path for current file
		        $filePath = $file->getRealPath();
		        $relativePath = substr($filePath, strlen($rootPath) + 1);

		        // Add current file to archive
		        $zip->addFile($filePath, $relativePath);
		        $c=$c+1;
		    }
		}
		if($c==0)
			return "No files exists within this folder";
		// Zip archive will be created only after closing object
		$zip->close();
	    // dd($zip);
	    return Response::download($request->path.'/'.$zip_file)->deleteFileAfterSend(true);;

 	}
 	
 	private function delete_directory($dirname) {
      if (is_dir($dirname))
           $dir_handle = opendir($dirname);
	 if (!$dir_handle)
	      return false;
	 while($file = readdir($dir_handle)) {
	       if ($file != "." && $file != "..") {
	            if (!is_dir($dirname."/".$file))
	                 unlink($dirname."/".$file);
	            else
	                 $this->delete_directory($dirname.'/'.$file);
	       }
	 }
	 closedir($dir_handle);
	 rmdir($dirname);
	 return true;
	}
 	public function deleteFolder(Request $request)
 	{
 		$dir=$request->path;
		$success="True";
		
		$this->delete_directory($dir.'/'.$request->name);
		// if($dir==$this->root.'/'.Auth::User()->id)
		// 	{
		// 		// dd("ASd");
		// 		$this->delete_directory($this->root.'/../'.'assignments'.'/'.Auth::User()->id.'/'.$request->name);
		
		// 	}
		$buff=scandir($dir);
		$files=array();
		$folders=array();
		foreach ($buff as $b) {
			if($b!='.' and $b!='..'){
				if(strpos($b,'.')!=false)
					array_push($files, $b);
				else
					array_push($folders,$b);
			}
		}
		// dd($folders);
		$path=$dir;
		// dd($dir);
		$this->root=$this->root.'/'.Auth::User()->id;
		$dirs=$this->makeTree($this->root);
		return response()->json([
    		'files' => $files,
    		'folders' => $folders,
			'path'=>$path,
			'success'=>$success,
			'dirs'=>$dirs
			]);
 	}
}
 