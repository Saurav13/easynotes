<?php

namespace App\Http\Controllers\Admin;

use App\Event;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use Image;

class EventController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events=Event::all();
        return view ('admin.event.index')->with('events',$events);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        $this->validate($request,array(
            'title'=>'required|max:255',
            'description'=>'required',
            'event_date'=>'required',
            'alias'=>'required|alpha_dash|unique:events,alias'

        ));
    $events=new Event;
    $events->title=$request->title;
    $events->description=$request->description;
    $events->event_date=$request->event_date;
    $events->event_time=$request->event_time;
    $events->alias=$request->alias;
    if($request->hasFile('image'))
    {
        $file=$request->file('image');
        $filename=time() . '.' .$file->getClientOriginalExtension();//image intervention library use garera yo method use garna payo

        $path='uploads';
        $location=public_path('uploads/'. $filename);
        Image::make($file)->resize(530,320)->save($location); 
        // $file->move($path,$filename);
         
        $events->image=$filename;
    }
    $events->save();

    Session::flash('success','The Event was added Succesfully');
    return redirect()->route('events.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $this->validate($request,array(
            'title'=>'required|max:255',
            'description'=>'required',
            'event_date'=>'required',
            
        ));

         $events = Event::find($request->id);
        
         if($events->alias!=$request->alias)
         {
        $this->validate($request,array(
            'alias'=>'required|alpha_dash|unique:events,alias'

        ));
        }
         $events->title=$request->title;
         $events->description=$request->description;
         $events->event_date=$request->event_date;
         $events->event_time=$request->event_time;
         $events->alias=$request->alias;
         if($request->hasFile('image'))
         {
             if($events->files!=null){    
            unlink('uploads/'.$events->image);
            }
             $file=$request->file('image');
             $filename=time() . '.' .$file->getClientOriginalExtension();//image intervention library use garera yo method use garna payo

             $path='uploads';
             // $file->move($path,$filename);
              $location=public_path('uploads/'. $filename);
            Image::make($file)->resize(530,320)->save($location); 
             $events->image=$filename;
         }
          $events->save();
          Session::flash('success','The Event was Updated Succesfully');
          return redirect()->route('events.index');
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $events = Event::findorFail($request->id);
        if($events->files!=null){
        unlink('uploads/'.$events->files);
        }
        $events->delete();

        return redirect()->route('events.index');
    }
}
