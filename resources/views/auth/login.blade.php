@extends('frontend.main')

@section('body')
<link rel="stylesheet" type="text/css" href="{{asset('newfrontend/loginform/css/style.css')}}">
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
<div class="container">
    

    <div class="omb_login">
        <h3 class="omb_authTitle">Login or <a style="text-decoration: none;">Sign up</a></h3>
        <div class="row omb_row-sm-offset-3 omb_socialButtons">
            <div class="col-xs-12 col-sm-3">
                <a href="{{URL::to('login/facebook')}}" class="btn btn-lg btn-block omb_btn-facebook">
                    <i class="fa fa-facebook visible-xs"></i>
                    <span class="hidden-xs"><i class="fa fa-facebook hidden-xs"></i>  Facebook</span>
                </a>
            </div>
            <div class="col-xs-12 col-sm-3">
                <a href="{{URL::to('login/google')}}" class="btn btn-lg btn-block omb_btn-google">
                    <i class="fa fa-google-plus visible-xs"></i>
                    <span class="hidden-xs"><i class="fa fa-google-plus hidden-xs"></i>  Google+</span>
                </a>
            </div>  
        </div>

        <div class="row omb_row-sm-offset-3 omb_loginOr">
            <div class="col-xs-12 col-sm-6">
                <hr class="omb_hrOr">
                <span class="omb_spanOr">or</span>
            </div>
        </div>

        <div class="row omb_row-sm-offset-3">
            <div class="col-xs-12 col-sm-6">    
                <form class="omb_loginForm" method="POST" action="{{route('login')}}" accept-charset="UTF-8">
                {{csrf_field()}}
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        <input type="email" class="form-control" name="email" id="email" placeholder="email address" value="{{ old('email') }}" required autofocus>
                    </div>
                    <span class="help-block"></span>
                                        
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                        <input class="form-control" required name="password" type="password" value="" id="password" required>
                    </div>
                                          <span class="help-block">
                   @if ($errors->has('email'))
                                              <strong>{{ $errors->first('email') }}</strong>
                    @endif
                                          </span>
                                            <span class="help-block">
                    @if ($errors->has('password'))
                                                <strong>{{ $errors->first('password') }}</strong>
                    @endif
                                            </span>
                    
                    <button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
                </form>
            </div>
        </div>
        <div class="row omb_row-sm-offset-3">
            <div class="col-xs-4 col-sm-2">
                <label class="checkbox">
                    <input name="remember" type="checkbox" value="1" {{ old('remember') ? 'checked' : '' }}>Remember Me
                </label>
            </div>
            <div class="col-xs-4 col-sm-2">
                <a href="{{route('register')}}" style="text-decoration: none;"  class="text-center"><button class="btn btn-block btn-success text-center">Sign Up</button></a>
            </div>
            <div class="col-xs-4 col-sm-2">
                <p class="omb_forgotPwd">
                    <a href="{{ route('password.request') }}">Forgot password?</a>
                </p>
            </div>
        </div>     
    </div>
</div>

    <script
      src="https://code.jquery.com/jquery-3.2.1.min.js"
      integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
      crossorigin="anonymous"></script>
      <script src="{{asset('newfrontend/loginform/js/index.js')}}"></script>
    

@endsection
