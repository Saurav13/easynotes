<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContributionFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contributionfiles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('file');
            $table->integer('contribution_id')->unsigned();
            $table->foreign('contribution_id')->references('id')->on('contributions')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('contributionfiles');
    }
}
