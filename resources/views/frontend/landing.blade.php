<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Edukunja</title>
    <!-- BOOTSTRAP CORE STYLE CSS -->
    <link href="{{asset('frontend-assets/assets/css/bootstrap.css')}}" rel="stylesheet" />
    <!-- FONT AWESOME CSS -->
<link href="{{asset('frontend-assets/assets/css/font-awesome.min.css')}}" rel="stylesheet" />
     <!-- FLEXSLIDER CSS -->
<link href="{{asset('frontend-assets/assets/css/flexslider.css')}}" rel="stylesheet" />
    <!-- CUSTOM STYLE CSS -->
    <link href="{{asset('frontend-assets/assets/css/style.css')}}" rel="stylesheet" />    
  <!-- Google	Fonts -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,300' rel='stylesheet' type='text/css' />
</head>
<body >

<div id="contact-sec">
	<div class="overlay">
		<div class="row">
			<div class="col-lg-2 col-lg-offset-1">
				<img src="{{asset('frontend-assets/assets/img/logo180-50.png')}}" style="height: 150px; width: 150px;
">
			</div>
			<div class="col-lg-6 col-lg-offset-3" style="
    padding-left: 0px;
    padding-right: 50px;
">
{{-- login --}}
				 <form role="form" method="POST" action="{{ route('login') }}">  
                    
                        {{ csrf_field() }}
                        <div class="{{ $errors->has('email') ? ' has-error' : '' }}">
                            <div class="col-md-5">
                              <label for="name" class="">Username</label>

                                <input id="email" type="email" placeholder="Email Address" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                         <div class="{{ $errors->has('password') ? ' has-error' : '' }}">

                            <div class="col-md-5">
                            <label for="name" class=" ">Password</label>

                                <input id="password" type="password" placeholder="Password..." class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                       


                            <div class="col-md-2 ">
                             <label for="name" class=""></label>
                                <button type="submit" class="btn btn-primary btn-block" style="border-radius: 30px; margin-top: 5px;">
                                    Login
                                </button>



                                
                            </div>
                        </div>
                           <div class="form-group">
                            <div class="col-md-5">
                                <div class="checkbox">
                                <label class="checkbox">
                                        <input type="checkbox" value="remember" id="rememberMe" name="remember" style="margin-top: 10px;" {{ old('remember') ? 'checked' : '' }} > Remember me
                                      </label>
                                   
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                          <div class="col-md-5">

                        <a class="" href="{{ route('password.request') }}" style="color: white;">
                                    Forgot Your Password?
                                </a>
                                </div>
                                </div>
                    </form> 
                    {{-- end login --}}
			</div>
		</div>
		<div class="row">
			<div class="container set-pad" style="padding-top: 0px;">
     <div class="row">

     <div class="col-lg-6 col-md-6 col-sm-6 " style="
    border-top-width: 20px;

    padding-top: 20px;
">
     	<div id="carousel-example-generic" class="carousel slide carousel-fade" data-ride="carousel" data-interval="2000">
     	  <!-- Wrapper for slides -->
     	    <div class="carousel-inner" style=" width:100%; height: 150px !important;">
     	      <div class="item active">
     	        <h2>Legal <small>Affadavits, Addendums, and more</small></h2>
     	        <p class="lead">Attorney Services; Legal Briefs, Addendums, Appeals, ... We work closely with the CT Appelate and Supreme Court</p>
     	      </div>
     	      <div class="item">
     	        <h2>Copying</h2>
     	        <p class="lead">Restore old photos; Get extra copies of that stuff you've always wanted. Oh and we'll copy that other stuff for you, too.</p>
     	      </div>
     	      <div class="item">
     	        <h2><small>Layout &</small> Design</h2>
     	        <p class="lead">Letterheads, Business Cards, and Typographic wonderment - we do it all in-house</p>
     	      </div>    
     	    </div>
     	  </div>
     </div>
                <div class="col-lg-6  col-md-6 col-sm-6">
                    <p style="padding-top: 0px;" data-scroll-reveal="enter from the bottom after 0.1s" class="header-line text-center" style="margin-top:0px; "" ><h1>Create an account</h1><p>Get registered! It's free</p></p>
                    <form class="form" role="form" method="POST" action="{{ route('register') }}">
                                                       {{ csrf_field() }}

                                                       <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                                          

                                                           <div>
                                                               <input id="name" type="text"  placeholder="Name" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                                               @if ($errors->has('name'))
                                                                   <span class="help-block">
                                                                       <strong>{{ $errors->first('name') }}</strong>
                                                                   </span>
                                                               @endif
                                                           </div>
                                                       </div>

                                                       <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                                         

                                                           <div >
                                                               <input id="email" type="email" placeholder="Email Address" class="form-control" name="email" value="{{ old('email') }}" required>

                                                               @if ($errors->has('email'))
                                                                   <span class="help-block">
                                                                       <strong>{{ $errors->first('email') }}</strong>
                                                                   </span>
                                                               @endif
                                                           </div>
                                                       </div>

                                                       <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                                          
                                                           <div>
                                                               <input id="password"  type="password" placeholder="Password" class="form-control" name="password" required>

                                                               @if ($errors->has('password'))
                                                                   <span class="help-block">
                                                                       <strong>{{ $errors->first('password') }}</strong>
                                                                   </span>
                                                               @endif
                                                           </div>
                                                       </div>

                                                       <div class="form-group">
                                                           
                                                           <div>
                                                               <input id="password-confirm" type="password" placeholder="Confirm password" class="form-control" name="password_confirmation" required>
                                                           </div>
                                                       </div>

                                                       

                                                       <div class="form-group">
                                                           <div>
                                                               <button type="submit" class="btn btn-primary btn-block" >
                                                                   Register
                                                               </button>
                                                           </div>
                                                       </div>
                                                   </form>
                                                    <h5 class="text-center">OR</h5>
                                                    
                                                    <div class=""><h5 class="text-center"> LOGIN WITH : 
                                                    <a href="{{URL::to('login/facebook')}}"><img height="30px" width="30px" style="border-radius: 50%;" src="{{asset('frontend-assets/img/fb.png')}}"/></a> 
                                                    <a href="{{URL::to('login/google')}}"><img height="30px" width="30px" style="border-radius: 50%;" src="{{asset('frontend-assets/img/g.png')}}"/></a></div>
                  
                </div>

            </div>
            <!--/.HEADER LINE END-->
        
              </div>
               </div>
         </div> 
		</div>
	</div>
</div>

   
       <!--HOME SECTION END-->   
    <div  class="tag-line" >
         <div class="container">
           <div class="row  text-center" >
           
               <div class="col-lg-12  col-md-12 col-sm-12">
               
        <h2 data-scroll-reveal="enter from the bottom after 0.1s" >WELCOME TO EDUKUNJA!</h2>
                   </div>
               </div>
             </div>
        
    </div>
    <!--HOME SECTION TAG LINE END-->   
         <div id="features-sec" class="container set-pad" >
             
             <!--/.HEADER LINE END-->


           <div class="row" >
           
               
                 <div class="col-lg-4  col-md-4 col-sm-4" data-scroll-reveal="enter from the bottom after 0.4s">
                     <div class="about-div">
                     <a href="{{URL::to('note')}}" style="text-decoration: none;" target="_blank">
                     <i class="fa fa-file-text-o fa-4x icon-round-border" ></i>
                     </a>
                   <h3>Notes</h3>
                 <hr />
                       <hr />
                   <p >
                       Study materials organized as per your subject, semester and your faculty. 
                   </p>
               <a href="{{URL::to('note')}}" class="btn btn-info btn-set" target="_blank" style="text-decoration: none;" >View Notes</a>
                </div>
                   </div>
                   <div class="col-lg-4  col-md-4 col-sm-4" data-scroll-reveal="enter from the bottom after 0.5s">
                     <div class="about-div">
                   <a href="{{URL::to('thread')}}" style="text-decoration: none;" target="_blank">
                     <i class="fa fa-bolt fa-4x icon-round-border" ></i>
                   </a>
                   <h3 >Discussion Forums</h3>
                 <hr />
                       <hr />
                   <p >
                       Create a discussion thread and let eachother help eachother!
                   </p>
               <a href="{{URL::to('thread')}}" style="text-decoration: none;" class="btn btn-info btn-set">Visit the forum</a>
                </div>
                   </div>
                 <div class="col-lg-4  col-md-4 col-sm-4" data-scroll-reveal="enter from the bottom after 0.6s">
                     <div class="about-div">
                     <a href="{{URL::to('question')}}" style="text-decoration: none;" target="_blank">
                     <i class="fa fa-question fa-4x icon-round-border" ></i>
                     </a>
                   <h3>Question Banks</h3>
                 <hr />
                       <hr />
                   <p >
                      Find past questions and syllabus all in one place in organized manner.
                   </p>
               <a href="{{URL::to('question')}}" class="btn btn-info btn-set" style="text-decoration: none;" target="_blank" >View Question Bank</a>
                </div>
                   </div>
                 
                 
               </div>
             </div>




                 
     <!-- CONTACT SECTION END-->
    <div id="footer">
          &copy 2017 www.edukunja.com | All Rights Reserved 
    </div>
     <!-- FOOTER SECTION END-->
   
    <!--  Jquery Core Script -->
    <script src="{{asset('frontend-assets/assets/js/jquery-1.10.2.js')}}"></script>
    <!--  Core Bootstrap Script -->
    <script src="{{asset('frontend-assets/assets/js/bootstrap.js')}}"></script>
    <!--  Flexslider Scripts --> 
         <script src="{{asset('frontend-assets/assets/js/jquery.flexslider.js')}}"></script>
     <!--  Scrolling Reveal Script -->
    <script src="{{asset('frontend-assets/assets/js/scrollReveal.js')}}"></script>
    <!--  Scroll Scripts --> 
    <script src="{{asset('frontend-assets/assets/js/jquery.easing.min.js')}}"></script>
    <!--  Custom Scripts --> 
         <script src="{{asset('frontend-assets/assets/js/custom.js')}}"></script>
</body>
</html>
