<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Syllabus;
use App\Faculty;
use Redirect;

class SyllabusController extends Controller
{
     public function index()
    {
    	$syllabus=Syllabus::join('faculties','syllabuses.faculty_id','faculties.faculty_id')->select('syllabuses.*','faculties.faculty_name')->orderBy('id','desc')->paginate(100);
    	return view('admin.syllabus.list')->with('syllabus',$syllabus);
    }

    public function create()
    {
    	$faculties=Faculty::all()->pluck('faculty_name','faculty_id');
    	return view('admin.syllabus.form',compact('faculties'));
    }
    public function store(Request $request)
    {
         $this->validate($request, array(
            'title'=>'required|max:255',
            'semester'=>'required',
            'file'=>'required',
            'faculty_id'=>'required'
        ));
    	$q=new Syllabus;
    	$q->title=$request->title;
    	$q->semester=$request->semester;
    	$q->faculty_id=$request->faculty_id;
        $q->link="qqq";
        $q->save();
		$file=$request->file('file');

		$extension = $file->getClientOriginalExtension();
		$fileName = $q->id.'_'.$q->semester.'_'.$q->faculty_id.'.'.$extension;
		$file->move('uploads/syllabus',$fileName);
		$q->link=$fileName;
		$q->save();
		return Redirect::to('admin/syllabus');
    }
    public function edit($id)
    {
        $faculties=Faculty::all()->pluck('faculty_name','faculty_id');
        $syllabus=Syllabus::find($id);
        return view('admin.syllabus.form',compact('syllabus','faculties'));


    }
    public function update(Request $request, $id)
    {
         $this->validate($request, array(
            'title'=>'required|max:255',
            'semester'=>'required',
            'file'=>'required',
            'faculty_id'=>'required'
        ));
        $q = Syllabus::find($id);
        $q->title=$request->title;
        $q->semester=$request->semester;
        $q->faculty_id=$request->faculty_id;

        unlink('uploads/syllabus'.'/'.$q->link);
        $file=$request->file('file');

        $extension = $file->getClientOriginalExtension();
        $fileName = $q->id.'_'.$q->semester.'_'.$q->faculty_id.'.'.$extension;
        $file->move('uploads/syllabus',$fileName);
        $q->link=$fileName;
        $q->save();
        return Redirect::to('admin/syllabus');
    }
    public function destroy(Request $request)
    {
        $id=$request->id;
        $q=Syllabus::find($id);
        if($q!=null)
        {
        unlink('uploads/syllabus'.'/'.$q->link);
        $q->delete();
        }
        return Redirect::to('admin/syllabus');
    }
}
