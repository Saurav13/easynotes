<script src="{{asset('newfrontend/js/jquery.js')}}"></script>
<script src="{{asset('newfrontend/js/jquery.easing.1.3.js')}}"></script>
<script src="{{asset('newfrontend/js/bootstrap.min.js')}}"></script>
<script src="{{asset('newfrontend/js/jquery.fancybox.pack.js')}}"></script>
<script src="{{asset('newfrontend/js/jquery.fancybox-media.js')}}"></script>
<script src="{{asset('newfrontend/js/google-code-prettify/prettify.js')}}"></script>
<script src="{{asset('newfrontend/js/portfolio/jquery.quicksand.js')}}"></script>
<script src="{{asset('newfrontend/js/portfolio/setting.js')}}"></script>
<script src="{{asset('newfrontend/js/jquery.flexslider.js')}}"></script>
<script src="{{asset('newfrontend/js/animate.js')}}"></script>
<script src="{{asset('newfrontend/js/custom.js')}}"></script>
<script type="text/javascript">
  document.onreadystatechange = function () {
    var state = document.readyState
    if (state == 'interactive') {
         document.getElementById('contents').style.visibility="hidden";
    } else if (state == 'complete') {
        setTimeout(function(){
           document.getElementById('interactive');
           document.getElementById('load').style.visibility="hidden";
           document.getElementById('contents').style.visibility="visible";
        },1000);
    }
  }
</script>



</body>
</html>