@extends('frontend.main')
@section('body')

<style type="text/css">
		select{
			font-size: 15px;
		}

		label, input, button, select, textarea {
    font-size: 16px;
    font-weight: normal;
    padding-left: 10px;
    padding-right: 10px;
}

ul.cat li a, ul.folio-detail li a {
    color: #656565;
    font-weight: 600;
}

.well{
	background-color: rgba(220, 220, 220, 0.12);
	border:0px;

}


	</style>
<style type="text/css">
	@-webkit-keyframes typing { from { width: 0; } }
	@-webkit-keyframes blink-caret { 50% { border-color: transparent; } }

	h6 { 
	    font: bold 170% Consolas, Monaco, monospace;
	    border-right: .1em solid black;
	    width: 43em; 
	    width: 100;
	    margin: 2em 1em;
	    white-space: nowrap;
	    overflow: hidden;
	    -webkit-animation: typing 2s steps(70, end),
	               blink-caret .5s step-end infinite alternate;

	}

</style>
	

	<div class="container">
	<br>
	
	</div>
		<div class="container containershadow">
		<ul class="" style="margin-left: 0px;">

		<h3>Choose you Field</h3>

		<div class="row col-md-8">
			<div class="col-md-4">
		<label>Field :</label>
		<select name="category" id='category' class="form-control">
		<option selected disabled>Choose Field</option>
		@foreach($categories as $category)
		  <option value="{{$category->category_id}}" >{{$category->category_name}}</option>
		@endforeach
		</select>
		</div>
		<div class="col-md-4">
		<label for="faculty">Faculty:</label>
		<select name="faculty" id='faculty' class="form-control" >
		<option selected disabled>Choose Faculty</option>

		</select>	
		</div>
		</div>
		
	
		

		<!-- <button class="pull-right btn btn-theme"><i class="fa fa-search"></i></button>
		<input type="" name="Search" placeholder="Search" class="pull-right"> -->
		<div class="row col-md-4 pull-right">
		<form class="form-inline" action="{{route('search.notes')}}" method="POST">
			{{csrf_field()}}
            <div class="input-group">
              <input class="form-control" type="text" name="search" placeholder="Search for...">
              <span class="input-group-btn">
                <button class="btn btn-theme" type="submit">
                  <i class="fa fa-search" style="line-height: 1.7"></i>
                </button>
              </span>
            </div>
          </form>

          <br>
          </div>
		</ul>

		</div>
		<br>
		<div class="preloader hidden" id="preloader" align="center">
			<img src="{{asset('img/preloader.gif')}}" style="height: 100px; width: 100px;" >
		</div>
		<div id="noresult" class="hidden" align="center">
			<img src="{{asset('img/noresults.png')}}" style="height: 300px; width: 382px;">
		</div>
		<section id="information" style="padding-top: 0px;">
			<div class="container containershadow">
				<h6>Welcome to Notes Section!
				Proceed by selecting your Faculty and Field first!</h6>
			</div>	
		</section>	
			<section id="content" style="padding-top: 0px;" class="hidden">



				<div class="container containershadow">
					<h3 id="facultyheading"></h3>
					<div class="col-lg-3" id="hidefirst">
					<div class="widget">
							<h4 class="widgetheading" style="color: #a94442;">First Semester</h4>
							<ul id="first" class="cat">
							</ul>
					</div>
					</div>
					<div class="col-lg-3" id="hidesecond">
					<div class="widget">
							<h4 class="widgetheading" style="color: #a94442;">Second Semester</h4>
							<ul class="cat" id="second">
							</ul>
					</div>
					</div>
					<div class="col-lg-3" id="hidethird">
					<div class="widget">
							<h4 class="widgetheading" style="color: #a94442;">Third Semester</h4>
							<ul class="cat" id="third">
							</ul>
					</div>
					</div>
					<div class="col-lg-3" id="hidefourth">
					<div class="widget">
							<h4 class="widgetheading" style="color: #a94442;">Fourth Semester</h4>
							<ul class="cat" id="fourth">
							</ul>
					</div>
					</div>
					<div class="col-lg-3" id="hidefifth">
					<div class="widget">
							<h4 class="widgetheading" style="color: #a94442;">Fifth Semester</h4>
							<ul class="cat" id="fifth">
							</ul>
					</div>
					</div>
					<div class="col-lg-3" id="hidesixth">
					<div class="widget">
							<h4 class="widgetheading" style="color: #a94442;">Sixth Semester</h4>
							<ul class="cat" id="sixth">
							</ul>
					</div>
					</div>
					<div class="col-lg-3" id="hideseventh">
					<div class="widget">
							<h4 class="widgetheading" style="color: #a94442;">Seventh Semester</h4>
							<ul class="cat" id="seventh">
							</ul>
					</div>
					</div>
					<div class="col-lg-3" id="hideeighth">
					<div class="widget">
							<h4 class="widgetheading" style="color: #a94442;">Eighth Semester</h4>
							<ul class="cat" id="eighth">
							</ul>
					</div>
					</div>
					</div>
				
			</section>

	
	<script
	  src="https://code.jquery.com/jquery-3.2.1.min.js"
	  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
	  crossorigin="anonymous"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		$('#category').on('change', function(e){
			$('#information').addClass('hidden');
			$('#noresult').addClass('hidden');
			$('#preloader').removeClass('hidden');
			$('#content').addClass('hidden');
			var cat_id = e.target.value;

			//ajax
			$.get('getfaculty?cat_id=' + cat_id,function(data){

				$('#faculty').empty();
				

				$.each(data.faculties, function(index, faculties){
					$('#faculty').append('<option value="'+faculties.faculty_id+'" name="'+faculties.faculty_name+'" >'+faculties.faculty_name+'</option>')
					
					//bake cha for faculty heading
				});

					$('#first').empty();
					$('#second').empty();
					$('#third').empty();
					$('#fourth').empty();
					$('#fifth').empty();
					$('#sixth').empty();
					$('#seventh').empty();
					$('#eighth').empty();
					
					if($.isEmptyObject(data.subjects)){
					  $('#preloader').addClass('hidden');
					  $('#noresult').fadeIn(1000).removeClass('hidden');
					}
				$.each(data.subjects, function(index,subjects){

					if(data.subjects != null)
					{
					$('#noresult').addClass('hidden');
					$('#preloader').addClass('hidden');
					$('#content').fadeIn(1000).removeClass('hidden');
					}
					
					
					if(subjects.semester=='1st')
					{		
						$('#first').append('<li><i class="icon-angle-right"></i><a href="{{URL::to('note/view')}}/?id='+subjects.subject_id+'&semester='+subjects.semester+'&faculty='+subjects.faculty_id+'">'+subjects.subject_name+'</li>');
				
					}
					if(subjects.semester=='2nd')
					{
						// $.each(data.subjects,function(index,subjects){
						$('#second').append('<li><i class="icon-angle-right"></i><a href="{{URL::to('note/view')}}/?id='+subjects.subject_id+'&semester='+subjects.semester+'&faculty='+subjects.faculty_id+'">'+subjects.subject_name+'</li>');
					// });

					}
					if(subjects.semester=='3rd')
					{
						// $.each(data.subjects,function(index,subjects){
						$('#third').append('<li><i class="icon-angle-right"></i><a href="{{URL::to('note/view')}}/?id='+subjects.subject_id+'&semester='+subjects.semester+'&faculty='+subjects.faculty_id+'">'+subjects.subject_name+'</li>');
					}
					if(subjects.semester=='4th')
					{
						// $.each(data.subjects,function(index,subjects){
						$('#fourth').append('<li><i class="icon-angle-right"></i><a href="{{URL::to('note/view')}}/?id='+subjects.subject_id+'&semester='+subjects.semester+'&faculty='+subjects.faculty_id+'">'+subjects.subject_name+'</li>');;
					// });
					}
					if(subjects.semester=='5th')
					{
						// $.each(data.subjects,function(index,subjects){
						$('#fifth').append('<li><i class="icon-angle-right"></i><a href="{{URL::to('note/view')}}/?id='+subjects.subject_id+'&semester='+subjects.semester+'&faculty='+subjects.faculty_id+'">'+subjects.subject_name+'</li>');
					// });
					}
					if(subjects.semester=='6th')
					{
						// $.each(data.subjects,function(index,subjects){
						$('#sixth').append('<li><i class="icon-angle-right"></i><a href="{{URL::to('note/view')}}/?id='+subjects.subject_id+'&semester='+subjects.semester+'&faculty='+subjects.faculty_id+'">'+subjects.subject_name+'</li>');
					// });
					}
					if(subjects.semester=='7th')
					{
						// $.each(data.subjects,function(index,subjects){
						$('#seventh').append('<li><i class="icon-angle-right"></i><a href="{{URL::to('note/view')}}/?id='+subjects.subject_id+'&semester='+subjects.semester+'&faculty='+subjects.faculty_id+'">'+subjects.subject_name+'</li>');
					// });
					}
					if(subjects.semester=='8th')
					{
						// $.each(data.subjects,function(index,subjects){
						$('#eighth').append('<li><i class="icon-angle-right"></i><a href="{{URL::to('note/view')}}/?id='+subjects.subject_id+'&semester='+subjjects.semester+'&faculty='+subjects.faculty_id+'">'+subjects.subject_name+'</li>');
					// });
					}
					
				});
				

				$('#faculty').on('change', function(e){
					$('#information').addClass('hidden');
					$('#preloader').removeClass('hidden');
					$('#content').addClass('hidden');
					$('#noresult').addClass('hidden');
					var faculty_id = e.target.value;
					
					$.get('getsubject?faculty_id=' + faculty_id,function(data){
							$('#first').empty();
							$('#second').empty();
							$('#third').empty();
							$('#fourth').empty();
							$('#fifth').empty();
							$('#sixth').empty();
							$('#seventh').empty();
							$('#eighth').empty();
							if($.isEmptyObject(data.sub)){
							  $('#preloader').addClass('hidden');
							  $('#noresult').fadeIn(1000).removeClass('hidden');
							}
						$.each(data.sub, function(index,sub){
							if(data.sub != null)
							{
							$('#noresult').addClass('hidden');
							$('#preloader').addClass('hidden');
							$('#content').fadeIn(1000).removeClass('hidden');
							}
							if(sub.semester=='1st')
							{
								
								$('#first').append('<li><i class="icon-angle-right"></i><a href="{{URL::to('note/view')}}/?id='+sub.subject_id+'&semester='+sub.semester+'&faculty='+sub.faculty_id+'">'+sub.subject_name+'</li>');
							 
							}
							if(sub.semester=='2nd')
							{
								// $.each(data.sub,function(index,sub){
								$('#second').append('<li><i class="icon-angle-right"></i><a href="{{URL::to('note/view')}}/?id='+sub.subject_id+'&semester='+sub.semester+'&faculty='+sub.faculty_id+'">'+sub.subject_name+'</li>');
							// });
							}
							if(sub.semester=='3rd')
							{
								// $.each(data.sub,function(index,sub){
								$('#third').append('<li><i class="icon-angle-right"></i><a href="{{URL::to('note/view')}}/?id='+sub.subject_id+'&semester='+sub.semester+'&faculty='+sub.faculty_id+'">'+sub.subject_name+'</li>');
							// });
							}
							if(sub.semester=='4th')
							{
								// $.each(data.sub,function(index,sub){
								$('#fourth').append('<li><i class="icon-angle-right"></i><a href="{{URL::to('note/view')}}/?id='+sub.subject_id+'&semester='+sub.semester+'&faculty='+sub.faculty_id+'">'+sub.subject_name+'</li>');
							// });
							}
							if(sub.semester=='5th')
							{
								// $.each(data.sub,function(index,sub){
								$('#fifth').append('<li><i class="icon-angle-right"></i><a href="{{URL::to('note/view')}}/?id='+sub.subject_id+'&semester='+sub.semester+'&faculty='+sub.faculty_id+'">'+sub.subject_name+'</li>');
							// });
							}
							if(sub.semester=='6th')
							{
								// $.each(data.sub,function(index,sub){
								$('#sixth').append('<li><i class="icon-angle-right"></i><a href="{{URL::to('note/view')}}/?id='+sub.subject_id+'&semester='+sub.semester+'&faculty='+sub.faculty_id+'">'+sub.subject_name+'</li>');
								// });
							}
							if(sub.semester=='7th')
							{
								// $.each(data.sub,function(index,sub){
								$('#seventh').append('<li><i class="icon-angle-right"></i><a href="{{URL::to('note/view')}}/?id='+sub.subject_id+'&semester='+sub.semester+'&faculty='+sub.faculty_id+'">'+sub.subject_name+'</li>');
							// });
							}
							if(sub.semester=='8th')
							{
								// $.each(data.sub,function(index,sub){
								$('#eighth').append('<li><i class="icon-angle-right"></i><a href="{{URL::to('note/view')}}/?id='+sub.subject_id+'&semester='+sub.semester+'&faculty='+sub.faculty_id+'">'+sub.subject_name+'</li>');
							// });
							}
					});

				});
			});
		});

	});
});
	
	</script>

@endsection