@if(Session::has('success'))


	
		<div class="container">
			<div class="row">

			<div class="alert alert-success" role="alert">
					
					<strong>Success:</strong>{{Session::get('success')}}
			</div>
		</div>
	</div>
	

@endif

@if(!$errors->isEmpty())

<div class="container">

	<div class="alert alert-warning" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<strong>Errors:</strong>
		<ul>
		@foreach($errors->all() as $error)
				
			<li>{{ $error }}</li>

		@endforeach
		</ul>
	</div>	
</div>	

@endif