<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Thread extends Model
{
    protected $fillable=['title','description','category_name','faculty_id'];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }

   public function comments()
   {
   		return $this->morphMany(Comment::class,'commentable')->latest();
   }
     
}
