@extends('frontend.main')
@section('body')
<section id="content" style="padding-top: 0px;">

		<style type="text/css">
			select{
				font-size: 17px;
			}

			label, input, select, textarea {
	    font-size: 18px;
	    font-weight: normal;
	    padding-left: 10px;
	    padding-right: 10px;
	}

	td {
	    color: #656565;
	    font-weight: 400;
	    font-size: 15px;
	}
	th{
		font-weight: 600;
	}

	.well{
		background-color: rgba(220, 220, 220, 0.12);
		border:0px;

	}
		</style>
		@if($countnotes == 0)
		<div class="container">
		<div id="contenedor">
		  <div id="error" align="center">
		    <div align="center">
		        <img src="{{asset('img/errorpage.png')}}">
		    </div>
		   
		    <h1 class="sub_desc">Sorry, we could'nt find any results matching "{{$search}}"</h1>
		    <h2>Try Searching using other keywords</h2>
		</div>
	</div>
				<form class="form-inline" action="{{route('search.notes')}}" method="POST">
					{{csrf_field()}}
		            <div class="input-group">
		              <input class="form-control" type="text" name="search" placeholder="Search for...">
		              <span class="input-group-btn">
		                <button class="btn btn-theme" type="submit">
		                  <i class="fa fa-search" style="line-height: 1.7"></i>
		                </button>
		              </span>
		            </div>
		          </form>
		  		 
		</div>
		@else
		<section id="content">
		<div class="container">
		<h1>Search Results...</h1>
	    <hr>
		</div>
	<div class="container containershadow">
			<h3 class="text-center">Found Contents..</h3>

		
		
		<div class="">

			  <table class="table table-condensed table-hover">
			    <thead>
			      <tr>
			        <th>Title</th>
			        <th>File Extension</th>
			        <th>Action</th>
			        <th>Rating</th>
			        <th>Review</th> 
			      </tr>
			    </thead>
			    <tbody>
			    	@foreach($notes as $note)
			      <tr>
			      
			        <td >{{$note->title}}</td>
			        @if(pathinfo($note->file, PATHINFO_EXTENSION) == 'docx')
			        <td><img src="{{asset('/img/docx.png')}}"> Word Document</td>
			        
			        @elseif(pathinfo($note->file, PATHINFO_EXTENSION) == 'pptx')
			        <td><img src="{{asset('/img/pptx.png')}}"> Powerpoint slides</td>
			       
			        @elseif(pathinfo($note->file, PATHINFO_EXTENSION) == 'pdf')
			        <td><img src="{{asset('/img/pdf.png')}}"> Pdf</td>
			        
			        @elseif(pathinfo($note->file, PATHINFO_EXTENSION) == 'readme')
			        <td><img src="{{asset('/img/readme.png')}}">Readme</td>
			        
			        @elseif(pathinfo($note->file, PATHINFO_EXTENSION) == 'jpeg')
			        <td><img src="{{asset('/img/jpeg.png')}}">Jpeg</td>
			        
			        @elseif(pathinfo($note->file, PATHINFO_EXTENSION) == 'php')
			        <td><img src="{{asset('/img/txt.png')}}" style="height: 20px;  width: 20px;"> Text Document</td>
			
			        @else
			        <td><img src="{{asset('/img/docx.png')}}"> Word Document</td>
			        @endif

			        <td>
			        <a href="{{route('view.single',$note->id)}}" ><button class="btn-theme" target="_blank"><i class="fa fa-eye" aria-hidden="true"></i></button>
			         <a href="{{ asset('notes/'.$note->file.'/'.$note->file)}}" download="{{$note->file}}"><button class="btn-theme"><i class="fa fa-download" aria-hidden="true"></i></button>
			        </td>
			        <td> <img src="{{asset('/img/Five-Stars.jpg')}}" style="height: 15px; width: 50px;">
			        @if($note->total_raters!=0)
			        {{$note->ratenumber/$note->total_raters}} /5
			        @else
			        0/5
			        @endif
			        </td>
			        <td>2</td>
			    
			      </tr>
			          @endforeach
			    </tbody>
			  </table>
			</div>
</section>
@endif
@endsection