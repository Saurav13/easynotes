<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Sliderimage;
use App\Event;
use App\Post;
use App\Opportunitie;
use Counter;

class HomePageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        Counter::showAndCount('home');
        $opportunities = Opportunitie::orderBy('id','desc')->paginate(5);     
    	$events = Event::orderBy('id','desc')->paginate(5);
    	$posts = Post::orderBy('id','desc')->paginate(5);
    	$slider = Sliderimage::all();
    	return view ('frontend.home.home')->with('slider',$slider)->with('events',$events)->with('posts',$posts)->with('opportunities',$opportunities);

    }

    public function contribute()
    {
        return view('frontend.contribute');
    }
  
}


