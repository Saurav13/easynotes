<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Event;
use Counter;
class EventsController extends Controller
{
    public function index()
    {
        Counter::showAndCount('events');
    	$events=Event::all();
    	return view('frontend.events.index')->with('events',$events);
    }

    public function single($id)
    {
        Counter::showAndCount('events');
        Counter::showAndCount('events',$id);
        $events=Event::where('id','=',$id)->get();
   	
        return view('frontend.events.singleevents')->with('events',$events);
   }
}
