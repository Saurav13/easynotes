@extends('frontend.main')

@section('body')
<div class="container" style="margin-bottom: 30px";>
				<h1>Edit thread</h1>
				        <hr>
				        <div class="alert alert-info">
				            <p>
				                Please try to search for your question first using
				                <a href="https://laravel.io/forum" class="alert-link">the search box</a> and make sure you've read our
				                <a href="https://laravel.io/rules" class="alert-link">Forum Rules</a> before creating a thread.
				            </p>
				        </div>
                
					
				<form action="{{route('thread.update',$thread->id)}}" role="form" method="post">
				
				{{csrf_field()}}
				{{method_field('put')}}
				<div class="form-group">
				    <label for="title">Subject</label>
				    <input class="form-control" required name="title" type="text" id="cattitle"  value="{{$thread->title}}">
				</div>
				<div class="form-group">
				    <label for="description">Body</label>
				    <textarea class="form-control wysiwyg" required name="description" cols="50" rows="10" id="editor">{{$thread->description}}</textarea>
				</div>
					    <div class="form-group">
					    	<label for="category_name">Category</label>
					  	<select name="category_name" class="form-control" id='category'>
					  	@foreach($categories as $category) 	
					  	  <option value="{{$category->category_id}}" >{{$category->category_name}}</option>
					  	@endforeach
					  	</select>
					    </div>
					      <div class="form-group">
					         <label for="faculty_id">Faculty</label>
					       <select name="faculty_id" class="form-control" id='faculty'>
					       <option selected disabled>Choose here</option>
					       
					       </select>
					      </div>
					<button class="btn btn-md btn-success" id="savecat">Save Changes</button>
				 </form>
					 
</div>
</div>


<script
  src="https://code.jquery.com/jquery-3.2.1.min.js"
  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  crossorigin="anonymous"></script>
  <script type="text/javascript">
    $(document).ready(function(){
      $('#category').on('change',function(e){
          console.log('ready');
          var cat_id = e.target.value;

          //ajax
          $.get('/thread/getthreadfaculty/'+cat_id,function(data){

            $('#faculty').empty();

            $.each(data.faculties, function(index, faculties){
              console.log(faculties);
              $('#faculty').append('<option value="'+faculties.faculty_id+'" name="'+faculties.faculty_name+'" >'+faculties.faculty_name+'</option>')
              
              //bake cha for faculty heading
            });
      });
    });
  });
  </script>
 <script>
         CKEDITOR.replace( 'editor' );
    </script>


	

@endsection