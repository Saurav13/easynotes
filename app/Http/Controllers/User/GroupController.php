<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Group;
use App\User;
use Auth;
use App\GroupChat;

class GroupController extends Controller
{
	private $group;
	private $user;

    public function __construct(){
    	$this->middleware('auth');
        
    	$this->middleware(function ($request, $next) {
            
    		$this->group = Group::find($request->route()->parameters()['id']);

            $this->user= Auth::user();
            
            if ($this->group->members()->find($this->user->id))
				return $next($request);
			else
                return response('You are not alowed here', 200)->header('Content-Type', 'text/plain');
        });

    }

    public function group(){
        
        return view('users.groups.group')->with('group',$this->group);
    }

    public function searchUser(Request $request){
        $final_results = [];
        $results = User::where('email','LIKE', '%'.$request->q.'%')->get();
        foreach ($results as $k) {
            if (!$this->group->members()->find($k->id))
                $final_results []= $k;
        }
        return json_encode($final_results);
    }

    public function addMembers(Request $request){
        $memberList = $request->newmembers;
        $this->group->members()->attach($memberList);
        $this->group->member_no += count($request->newmembers);
        $this->group->save();
        return redirect()->route('groups.group',$this->group->id);
    }

    public function removeMember(Request $request){
        $memberList = $request->member;
        $this->group->members()->detach($memberList);
        $this->group->member_no -= 1;
        $this->group->save();
        return redirect()->route('groups.group',$this->group->id);
    }

    public function message(Request $request){
        $post = new GroupChat;
        $post->poster_id = Auth::user()->id;
        $post->content = $request->content;
        $post->group_id = $this->group->id;
        $post->save();

        return redirect()->route('groups.group',$this->group->id);
    }

}
