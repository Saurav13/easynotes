@extends ('admin.admin')


@section('body')
<div class="container-fluid">
<div class="card">
	<div class="container-fluid">
		<div class="card card-plain">
			<div class="header">
				<h4 class="title">Syllabus</h4>
				<a href="{{URL::to('admin/syllabus/create')}}" class="btn btn-success btn-md pull-right">Add</a>
			</div>
			<div class="content table-responsive table-full-width">
				<table class="table table-hover">
					<thead>
						<th>ID</th>
						<th>Title</th>
						<th>Faculty</th>
						<th>Semester</th>
						<th>Actions</th>
					</thead>
					<tbody>
						@foreach($syllabus as $q)
						<tr>
							<td>{{$q->id}}</td>
							<td>{{$q->title}}</td>
							<td>{{$q->faculty_name}}</td>
							<td>{{$q->semester}}</td>
							<td>
								<a href="{{URL::to('admin/syllabus'.'/'.$q->id.'/'.'edit')}}" title="edit">
									<i class="pe-7s-note2" style="font-size:20px"></i>
								</a>&nbsp;&nbsp;&nbsp;&nbsp;
								
								<a href="#"id="delete{{$q->id}}" cid="{{$q->id}}" title="delete">
									<i class="pe-7s-close" style="font-size:20px"></i>
								</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
</div>
<script src="https://code.jquery.com/jquery-3.2.1.min.js" ></script>

<script>
	$(document).ready(function(){
		$('[id *= "delete"]').click(function(event){
			var id = $(this).attr('cid');
			console.log(id);
			swal({
			  title: "Are you sure?",
			  text: "You will not be able to recover this file!",
			  type: "warning",
			  
			  showCancelButton: true,
			  
			}).then(function(){
				$.post("{{route('syllabus.delete')}}",{id:id,_token:"{{csrf_token()}}"},function(data){
					swal({
						title:"Deleted Successfully",
						type:"success"

					}).then(function(){
						window.location.reload();
					});
			});
		});
	});
	});
</script>

@stop