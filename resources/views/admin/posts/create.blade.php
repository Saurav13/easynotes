@extends('admin.admin')


@section('body')
<div class="container-fluid">
	<h3>Create New Post</h3>
	<div class="card">
		<div class="container-fluid">
			<br>
			{!! Form::open(array('route' => 'post.store' , 'enctype' =>'multipart/form-data','files'=>'true')) !!}
			{{csrf_field()}}
			{{Form::label('title','Title:')}}
			{{Form::text('title',null,array('id'=>'title','class'=>'form-control','required'=>'', 'maxlength'=>'255'))}}
			<br>
			<label for="alias">Alias</label>
			<input type="text" required name="alias" id="alias" class="form-control"/>

			<br>
			
			{{Form::label('description','Post Description:')}}
			{{Form::textarea('description',null/*'Enter the description' yo garyo bhane chain placeholder jastai kam garcha*/,array('class'=>'form-control','id'=>'addeditor'))}}
			<br>
			
			<label for="image">Featured Image:</label>
			<input type="file" name="image">
			<br>
						{{Form::submit('Create Post',array('class'=>'form-control', 'class'=>'btn btn-md btn-success','style'=>'margin-top:20px;'))}}
			{!! Form::close() !!}
			<br>
		</div>
	</div>
</div>
<script
  src="https://code.jquery.com/jquery-3.2.1.min.js"
  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  crossorigin="anonymous"></script>
   <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
            <script>
              tinymce.init({
            selector: "textarea#addeditor",
            
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern"
            ],
            toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
            toolbar2: "print preview | forecolor backcolor emoticons | template",
            image_advtab: true,
            file_picker_callback: function(callback, value, meta) {
            if (meta.filetype == 'image') {
                $('#upload').trigger('click');
                $('#upload').on('change', function() {
                var file = this.files[0];
                var reader = new FileReader();
                reader.onload = function(e) {
                    callback(e.target.result, {
                    alt: ''
                    });
                };
                reader.readAsDataURL(file);
                });
            }
            },
            templates: [
          {title: 'Newsletter1', description: 'Notice', url: "/templates/newsletter.html"}
        ]
        });
              </script>
<script>
	$(document).ready(function(){
		$('#title').keyup(function(){
			var text=$('#title').val();

			var alias=text.split(' ').join('-').toLowerCase();
			console.log(text,alias);
			$('#alias').val(alias);
		})
	});

</script>
@endsection
